

$(document).ready(function() {
$('#contactusenqy').click(function(e) {

var name = $('#name').val();
var email = $('#email').val();
var enquiry = $('#enquiry').val();
var contcno = $('#contcno').val();


var name_regex = /^[^-\s][a-zA-Z0-9\s-\. ]+$/;
var email_regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
var enquiry_regex = /^[^-\s][a-zA-Z0-9\s-,\. ]+$/;
var contcno_regex = /^[0-9+\s-]+$/;

if (name.length == 0) {
$('#prflupdateerror').text("* All fields are mandatory *"); // This Segment Displays The Validation Rule For All Fields
$("#name").focus();
return false;
}
// Validating name Field.
else if (!name.match(name_regex) || name.length == 0) {
$('#contact1').text("* Please enter alphabets only *"); // This Segment Displays The Validation Rule For Name
$("#name").focus();
setTimeout(function(){ $('#contact1').html(''); $('#name').css({'border-color':'#dedede'})}, 2000);
return false;
}
// Validating email Field.
else if (!email.match(email_regex) || email.length == 0) {
$('#contact2').text("* Please enter valid email address *"); // This Segment Displays The Validation Rule For Name
$("#email").focus();
setTimeout(function(){ $('#contact2').html(''); $('#email').css({'border-color':'#dedede'})}, 2000);
return false;
}
else if (!contcno.match(contcno_regex) || contcno.length == 0) {
$('#contact4').text("* Please enter valid digit number *"); // This Segment Displays The Validation Rule For Name
$("#contcno").focus();
setTimeout(function(){ $('#contact4').html(''); $('#contcno').css({'border-color':'#dedede'})}, 2000);
return false;
}
else if (!enquiry.match(enquiry_regex) || enquiry.length == 0) {
$('#contact3').text("* Please enter chracters only *"); // This Segment Displays The Validation Rule For Name
$("#enquiry").focus();
setTimeout(function(){ $('#contact3').html(''); $('#enquiry').css({'border-color':'#dedede'})}, 2000);
return false;
}



else {
//alert("Form Submitted Successfuly!");
return true;
}
});
});


$(document).ready(function () {
    $('.txtNumericonly').keydown(function (e) {
        if (e.shiftKey || e.ctrlKey || e.altKey) {
            e.preventDefault();
        } else {
            var key = e.keyCode;
            if (!((key == 8) || (key == 9) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                e.preventDefault();
            }
        }
    });
});
