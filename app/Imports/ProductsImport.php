<?php
namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use App\Product;

class ProductsImport implements ToModel
{
    public function model(array $row)
    {
        return new Product([
            'product_name' => $row[0],
            'product_description' => $row[1],
            'cat_name' => $row[2],
            'subcat' => $row[3],
            'image_name' => $row[4],
            'detail' => $row[5],
        ]);
    }
}