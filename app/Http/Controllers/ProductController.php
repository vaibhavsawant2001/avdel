<?php

namespace App\Http\Controllers;

use App\Imports\ProductsImport;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use League\Csv\Writer;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $product = Product::all();
        return response()->json($product);
    }
    public function storeOrUpdate(Request $request)
    {
        // Validate CSV file if provided
        $validator = Validator::make($request->all(), [
            'csv_file' => 'nullable|mimes:csv,txt|max:2048',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
    
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }
    
        // Check if CSV file is provided
        if ($request->hasFile('csv_file')) {
            // Process CSV file
            $file = $request->file('csv_file');
            $filePath = $file->getPathname();
            $csvData = array_map('str_getcsv', file($filePath));
            $header = array_shift($csvData);
    
            // Indexes for CSV columns
            $product_name_index = array_search('product_name', $header);
            $product_description_index = array_search('product_description', $header);
            $cat_name_index = array_search('cat_name', $header);
            $subcat_index = array_search('subcat', $header);
            $image_name_index = array_search('image_name', $header);
            $detail_index = array_search('detail', $header);
    
            // Process CSV data
            foreach ($csvData as $row) {
                // Check if 'product_name' is not empty or null
                if (!empty($row[$product_name_index])) {
                    $product = Product::firstOrNew(['product_name' => $row[$product_name_index]]);
                    $product->product_description = $row[$product_description_index];
                    $product->cat_name = $row[$cat_name_index];
                    $product->subcat = $row[$subcat_index];
                    $product->detail = $row[$detail_index];
    
                    // Handle image upload
                    $imageFile = $request->file('image');
                    if ($imageFile) {
                        $imageName = time() . '.' . $imageFile->getClientOriginalExtension();
                        $imageFile->move(public_path('images'), $imageName);
                        $product->image_name = $imageName;
                    } else {
                        $product->image_name = $row[$image_name_index];
                    }
    
                    $product->save();
                }
            }
        }
    
        // Process manual entry
        $manualProduct = new Product();
        $manualProduct->product_name = $request->input('product_name');
    
        // Check if 'product_name' for manual entry is not empty or null
        if (!empty($manualProduct->product_name)) {
            $manualProduct->product_description = $request->input('product_description');
            $manualProduct->cat_name = $request->input('cat_name');
            $manualProduct->subcat = $request->input('subcat');
            $manualProduct->detail = $request->input('detail');
    
            // Handle image upload for manual entry
            $manualImageFile = $request->file('image');
            if ($manualImageFile) {
                $manualImageName = time() . '.' . $manualImageFile->getClientOriginalExtension();
                $manualImageFile->move(public_path('images'), $manualImageName);
                $manualProduct->image_name = $manualImageName;
            } else {
                $manualProduct->image_name = $request->input('image_name');
            }
    
            $manualProduct->save();
        }
    
        return redirect('display');
    }
      
}