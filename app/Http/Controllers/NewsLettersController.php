<?php

namespace App\Http\Controllers;

use App\Models\NewsLetters;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NewsLettersController extends Controller
{
    public function index()
    {
        $newsLetters = NewsLetters::all();
        return response()->json(['data' => $newsLetters], 200);
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'album_name' => 'required|string',
            'date' => 'required|string',
            'location' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif',
            'description' => 'required|string',
        ]);
        $imagePath = $request->file('image')->store('images', 'public');
        $validatedData['image'] = $imagePath;
        $newsLetter = NewsLetters::create($validatedData);
        return redirect('yaaaro_pms/newsnletters');
    }
    public function update(Request $request, NewsLetters $newsLetter)
    {
        $validatedData = $request->validate([
            'album_name' => 'required|string',
            'date' => 'required|string',
            'location' => 'required|string',
            'image' => 'image|mimes:jpeg,png,jpg,gif',
            'description' => 'required|string',
        ]);
    
        // Check if the image exists before attempting to delete
        if ($newsLetter->image) {
            Storage::disk('public')->delete($newsLetter->image);
        }
    
        if ($request->hasFile('image')) {
            $imagePath = $request->file('image')->store('images', 'public');
            $validatedData['image'] = $imagePath;
        }
    
        $newsLetter->update($validatedData);
    
        return response()->json(['message' => 'NewsLetter updated successfully', 'data' => $newsLetter], 200);
    }
    
    public function edit($id)
    {
        $newsLetter = NewsLetters::find($id);
        return View('yaaaro_pms/newsletters_edit')->with(compact('newsLetter'));
    }
    public function show(NewsLetters $newsLetter)
    {
        return response()->json(['data' => $newsLetter], 200);
    }
    public function destroy(NewsLetters $newsLetter)
    {
        $newsLetter->delete();
        return response()->json(['message' => 'NewsLetter deleted successfully'], 204);
    }
}
