<?php

namespace App\Http\Controllers;

use App\Models\Mission;
use Illuminate\Http\Request;

class MissionController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->validate([
            'image' => 'required|image',
            'description' => 'required|string',
        ]);
        $imagePath = $request->file('image')->store('images');
        $mission = Mission::create([
            'image' => $imagePath,
            'description' => $data['description'],
        ]);
        return response()->json($mission);
    }
}
