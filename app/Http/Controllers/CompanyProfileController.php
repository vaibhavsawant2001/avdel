<?php

namespace App\Http\Controllers;

use App\Models\CompanyProfile;
use Illuminate\Http\Request;

class CompanyProfileController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->validate([
            'image' => 'required|image',
            'description' => 'required|string',
        ]);
        $imagePath = $request->file('image')->store('images');
        $companyProfile = CompanyProfile::create([
            'image' => $imagePath,
            'description' => $data['description'],
        ]);
        return response()->json($companyProfile);
    }
}
