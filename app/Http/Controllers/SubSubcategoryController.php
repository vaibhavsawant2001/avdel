<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Subcategory;
use App\Models\SubSubcategory;
use Illuminate\Http\Request;

class SubSubcategoryController extends Controller
{
        // Index function to display all subsubcategories
        public function index()
        {
            $subsubcategories = SubSubcategory::with(['category', 'subcategory'])->get();
            return view('subsubcategories.index', compact('subsubcategories'));
        }
    
        // Create function to show the form for creating a new subsubcategory
        public function create()
        {
            $categories = Category::all();
            $subcategories = Subcategory::all();
            return view('subsubcategories.create', compact('categories', 'subcategories'));
        }
    
        // Store function to store a new subsubcategory in the database
        public function store(Request $request)
        {
            $request->validate([
                'category_id' => 'required|exists:categories,id',
                'subcategory_id' => 'required|exists:subcategories,id',
                'subsubcat_name' => 'required|string',
                'status' => 'boolean',
            ]);
    
            $subsubcategory = SubSubcategory::create($request->all());
    
            return response()->json($subsubcategory);
        }
    
        // Show function to display the details of a specific subsubcategory
        public function show(SubSubcategory $subsubcategory)
        {
            return view('subsubcategories.show', compact('subsubcategory'));
        }
    
        // Edit function to show the form for editing a specific subsubcategory
        public function edit(SubSubcategory $subsubcategory)
        {
            $categories = Category::all();
            $subcategories = Subcategory::all();
            return view('subsubcategories.edit', compact('subsubcategory', 'categories', 'subcategories'));
        }
    
        // Update function to update the specified subsubcategory in the database
        public function update(Request $request, SubSubcategory $subsubcategory)
        {
            $request->validate([
                'category_id' => 'required|exists:categories,id',
                'subcategory_id' => 'required|exists:subcategories,id',
                'subsubcat_name' => 'required|string',
                'status' => 'boolean',
            ]);
    
            $subsubcategory->update($request->all());
    
            return redirect()->route('subsubcategories.index')
                ->with('success', 'Subsubcategory updated successfully');
        }
    
        // Destroy function to remove the specified subsubcategory from the database
        public function destroy(SubSubcategory $subsubcategory)
        {
            $subsubcategory->delete();
    
            return redirect()->route('subsubcategories.index')
                ->with('success', 'Subsubcategory deleted successfully');
        }
        public function getSubcategories($category)
        {
            $subcategories = Subcategory::where('category_id', $category)->get();
        
            return response()->json(['subcategories' => $subcategories]);
        }
        
}
