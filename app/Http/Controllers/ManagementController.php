<?php

namespace App\Http\Controllers;

use App\Models\Management;
use Illuminate\Http\Request;

class ManagementController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->validate([
            'image' => 'required|image',
            'description' => 'required|string',
        ]);
        $imagePath = $request->file('image')->store('images');
        $companyProfile = Management::create([
            'image' => $imagePath,
            'description' => $data['description'],
        ]);
        return response()->json($companyProfile);
    }
}
