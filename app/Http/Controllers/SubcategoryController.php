<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;

class SubcategoryController extends Controller
{
        // Index function to display all subcategories
        public function index()
        {
            $subcategories = Subcategory::with('category')->get();
            return view('subcategories.index', compact('subcategories'));
        }
    
        // Create function to show the form for creating a new subcategory
        public function create()
        {
            $categories = Category::all();
            return view('subcategories.create', compact('categories'));
        }
    
        // Store function to store a new subcategory in the database
        public function store(Request $request)
        {
            $request->validate([
                'category_id' => 'required|exists:categories,id',
                'subcat_name' => 'required|string',
                'status' => 'boolean',
            ]);
    
            $subcategory = Subcategory::create($request->all());
    
            return response()->json($subcategory);
        }
    
        // Show function to display the details of a specific subcategory
        public function show(Subcategory $subcategory)
        {
            return view('subcategories.show', compact('subcategory'));
        }
    
        // Edit function to show the form for editing a specific subcategory
        public function edit(Subcategory $subcategory)
        {
            $categories = Category::all();
            return view('subcategories.edit', compact('subcategory', 'categories'));
        }
    
        // Update function to update the specified subcategory in the database
        public function update(Request $request, Subcategory $subcategory)
        {
            $request->validate([
                'category_id' => 'required|exists:categories,id',
                'subcat_name' => 'required|string',
                'status' => 'boolean',
            ]);
    
            $subcategory->update($request->all());
    
            return redirect()->route('subcategories.index')
                ->with('success', 'Subcategory updated successfully');
        }
    
        // Destroy function to remove the specified subcategory from the database
        public function destroy(Subcategory $subcategory)
        {
            $subcategory->delete();
    
            return redirect()->route('subcategories.index')
                ->with('success', 'Subcategory deleted successfully');
        }
}
