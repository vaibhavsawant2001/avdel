<?php

namespace App\Http\Controllers;

use App\Models\Pages;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(Request $request)
    {
        $data = [
            'pages' => Pages::all(),
        ];
        return view('company_profile_edit')->with($data);
    }
    public function store(Request $request)
    {
        $requestData = $request->validate([
            'page_name' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif',
            'description' => 'required|string',
        ]);

        $imagePath = $request->file('image')->store('images');

        $page = Pages::create([
            'page_name' => $requestData['page_name'],
            'image' => $imagePath,
            'description' => $requestData['description'],
        ]);

        return response()->json($page);
    }

    public function update(Request $request, $id)
    {
        $page = Pages::findOrFail($id);
        if ($request->hasFile('image')) {
            $imagePath = $request->file('image')->store('images');
            $page->update([
                'page_name' => $request->input('page_name'),
                'image' => $imagePath,
                'description' => $request->input('description'),
            ]);
        } else {
            $page->update([
                'page_name' => $request->input('page_name'),
                'description' => $request->input('description'),
            ]);
        }
        return redirect('yaaaro_pms/company_profile_edit');
    }
    public function edit($id)
    {
        $user = Pages::find($id);
        return View('yaaaro_pms/management_team_edit')->with(compact('user'));
    }
}
