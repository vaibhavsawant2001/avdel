<?php

use App\Http\Controllers\CategoryController;
use App\Models\Category;
use App\Models\CompanyProfile;
use App\Models\Management;
use App\Models\Mission;
use App\Models\NewsLetters;
use App\Models\Pages;
use App\Models\Subcategory;
use App\Models\SubSubcategory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('about-us/profile', function () {
    $pages = Pages::all();
    return view('about-us/profile',[
        'pages'=>$pages,
    ]);
});
Route::get('about-us/management', function () {
    $pages = Pages::all();
    return view('about-us/management',[
        'pages'=>$pages,
    ]);
});
Route::get('about-us/mission', function () {
    $pages = Pages::all();
    return view('about-us/mission',[
        'pages'=>$pages,
    ]);
});
Route::get('about-us/partners', function () {
    $pages = Pages::all();
    return view('about-us/partners',[
        'pages'=>$pages,
    ]);
});
Route::get('about-us/clients', function () {
    $pages = Pages::all();
    return view('about-us/clients',[
        'pages'=>$pages,
    ]);
});
Route::get('about-us/approvals', function () {
    $pages = Pages::all();
    return view('about-us/approvals',[
        'pages'=>$pages,
    ]);
});
Route::get('about-us/downloads', function () {
    $pages = Pages::all();
    return view('about-us/downloads',[
        'pages'=>$pages,
    ]);
});
Route::get('about-us/associations', function () {
    $pages = Pages::all();
    return view('about-us/associations',[
        'pages'=>$pages,
    ]);
});
Route::get('divisions/atd', function () {
    $pages = Pages::all();
    return view('divisions/atd',[
        'pages'=>$pages,
    ]);
});
Route::get('divisions/aviation', function () {
    $pages = Pages::all();
    return view('divisions/aviation',[
        'pages'=>$pages,
    ]);
});
Route::get('product/products', function () {
    return view('product/products');
});
Route::get('newsnevents', function () {
    $newsletters = NewsLetters::all();
    $pages = Pages::all();
    return view('newsnevents',[
        'newsletters'=>$newsletters,
        'pages'=>$pages,
    ]);
});
Route::get('newsnevents_detail', function () {
    $newsletters = NewsLetters::all();
    return view('newsnevents_detail',[
        'newsletters'=>$newsletters,
    ]);
});
Route::get('careers', function () {
    $pages = Pages::all();
    return view('careers',[
        'pages'=>$pages,
    ]);
});
Route::get('contact-us', function () {
    return view('contact-us');
});
Route::get('product', function () {
    $categories = Category::all();
    $subcategories = Subcategory::all();
    $subsubcategories = SubSubcategory::all();
    return view('product',[
        'categories' => $categories,
        'subcategories' => $subcategories,
        'subsubcategories' => $subsubcategories,
    ]);
});
Route::get('display', function () {
    $products = DB::table('products')->get();
    return view('display',[
        'products'=>$products,
    ]);
});
Route::get('category_display', function () {
    $categories = Category::all();
    return view('category_display',[
        'categories'=>$categories,
    ]);
});

Route::get('subcategories', function () {
    $categories = Category::all();
    return view('subcategories',[
        'categories'=>$categories,
    ]);
});

Route::get('sub_subcategory', function () {
    $categories = Category::all();
    $subcategories = Subcategory::all();
    return view('sub_subcategory',[
        'categories'=>$categories,
        'subcategories'=>$subcategories,

    ]);
});
Route::get('show_cat', function () {
    $categories = Category::all();
    $subcategories = Subcategory::all();
    $subsubcategories = SubSubcategory::all();
    return view('show_cat', [
        'categories' => $categories,
        'subcategories' => $subcategories,
        'subsubcategories' => $subsubcategories,
    ]);
});

Route::get('yaaaro_pms', function () {
    return view('yaaaro_pms/index');
});
Route::get('yaaaro_pms/admin', function () {
    return view('yaaaro_pms/admin');
});
Route::get('yaaaro_pms/company_profile_edit', function () {
    $pages = Pages::all();
    return view('yaaaro_pms/company_profile_edit',[
        'pages'=>$pages,
    ]);
});
Route::get('yaaaro_pms/management_team_edit', function () {
    return view('yaaaro_pms/management_team_edit');
});
Route::get('yaaaro_pms/newsnletters', function () {
    $newsletters = NewsLetters::all();
    return view('yaaaro_pms/newsnletters',[
        'newsletters'=>$newsletters,
    ]);
});
Route::get('yaaaro_pms/newsletters_add', function () {
    return view('yaaaro_pms/newsletters_add');
});
Route::get('yaaaro_pms/newsletters_edit', function () {
    return view('yaaaro_pms/newsletters_edit');
});
Route::get('yaaaro_pms/mission', function () {
    return view('yaaaro_pms/mission');
});