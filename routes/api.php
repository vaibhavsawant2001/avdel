<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CompanyProfileController;
use App\Http\Controllers\ManagementController;
use App\Http\Controllers\MissionController;
use App\Http\Controllers\NewsLettersController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SubcategoryController;
use App\Http\Controllers\SubSubcategoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('products', [ProductController::class, 'index'])->name('products.index');
Route::post('products', [ProductController::class, 'storeOrUpdate'])->name('products.import');



Route::get('/categories', [CategoryController::class, 'index'])->name('categories.index');
Route::get('/categories/create', [CategoryController::class, 'create'])->name('categories.create');
Route::post('/categories', [CategoryController::class, 'store'])->name('categories.store');
Route::get('/categories/{category}', [CategoryController::class, 'show'])->name('categories.show');
Route::get('/categories/{category}/edit', [CategoryController::class, 'edit'])->name('categories.edit');
Route::put('/categories/{category}', [CategoryController::class, 'update'])->name('categories.update');
Route::delete('/categories/{category}', [CategoryController::class, 'destroy'])->name('categories.destroy');


// Subcategories routes
Route::get('/subcategories', [SubcategoryController::class, 'index'])->name('subcategories.index');
Route::get('/subcategories/create', [SubcategoryController::class, 'create'])->name('subcategories.create');
Route::post('/subcategories', [SubcategoryController::class, 'store'])->name('subcategories.store');
Route::get('/subcategories/{subcategory}', [SubcategoryController::class, 'show'])->name('subcategories.show');
Route::get('/subcategories/{subcategory}/edit', [SubcategoryController::class, 'edit'])->name('subcategories.edit');
Route::put('/subcategories/{subcategory}', [SubcategoryController::class, 'update'])->name('subcategories.update');
Route::delete('/subcategories/{subcategory}', [SubcategoryController::class, 'destroy'])->name('subcategories.destroy');


// Subsubcategories routes
Route::get('/subsubcategories', [SubSubcategoryController::class, 'index'])->name('subsubcategories.index');
Route::get('/subsubcategories/create', [SubSubcategoryController::class, 'create'])->name('subsubcategories.create');
Route::post('/subsubcategories', [SubSubcategoryController::class, 'store'])->name('subsubcategories.store');
Route::get('/subsubcategories/{subsubcategory}', [SubSubcategoryController::class, 'show'])->name('subsubcategories.show');
Route::get('/subsubcategories/{subsubcategory}/edit', [SubSubcategoryController::class, 'edit'])->name('subsubcategories.edit');
Route::put('/subsubcategories/{subsubcategory}', [SubSubcategoryController::class, 'update'])->name('subsubcategories.update');
Route::delete('/subsubcategories/{subsubcategory}', [SubSubcategoryController::class, 'destroy'])->name('subsubcategories.destroy');

Route::get('/get-subcategories/{category}', [SubSubcategoryController::class, 'getSubcategories']);


Route::post('company_profile', [CompanyProfileController::class, 'store'])->name('companyprofile.store');
Route::post('management', [ManagementController::class, 'store'])->name('management.store');
Route::post('mission', [MissionController::class, 'store'])->name('mission.store');


Route::post('/pages', [PagesController::class, 'store'])->name('pages.store');
Route::get('/pages/{id}/edit', [PagesController::class, 'edit'])->name('pages.edit');
Route::put('/pages/{id}', [PagesController::class, 'update'])->name('pages.update');


Route::post('/newsletters', [NewsLettersController::class, 'store'])->name('newsletters.store');
Route::get('/newsletters/{id}/edit', [NewsLettersController::class, 'edit'])->name('newsletters.edit');
Route::put('/newsletters/{id}', [NewsLettersController::class, 'update'])->name('newsletters.update');