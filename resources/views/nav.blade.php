<header id="main-navigation">
            <div class="row mainbluepatti">
                <div class="col-md-8 "></div>
                <div class="col-md-4 col-xs-12">
                    <div class="">

                        <div id="ucHeader1_pnlSearch" onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;ucHeader1_lnkSearch&#39;)">

                            <div class="form-group pull-right sb-search-open" id="search" style="position: relative;">
                                <input type="text" class="form-control mainform" name="search" placeholder="Search" id="txtSearch" autocomplete="off">

                                <a onclick="return fnSearch(&#39;/Search&#39;);" id="ucHeader1_lnkSearch" class="form-control form-control-submit" href="javascript:__doPostBack(&#39;ctl00$ucHeader1$lnkSearch&#39;,&#39;&#39;)"><i class="glyphicon glyphicon-search"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="navigation" data-spy="affix" data-offset-top="20">
                <div class="row">
                    <div class="row upperrow" style="position: relative;">
                        <div class="col-md-3 col-xs-9">
                            <div class="mainlogo">
                                <a class="navbar-brand logo" href="{{url('/')}}">
                                    <img src="{{url('images/avdel-logo.png')}}" alt="logo" class="img-responsive"></a>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <nav class="navbar navbar-default">
                                <div class="navbar-header page-scroll">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-collapse-navbar" aria-expanded="true"><span class="icon-bar top-bar"></span><span class="icon-bar middle-bar"></span><span class="icon-bar bottom-bar"></span></button>
                                </div>
                                <div id="fixed-collapse-navbar" class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li id="Menu1"><a href="{{url('/')}}">Home</a></li>
                                        <li id="Menu2" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">About Us &nbsp;<i class="fa fa-caret-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{url('about-us/profile')}}">Company Profile </a></li>
                                                <li><a href="{{url('about-us/management')}}">Mangement Team </a></li>
                                                <li><a href="{{url('about-us/mission')}}">Mission, Vision & Strategy </a></li>

                                                <li><a href="{{url('about-us/partners')}}">Our Partners </a></li>
                                                <li><a href="{{url('about-us/clients')}}">Clients </a></li>
                                                <li><a href="{{url('about-us/approvals')}}">Approvals & Manuals </a></li>
                                                <li><a href="{{url('about-us/associations')}}">Associations </a></li>
                                            </ul>
                                        </li>
                                        <li id="Menu3" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Divisions &nbsp;<i class="fa fa-caret-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{url('divisions/atd')}}">ATD
                                                        <br/>
                                                        <small>(Aerospace/Transportation/Defence)</small></a> </li>
                                                <li><a href="{{url('divisions/aviation')}}">Aviation </a></li>
                                            </ul>
                                        </li>
                                        <li id="Menu4" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Products &nbsp;<i class="fa fa-caret-down"></i></a>

                                            <ul class="dropdown-menu">

                                                <li><a href="{{url('product/products')}}">Fasteners </a></li>


                                                <li><a href="/product/miscelleneous-hardware/4">Miscelleneous Hardware </a></li>


                                                <li><a href="/product/power-operated-tools/16">Power-Operated Tools </a></li>


                                                <li><a href="/product/advanced-materials/6">Advanced Materials </a></li>


                                                <li><a href="/product/hand-tools/17">Hand Tools </a></li>


                                                <li><a href="/product/control-cables/3">Control Cables </a></li>


                                                <li><a href="/product/carpet-floorings/8">Carpet Floorings </a></li>


                                                <li><a href="/product/surface-coverings/9">Surface Coverings </a></li>


                                                <li><a href="/product/leather-seat-materials/10">Leather Seat Materials </a></li>


                                                <li><a href="/product/sheepskins/11">Sheepskins </a></li>


                                                <li><a href="/product/aircraft-glass/7">Aircraft Glass </a></li>


                                                <li><a href="/product/aviation-fabrics/13">Aviation Fabrics </a></li>

                                            </ul>
                                        </li>
                                        <li id="Menu5" class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services &nbsp;<i class="fa fa-caret-down"></i> </a>
                                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" style="right: auto;">
                                                <li id="subMenu1" class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">AVTRIP</a>
                                                    <ul class="dropdown-menu" style="width: 230px;">
                                                        <li id="InnerMenu1"><a class="dropdown-item" href="/services/trip">Trip Management Services</a></li>
                                                        <li id="InnerMenu2"><a class="dropdown-item" href="/services/interior">Interior Refurbishment</a></li>
                                                        <li id="InnerMenu3"><a class="dropdown-item" href="/services/mro">MRO Support</a></li>
                                                    </ul>
                                                </li>
                                                <li id="subMenu2" class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">AVTRADE</a>
                                                    <ul class="dropdown-menu" style="width: 230px;">
                                                        <li id="InnerMenu4"><a class="dropdown-item" href="/services/aircraft">Aircraft Acquisition & Sales</a></li>
                                                    </ul>
                                                </li>
                                                <li id="subMenu3" class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">AVCARE</a>
                                                    <ul class="dropdown-menu" style="width: 230px;">
                                                        <li id="InnerMenu5"><a class="dropdown-item" href="/services/aircharters">Air Charter Services</a></li>
                                                        <li id="InnerMenu6"><a class="dropdown-item" href="/services/vipconciergeservices">VIP Concierge Services</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li id="Menu6"><a href="{{url('newsnevents')}}">News & Events</a> </li>
                                        <li id="Menu7"><a href="{{url('careers')}}">Careers</a></li>
                                        <li id="Menu8"><a href="{{url('contact-us')}}">Contact Us</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row mainblue">
                    <div class="col-md-12">
                        <div class="container">
                        </div>
                    </div>
                </div>
            </div>
        </header>