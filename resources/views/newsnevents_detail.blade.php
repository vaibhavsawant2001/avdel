@include('head')
@include('nav')
<script>
    function storeCID(id, urlName) {
        if (id != undefined && id != '') {
            // alert(id);
            // alert(urlName);
            var url = '/wsCommon.asmx/StoreCategoryID'
            $.ajax({
                type: "POST",
                url: url,
                data: "{ CatID: '" + id + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: "true",
                cache: "false",
                success: function (msg) {
                    window.location.href = urlName;
                },
                Error: function (x, e) {
                    // On Error
                }
            });
        }
    }

    function fnSearch(urlName) {
        //  alert("hiii");
        //  alert(urlName);
        var stxt = $("#txtSearch").val();
        if (stxt == undefined || stxt == '') {
            alert("Enter Search Text");
            $("#txtSearch").focus();
            return false;
        }
        else {
            var url = '/wsCommon.asmx/SearchMethod'
            $.ajax({
                type: "POST",
                url: url,
                data: "{ txt: '" + stxt + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: "true",
                cache: "false",
                success: function (msg) {
                    window.location.href = urlName;
                },
                Error: function (x, e) {
                    // On Error
                }
            });
            return false;
        }
    }
</script>


        <div>
            
    <div class="breadcrumbs" id="companyprofile">
        <div class="row breadinn">
            <div class="col-md-9 pad0">
                <div class="mainbread">
                    <ol class="breadcrumb">
                        <li><a href="/home">Home</a></li>
                        <li class="active">
                            <span id="ContentPlaceHolder1_lblCurrentPage">Avdel celebrates "International Womens Day" at its facility.</span></li>
                    </ol>
                    <h2>NEWS & EVENTS</h2>
                </div>
            </div>
            <div class="col-md-3 topspace50">
                
            </div>
        </div>
    </div>


    <!-- Responsive image with left -->
    <section id="responsive" class="innerpadding">
  <div class="container">
   
    <div class="row">
    <div class="col-md-12">
    	<div class="newsdetailpage maintitlepage">
        	<h2><span id="ContentPlaceHolder1_lblTitle">Avdel celebrates "International Womens Day" at its facility.</span></h2>
            <h5><span id="ContentPlaceHolder1_lblDate">March 8, 2022</span> </h5>
        </div>
        </div>
    	 <div class="">
        <div class="col-md-12">

      <div class="newseventstops">
        
            <p>Avdel India celebrated &quot;Internatonal Womens Day&quot; at its&nbsp;office premises on 08th March-2022 in Mumbai.</p>

<p>Avdel acknowledges the contribution for its&nbsp;women workforce and salutes all women eployees at all its facilities for thier immense decication and hardwork.&nbsp;&nbsp;&nbsp;&nbsp;</p>

<p>Three Cheers to Women Power.</p>

<p>&nbsp;</p>

      </div>
      
	<div class="row">
		<div class='list-group gallery'>
            
                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                        <a class="thumbnail fancybox" rel="ligthbox" href="/images/Events/33/IMG20220308172539_Bokeh.jpg">
                            <img class="img-responsive" alt="" src="/images/Events/33/IMG20220308172539_Bokeh.jpg" />
                        </a>
                    </div>
                
                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                        <a class="thumbnail fancybox" rel="ligthbox" href="/images/Events/33/IMG20220308171834.jpg">
                            <img class="img-responsive" alt="" src="/images/Events/33/IMG20220308171834.jpg" />
                        </a>
                    </div>
                
                    <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                        <a class="thumbnail fancybox" rel="ligthbox" href="/images/Events/33/IMG20220308172836_Bokeh.jpg">
                            <img class="img-responsive" alt="" src="/images/Events/33/IMG20220308172836_Bokeh.jpg" />
                        </a>
                    </div>
                
        </div> <!-- list-group / end -->
	</div> <!-- row / end -->
      </div>
        </div>
    </div>
  </div>
</section>
    <!-- Latest Publications -->
    <!------- CUSTOMERS --------->
    <script src="/assets/owlcarousel/owl.carousel.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Menu6").addClass("active");
        });
    </script>

        </div>
@include('footer')