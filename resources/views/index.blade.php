<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head id="headMaster">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/icomoon-fonts.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/settings.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/addnew.css')}}">

    <link rel="stylesheet" type="text/css" href="{{url('css/jquery.fancybox.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/zerogrid.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/jPushMenu.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400%7COpen+Sans:400,300" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('css/demo.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('css/slider-style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('css/onepage.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/one-color.css')}}">
    <link href="/bxslider/jquery.bxslider.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/css/loader.css">
    <link rel="stylesheet" type="text/css" href="/css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="{{url('js/jquery-2.1.4.js')}}"></script>
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('bxslider/jquery.bxslider.js')}}"></script>
    <title>
        AVDEL
    </title>
    <link rel="shortcut icon" href="{{url('images/favicon.png')}}">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-173024969-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-173024969-1');
    </script>
    <link rel="canonical" href="/Home.aspx" />
    <!--%%%%%%%%%%%%%%%%%% Code Added by amol for Pop up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->
    <script type="text/javascript">
        //$(document).ready(function () {
        //    $("#covidpop").modal();
        //});
    </script>
    <!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->
    <link rel="stylesheet" href="{{url('assets/owlcarousel/assets/owl.carousel.min.css')}}">
</head>

<body id="page-top" data-spy="scroll" data-target="#fixed-collapse-navbar" data-offset="120">
    <form method="post" action="./" id="form1">
        <div class="aspNetHidden">
            <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
            <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
            <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTIwOTI5NjU3MjUPZBYCZg9kFgICAQ9kFgYCAw9kFgRmD2QWAgIBDw8WAh4NT25DbGllbnRDbGljawUbcmV0dXJuIGZuU2VhcmNoKCcvU2VhcmNoJyk7ZGQCAQ8WAh4LXyFJdGVtQ291bnQCDBYYAgEPZBYCZg8VAhQvcHJvZHVjdC9mYXN0ZW5lcnMvMQlGYXN0ZW5lcnNkAgIPZBYCZg8VAiEvcHJvZHVjdC9taXNjZWxsZW5lb3VzLWhhcmR3YXJlLzQWTWlzY2VsbGVuZW91cyBIYXJkd2FyZWQCAw9kFgJmDxUCIC9wcm9kdWN0L3Bvd2VyLW9wZXJhdGVkLXRvb2xzLzE2FFBvd2VyLU9wZXJhdGVkIFRvb2xzZAIED2QWAmYPFQIdL3Byb2R1Y3QvYWR2YW5jZWQtbWF0ZXJpYWxzLzYSQWR2YW5jZWQgTWF0ZXJpYWxzZAIFD2QWAmYPFQIWL3Byb2R1Y3QvaGFuZC10b29scy8xNwpIYW5kIFRvb2xzZAIGD2QWAmYPFQIZL3Byb2R1Y3QvY29udHJvbC1jYWJsZXMvMw5Db250cm9sIENhYmxlc2QCBw9kFgJmDxUCGy9wcm9kdWN0L2NhcnBldC1mbG9vcmluZ3MvOBBDYXJwZXQgRmxvb3JpbmdzZAIID2QWAmYPFQIcL3Byb2R1Y3Qvc3VyZmFjZS1jb3ZlcmluZ3MvORFTdXJmYWNlIENvdmVyaW5nc2QCCQ9kFgJmDxUCIi9wcm9kdWN0L2xlYXRoZXItc2VhdC1tYXRlcmlhbHMvMTAWTGVhdGhlciBTZWF0IE1hdGVyaWFsc2QCCg9kFgJmDxUCFi9wcm9kdWN0L3NoZWVwc2tpbnMvMTEKU2hlZXBza2luc2QCCw9kFgJmDxUCGS9wcm9kdWN0L2FpcmNyYWZ0LWdsYXNzLzcOQWlyY3JhZnQgR2xhc3NkAgwPZBYCZg8VAhwvcHJvZHVjdC9hdmlhdGlvbi1mYWJyaWNzLzEzEEF2aWF0aW9uIEZhYnJpY3NkAgUPZBYeZg8WAh8BAgcWDgIBD2QWAmYPFQIBMQ5jbGFzcz0nYWN0aXZlJ2QCAg9kFgJmDxUCATIIY2xhc3M9JydkAgMPZBYCZg8VAgEzCGNsYXNzPScnZAIED2QWAmYPFQIBNAhjbGFzcz0nJ2QCBQ9kFgJmDxUCATUIY2xhc3M9JydkAgYPZBYCZg8VAgE2CGNsYXNzPScnZAIHD2QWAmYPFQIBNwhjbGFzcz0nJ2QCAQ8WAh8BAgcWDgIBD2QWAmYPFQQBMRNjbGFzcz0naXRlbSBhY3RpdmUnHS9pbWFnZXMvTWFzdGhlYWQvNy9iYW5uZXIuanBnAGQCAg9kFgJmDxUEATINY2xhc3M9J2l0ZW0gJycvaW1hZ2VzL01hc3RoZWFkLzYvQUlSQ1JBRlRDSEFSVEVSUy5qcGcsQWlyY3JhZnQgQ2hhcnRlcnMgPGJyIC8+JiBDb25jaWVyZ2UgU2VydmljZXNkAgMPZBYCZg8VBAEzDWNsYXNzPSdpdGVtICchL2ltYWdlcy9NYXN0aGVhZC81L2JhY2tncm91bmQuanBnHk5ldyBEYXk8YnIgLz5OZXcgUG9zc2liaWxpdGllc2QCBA9kFgJmDxUEATQNY2xhc3M9J2l0ZW0gJx4vaW1hZ2VzL01hc3RoZWFkLzQvc2xpZGVyMi5qcGchU21hbGwgUGFydHMsIDxiciAvPkJpZyBUZWNobm9sb2d5ZAIFD2QWAmYPFQQBNQ1jbGFzcz0naXRlbSAnHi9pbWFnZXMvTWFzdGhlYWQvMy9zbGlkZXIzLmpwZyBPd24geW91ciBwZWFjZSA8YnIgLz5pbiB0aGUgU2t5IGQCBg9kFgJmDxUEATYNY2xhc3M9J2l0ZW0gJx4vaW1hZ2VzL01hc3RoZWFkLzIvc2xpZGVyNC5qcGc/RGVsaXZlcmluZyBNYW51ZmFjdHVyaW5nIFNvbHV0aW9ucyA8YnI+IGZvciBtb3JlIHRoYW4gNiBEZWNhZGVzZAIHD2QWAmYPFQQBNw1jbGFzcz0naXRlbSAnHi9pbWFnZXMvTWFzdGhlYWQvMS9zbGlkZXI1LmpwZxtXZSBEZWxpdmVyIDxicj4gIHRvIENvbm5lY3RkAgIPFgIeBFRleHQFsQQ8cD5BdmRlbCAoSW5kaWEpIFB2dC4gTHRkIHdhcyBlc3RhYmxpc2hlZCBpbiAxOTYxIGFuZCBpcyBhIHBhcnQgb2YgYSB3ZWxsLWVzdGFibGlzaGVkIGdyb3VwIG9mIGNvbXBhbmllcyB3aXRoIGludGVyZXN0czwvcD4NCg0KPHA+aW4gdmFyaW91cyBidXNpbmVzcyBzZWN0b3JzIGluY2x1ZGluZyBBZXJvc3BhY2UsIEF2aWF0aW9uLCBBdXRvbW90aXZlLCBhbmQgUmFpbCBhbW9uZ3N0IG90aGVycy4gQXZkZWwgSW5kaWEgaXM8L3A+DQoNCjxwPmFuIEFTIDkxMjAgQiAmYW1wOyBJU08gOTAwMToyMDE1IENlcnRpZmllZCBjb21wYW55LiBXaXRoIGhlYWRxdWFydGVycyBpbiBNdW1iYWkgKEluZGlhKSwgQXZkZWwgSW5kaWEgaGFzIHJlZ2lvbmFsIG9mZmljZXM8L3A+DQoNCjxwPmluIEJlbmdhbHVydSAodGhlIGFlcm9zcGFjZSBodWIgb2YgdGhlIGNvdW50cnkpIGFuZCBzYWxlcyBvZmZpY2UgaW4gSHlkZXJhYmFkLCBhbmQgVHJpdmFuZHJ1bS4mbmJzcDs8L3A+DQoNCjxwPiZuYnNwOzwvcD4NCjxxdWlsbGJvdC1leHRlbnNpb24tcG9ydGFsPjwvcXVpbGxib3QtZXh0ZW5zaW9uLXBvcnRhbD5kAgMPFgIeA3NyYwUfL2ltYWdlcy9Ib21lL0FURC9maXJzdGltYWdlLmpwZ2QCBA8WAh8DBSIvaW1hZ2VzL0hvbWUvQVREL0FURF90aHVtYm5haWwuanBnZAIFDxYCHwMFLC9pbWFnZXMvSG9tZS9BdmlhdGlvbi9BdmlhdGlvbl90aHVtYm5haWwuanBnZAIGDxYCHwIF4gM8cD5BdmRlbCBJbmRpYS0gQVREIERpdmlzaW9uIHdhcyBzZXQtdXAgdG8gd2FyZWhvdXNlIGFuZCBzdXBwbHkgZmFzdGVuZXJzIGFuZCBvdGhlciBoYXJkd2FyZSBpbmNsdWRpbmcgbWVjaGFuaWNhbCBjb250cm9sIGNhYmxlcywgYW5kIHRvIGFsc28gc3VwcGx5IEFkdmFuY2VkIENvbXBvc2l0ZSBNYXRlcmlhbHMuPC9wPg0KPHA+V2l0aCBEaXN0cmlidXRpb24gYW5kIHJlcHJlc2VudGF0aW9uIGZvciBzb21lIG9mIHRoZSB3b3JsZCZyc3F1bztzIGxlYWRpbmcgYnJhbmRzIGZvciBhbGwgdGhlIGFib3ZlIHByb2R1Y3RzLCBBdmRlbCBJbmRpYSBoYXMgZW1lcmdlZCBhcyBhIGxlYWRpbmcgc3VwcGxpZXIgdG8gaXRzIGN1c3RvbWVycyBmb3IgdGhlIHNhbWUuIE5vdCBqdXN0IGZvciB0aGUgQWVyb3NwYWNlIHNlZ21lbnQgYnV0IGFsc28gZm9yIERlZmVuY2UsIFJhaWwsIE1hcmluZSAmYW1wOyBBbHRlcm5hdGUgRW5lcmd5IEluZHVzdHJpZXMuPC9wPmQCBw8WAh8DBSYvaW1hZ2VzL0hvbWUvQXZpYXRpb24vc2Vjb25kaW1hZ2VzLmpwZ2QCCA8WAh8DBSIvaW1hZ2VzL0hvbWUvQVREL0FURF90aHVtYm5haWwuanBnZAIJDxYCHwMFLC9pbWFnZXMvSG9tZS9BdmlhdGlvbi9BdmlhdGlvbl90aHVtYm5haWwuanBnZAIKDxYCHwIF6gM8cD5BdmRlbCBBdmlhdGlvbiBjYXRlcnMgdG8gdGhlIG5lZWRzIG9mIHRoZSBncm93aW5nIENvbW1lcmNpYWwgQXZpYXRpb24gYW5kIE1STyBzZWdtZW50IHdpdGggYSBzcGVjaWFsIGZvY3VzIG9uIEJ1c2luZXNzIChHZW5lcmFsKSZuYnNwOyBBdmlhdGlvbiBpbiBJbmRpYS4gQXZkZWwgYWxvbmcgd2l0aCB0aGVpciBwYXJ0bmVycyBwcm92aWRlIFRyaXAgTWFuYWdlbWVudCBTZXJ2aWNlcywgQnVzaW5lc3MgSmV0IEFjcXVpc2l0aW9uICZhbXA7IFNhbGVzIENvbnN1bHRhbmN5LCBBaXJjcmFmdCBJbnRlcmlvciByZWZ1cmJpc2htZW50IHNlcnZpY2VzLCBTdXBwbHkgb2YgQXZpYXRpb24gZ3JhZGUgY2FycGV0cyAmYW1wOyBzdXJmYWNlIGNvdmVyaW5nIG1hdGVyaWFscywgTGVhdGhlciAmYW1wOyBzaGVlcHNraW5zLCBBdmlhdGlvbiBmYWJyaWNzLCBNaXJyb3JzICZhbXA7IEdsYXNzIHBhbmVscyBhbmQgTWV0YWwgJmFtcDsgUGxhc3RpYyBDb2F0aW5ncy48L3A+ZAILDxYCHwECCBYQZg9kFgJmDxUGFi9uZXdzbmV2ZW50cy1kZXRhaWwvNDEnL2ltYWdlcy9FdmVudHMvNDEvQWVybyBJbmRpYSAtMjIgTkUuanBnQk1FRVQgVEhFIEFWREVMIElORElBIFRFQU0gQVQgQUVSTyBJTkRJQSBTQ0hFRFVMRUQgRlJPTSAxMy0xNyBUSC4uLg9KYW51YXJ5IDEsIDIwMjNnQXZkZWwgSW5kaWEgc2hhbGwgYmUgcGFydGljaXBhdGluZyBhdCBBZXJvIEluZGlhIHNjaGVkdWxlZCBmcm9tIDEzLTE3dGggRmVicnVhcnkgMjAyMyBhdCBZZWxhaGFua2EgQS4uLhYvbmV3c25ldmVudHMtZGV0YWlsLzQxZAIBD2QWAmYPFQYWL25ld3NuZXZlbnRzLWRldGFpbC80MCwvaW1hZ2VzL0V2ZW50cy80MC9BdmRlbCBJbmRpYSBDcmliTWFzdGVyLnBuZ0RTZW1pbmFyIG9uIOKAnFNUQU5MRVkgQ3JpYk1hc3RlciAtIEF1dG9tYXRlZCBJbnZlbnRvcnkgRGlzcGVuc2luZy4uLhBPY3RvYmVyIDE0LCAyMDIyZ0F2ZGVsIEluZGlhIGFsb25nIHdpdGggQ3JpYk1hc3RlciBpcyBjb25kdWN0aW5nIGFuIE9ubGluZSBTZW1pbmFyIG9uIFRodXJzZGF5LSZuYnNwO05vdmVtYmVyIDAzLCAyMDIuLi4WL25ld3NuZXZlbnRzLWRldGFpbC80MGQCAg9kFgJmDxUGFi9uZXdzbmV2ZW50cy1kZXRhaWwvMzkmL2ltYWdlcy9FdmVudHMvMzkvRklBIGFubm91bmNlbWVudC5qcGdCTWVldCB0aGUgQXZkZWwgSW5kaWEgVGVhbSBhdCBGYXJuYm9yb3VnaCBJbnRlcm5hdGlvbmFsIEFpciBTaG93Li4uDUp1bmUgMjIsIDIwMjJnJm5ic3A7DQoNCkF2ZGVsIEluZGlhIFNlbmlvciBtYW5hZ2VtZW50IHRlYW0gc2hhbGwgYmUgcHJlc2VudCBhdCBGYXJuYm9yb3VnaCBJbnRlcm5hdGlvbmFsIEFpciBzaG93IC4uLhYvbmV3c25ldmVudHMtZGV0YWlsLzM5ZAIDD2QWAmYPFQYWL25ld3NuZXZlbnRzLWRldGFpbC8zNyEvaW1hZ2VzL0V2ZW50cy8zNy9OQU1TIEFXQVJEMi5qcGdCIEF2ZGVsIEluZGlhIHBhcnRpY2lwYXRpb24gYXQgTmF0aW9uYWwgQWVyb3NwYWNlIE1hbnVmYWN0dXJpbmcgLi4uDE1heSAyNSwgMjAyMmdBdmRlbCBJbmRpYSBoYWQgYW4gYWN0aXZlIHBhcnRpY2lwYXRpb24gYW5kIGNvLXNwb25zb3JlZCZuYnNwO3RoZSBOYXRpb25hbCBBZXJvc3BhY2UgTWFudWZhY3R1cmluZyBTLi4uFi9uZXdzbmV2ZW50cy1kZXRhaWwvMzdkAgQPZBYCZg8VBhYvbmV3c25ldmVudHMtZGV0YWlsLzM2PS9pbWFnZXMvRXZlbnRzLzM2L1NBTEVTIENPTkZBdmRlbCBuZXcgZXZlbnQgaW1hZ2UgMjAyMi0wNC5qcGcoQXZkZWwgSW5kaWEgU2FsZXMgQ29uZmVyZW5jZSAmIFdvcmtzaG9wLg5BcHJpbCAyMCwgMjAyMmdBdmRlbCBJbmRpYSBzY2hlZHVsZWQgdGhlaXIgJnF1b3Q7U2FsZXMgQ29uZmVyZW5jZSAmYW1wOyBXb3Jrc2hvcCZxdW90OyBhdCBGb3VudGFpbiBIZWFkLCZuYnNwO0FsaWJhLi4uFi9uZXdzbmV2ZW50cy1kZXRhaWwvMzZkAgUPZBYCZg8VBhYvbmV3c25ldmVudHMtZGV0YWlsLzM0Ji9pbWFnZXMvRXZlbnRzLzM0L1NQT1JUUyBBdmRlbCBuZXcuanBnHUF2ZGVsIEFubnVhbCBTcG9ydHMgTWVldC0yMDIyDk1hcmNoIDEzLCAyMDIyZ0F2ZGVsIEluZGlhIG9yZ2FuaXplZCBBbm51YWwgU3BvcnRzIEV2ZW50IG9uIDEzLSBNYXJjaC0yMDIyIGF0IE92YWwgTWFpZGFhbiBNdW1iYWkgZm9yIGFsbCBvZiBpdHMgc3QuLi4WL25ld3NuZXZlbnRzLWRldGFpbC8zNGQCBg9kFgJmDxUGFi9uZXdzbmV2ZW50cy1kZXRhaWwvMzMsL2ltYWdlcy9FdmVudHMvMzMvV09NRU5TIEF2ZGVsIG5ldyBldmVudC5qcGc8QXZkZWwgY2VsZWJyYXRlcyAiSW50ZXJuYXRpb25hbCBXb21lbnMgRGF5IiBhdCBpdHMgZmFjaWxpdHkuDU1hcmNoIDgsIDIwMjJnQXZkZWwgSW5kaWEgY2VsZWJyYXRlZCAmcXVvdDtJbnRlcm5hdG9uYWwgV29tZW5zIERheSZxdW90OyBhdCBpdHMmbmJzcDtvZmZpY2UgcHJlbWlzZXMgb24gMDh0aCBNYXJjaC4uLhYvbmV3c25ldmVudHMtZGV0YWlsLzMzZAIHD2QWAmYPFQYWL25ld3NuZXZlbnRzLWRldGFpbC8zMkIvaW1hZ2VzL0V2ZW50cy8zMi9BdmRlbCBJbmRpYSAtIE5ld3MgYW5kIEV2ZW50cyBCYW5uZXIgLSBQTE1TUy5qcGdCQXZkZWwgSW5kaWEgdG8gcGFydGljaXBhdGUgaW4gUExNU1MtMjAyMSAoIFZpcnR1YWwgZXhoaWJpdGlvbikgLi4uEERlY2VtYmVyIDEsIDIwMjFnTWVldCB1cyBhdCBQTE1TUyAyMDIxDQoNCkF2ZGVsIEluZGlhIGlzIGV4Y2l0ZWQgdG8gYW5ub3VuY2Ugb3VyIHBhcnRpY2lwYXRpb24gaW4gdGhlIDh0aCBJbnRlcm5hdGlvbi4uLhYvbmV3c25ldmVudHMtZGV0YWlsLzMyZAIMDxYCHwECCxYWAgEPZBYCZg8VBwExDU9jdG9iZXIsIDIwMjEQL2Jsb2dzLWRldGFpbC8xNj0vaW1hZ2VzL0Jsb2dzLzE2L0FkdmFuY2VkIE1hdGVyaWFscyB1c2VkIGluIEFlcm9zcGFjZSAoMSkuanBnMldoYXQgYXJlIHRoZSBBZHZhbmNlZCBtYXRlcmlhbHMgdXNlZCBpbiBBZXJvc3BhY2U/cTxwPldoZW4gaW52ZXN0aW5nIGluIGFkdmFuY2VkIG1hdGVyaWFscyBmb3IgYWVyb3NwYWNlIGFuZCBhdmlhdGlvbiB1bml0cywgaXQgaXMgd2lzZSB0byBpbnZlc3QgaW4gcmVsaWFibGUgYW5kIGR1EC9ibG9ncy1kZXRhaWwvMTZkAgIPZBYCZg8VBwIxOQlNYXksIDIwMjEQL2Jsb2dzLWRldGFpbC8xNUMvaW1hZ2VzL0Jsb2dzLzE1L1doZXJlIHRvIHB1cmNoYXNlIG5ldyBhaXJjcmFmdCBhbmQgaGVsaWNvcHRlcnMuanBnL1doZXJlIHRvIHB1cmNoYXNlIG5ldyBhaXJjcmFmdCBhbmQgaGVsaWNvcHRlcnM/cTxwPkJlZm9yZSB5b3UgcHJvY2VlZCB3aXRoIGFuIGludmVzdG1lbnQgb2YgdGhpcyBtYWduaXR1ZGUsIHRoZXJlIGFyZSBzZXZlcmFsIHZhcmlhYmxlcyB0aGF0IG9uZSBtdXN0IGZhY3RvciBpbiB3EC9ibG9ncy1kZXRhaWwvMTVkAgMPZBYCZg8VBwEyC01hcmNoLCAyMDIxEC9ibG9ncy1kZXRhaWwvMTE4L2ltYWdlcy9CbG9ncy8xMS9CbG9nIDQgLSBBaXIgQ2hhcnRlciBTZXJ2aWNlcygxKSgxKS5qcGc/VGhpbmdzIHlvdSBzaG91bGQga25vdyBhYm91dCBiZWZvcmUgYm9va2luZyBhbiBBaXIgQ2hhcnRlciBTLi4ucTxwPjxzcGFuIHN0eWxlPSJmb250LXdlaWdodDogNDAwOyI+QXMgc29vbiBhcyBvbmUgY29udGVtcGxhdGVzIHRoZSBkZWNpc2lvbiBvZiBib29raW5nIGEgY2hhcnRlciBmbGlnaHQsIG9uZSBpcyBiEC9ibG9ncy1kZXRhaWwvMTFkAgQPZBYCZg8VBwIyNA5GZWJydWFyeSwgMjAyMQ8vYmxvZ3MtZGV0YWlsLzhQL2ltYWdlcy9CbG9ncy84L1doeSBpcyBpdCBuZWNlc3NhcnkgdG8gaGF2ZSBzZWF0IGNvdmVyIGZvciBhaXJwbGFuZSBzZWF0cygxKS5wbmc6V2h5IGlzIGl0IG5lY2Vzc2FyeSB0byBoYXZlIHNlYXQgY292ZXIgZm9yIGFpcnBsYW5lIHNlYXRzP3E8cD48c3BhbiBzdHlsZT0iZm9udC13ZWlnaHQ6IDQwMDsiPkp1c3QgaG93IHdlIG1ha2Ugc3VyZSB0byBwcm90ZWN0IG91ciBsaXZpbmcgcm9vbSBjb3VjaGVzIGFuZCBjdXNoaW9ucyBhcmUgYWx3YQ8vYmxvZ3MtZGV0YWlsLzhkAgUPZBYCZg8VBwIyNA5GZWJydWFyeSwgMjAyMQ8vYmxvZ3MtZGV0YWlsLzlML2ltYWdlcy9CbG9ncy85L1doaWNoIGNvbXBhbnkgc3VwcGxpZXMgYWlycGxhbmUgaW50ZXJpb3IgZGVzaWduIHBhcnRzKDEpLnBuZzZXaGljaCBjb21wYW55IHN1cHBsaWVzIGFpcnBsYW5lIGludGVyaW9yIGRlc2lnbiBwYXJ0cz9xPHA+PHN0cm9uZz5XaGF0IGFyZSBzb21lIG9mIHRoZSBtYWpvciBmZWF0dXJlcyB0aGF0IHNldCBvdXQgdG8gZGVmaW5lIGFueSBhaXJjcmFmdCBpbnRlcmlvcj88L3N0cm9uZz48L3A+DQo8b2w+DQoPL2Jsb2dzLWRldGFpbC85ZAIGD2QWAmYPFQcCMjQORGVjZW1iZXIsIDIwMjAPL2Jsb2dzLWRldGFpbC82Ki9pbWFnZXMvQmxvZ3MvNi9SaXZldHMgJiBGYXN0ZXJuZXJzKDEpLnBuZx9BIEd1aWRlIG9uIFJpdmV0cyBhbmQgRmFzdGVuZXJzcTxwPjxzcGFuIHN0eWxlPSJmb250LXdlaWdodDogNDAwOyI+SWYgeW91IHdhbnQgdG8ga25vdyBtb3JlIGFib3V0IFJpdmV0cyBhbmQgRmFzdGVuZXJzLCB0aGlzIGd1aWRlIGlzIGZvciB5b3UuPC9zDy9ibG9ncy1kZXRhaWwvNmQCBw9kFgJmDxUHAjI0DkRlY2VtYmVyLCAyMDIwDy9ibG9ncy1kZXRhaWwvNysvaW1hZ2VzL0Jsb2dzLzcvUG93ZXIgT3BlcmF0ZWQgVG9vbHMoMSkucG5nL1doYXQgYXJlIHRoZSBwb3dlciBvcGVyYXRlZCB0b29scyBpbiBBaXJjcmFmdD8gcTxwPjxzcGFuIHN0eWxlPSJmb250LXdlaWdodDogNDAwOyI+QWxsIHRoZSBhaXJjcmFmdHMgbmVlZCB0byBiZSBtYWludGFpbmVkIGZyb20gdGltZSB0byB0aW1lIGFuZCBmb3IgdGhhdCBwb3dlciBvDy9ibG9ncy1kZXRhaWwvN2QCCA9kFgJmDxUHAjE0DU9jdG9iZXIsIDIwMTkPL2Jsb2dzLWRldGFpbC81Fy9pbWFnZXMvQmxvZ3MvNS9BY2cucG5nP1RlY2hub2xvZ2ljYWwgVXBncmFkZTogUmVhbCBHbGFzcyBtaXJyb3JzIGZvciB0aGUgQXZpYXRpb24gSS4uLnE8cD48c3Ryb25nPkxpZ2h0d2VpZ2h0IHNpbHZlciBtaXJyb3JzIGluIGRlbWFuZDwvc3Ryb25nPjxiciAvPkFpci1DcmFmdGdsYXNzIHdhcyByZWNlbnRseSBjb21taXNzaW9uZWQgdG8gc3VwcGx5IA8vYmxvZ3MtZGV0YWlsLzVkAgkPZBYCZg8VBwExC01hcmNoLCAyMDE4Dy9ibG9ncy1kZXRhaWwvMhgvaW1hZ2VzL0Jsb2dzLzIvdWRhbi5qcGciVURBTi0gUmVnaW9uYWwgQ29ubmVjdGl2aXR5IFNjaGVtZXE8cD5JbiAyMDE3LCB0aGUgQ2l2aWwgQXZpYXRpb24gTWluaXN0cnkgZmlyc3QgYW5ub3VuY2VkIHRoZSBVREFOIHNjaGVtZSBmb3IgcmVnaW9uYWwgYWlyIGNvbm5lY3Rpdml0eS4gVURBTiAoVWRlIA8vYmxvZ3MtZGV0YWlsLzJkAgoPZBYCZg8VBwExDkZlYnJ1YXJ5LCAyMDE4Dy9ibG9ncy1kZXRhaWwvMxcvaW1hZ2VzL0Jsb2dzLzMvc2t5LmpwZy5JbnRlcm5ldCBTZWFyY2ggb24gSW5kaWFuIFNraWVzLCBBIFJlYWxpdHkgTm93cTxwPkVhcmx5IHRoaXMgeWVhciBpbiBKYW51YXJ5LCB0aGUgVGVsZWNvbSBSZWd1bGF0b3J5IEF1dGhvcml0eSBvZiBJbmRpYSAoVFJBSSkgY2xlYXJlZCB0aGUgZGVja3MgZm9yIGluLWZsaWdodCBjDy9ibG9ncy1kZXRhaWwvM2QCCw9kFgJmDxUHATENSmFudWFyeSwgMjAxOA8vYmxvZ3MtZGV0YWlsLzQkL2ltYWdlcy9CbG9ncy80L0F2ZGVsLUFkLUZvci13ZWIuanBnQUF2ZGVsIEluZGlh4oCZcyBzdXBwb3J0IHRvIEluZGlhbiBBZXJvc3BhY2UgYW5kIEF2aWF0aW9uIHNlY3RvLi4ucTxwPlByb2dyZXNzIGlzIHVucHJlZGljdGFibGUsIHdoZXRoZXIgZ2xvYmFsIG9yIGNsb3NlciB0byBob21lIGl0IGZvbGxvd3MgYW4gdW5ldmVuIHBhdGguJm5ic3A7IEJ1dCB0aGVyZSZyc3F1bztzDy9ibG9ncy1kZXRhaWwvNGQCDQ8WAh8BAgIWBGYPZBYCZg8VAaUEPHA+VGhhbmsgeW91IGZvciB5b3VyIGNvbmdyYXR1bGF0b3J5IG1lc3NhZ2Ugb24gdGhlIHN1Y2Nlc3NmdWwgbGF1bmNoIG9mIEdTTFYgTWtJSUktTTEvIENoYW5kcmF5YWFuLTIuIEl0IGlzIGluZGVlZCBhIHByb3VkIG1vbWVudCBmb3IgZXZlcnkgSW5kaWFuIGFuZCB3ZSBhdCBJU1JPIHJlZGVkaWNhdGUgb3Vyc2VsdmVzIHRvIHNlcnZlIG91ciBjb3VudHJ5IGFuZCBicmluZyB0aGUgYmVuZWZpdHMgb2YgU3BhY2UgdGVjaG5vbG9neSB0byB0aGUgY29tbW9uIG1hbi48L3A+DQo8cD5JIHRha2UgdGhpcyBvcHBvcnR1bml0eSB0byBhY2tub3dsZWRnZSB0aGUgY29udHJpYnV0aW9uIG9mIEF2ZGVsIEluZGlhIFB2dCBMdGQuIHRvIElTUk8ncyBtaXNzaW9uIGFuZCBsb29rIGZvcndhcmQgdG8geW91ciBjb250aW51ZWQgc3VwcG9ydCBmb3IgYWxsIHlvdXIgZnV0dXJlIHNwYWNlIGVuZGVhdm9ycy48L3A+DQo8cD48c3Ryb25nPkRyLiBLLiBTaXZhbjwvc3Ryb25nPjwvcD4NCjxwPjxzdHJvbmc+Q2hhaXJtYW4sIElTUk88L3N0cm9uZz48L3A+DQo8cD4mbmJzcDs8L3A+ZAIBD2QWAmYPFQG4BTxwPk9uIHRoZSBvY2Nhc2lvbiBvZiBhY2hpZXZpbmcgdGhpcyBzaWduaWZpY2FudCBtaWxlc3RvbmUsIEkgd291bGQgbGlrZSB0byB0aGFuayB5b3UgYW5kIHlvdXIgdGVhbSBmb3IgdGhlIHVuc3RpbnRlZCBzdXBwb3J0LCBjb29wZXJhdGlvbiBhbmQgY29udHJpYnV0aW9uIHJpZ2h0IHRocm91Z2ggdGhlIEthdmVyaSBlbmdpbmUgcHJvZ3JhbSwgd2hpY2ggaGFzIGN1bG1pbmF0ZWQgaW50byB0aGlzIHN1Y2Nlc3MuIEkgZG8gYmVsaWV2ZSB0aGF0IHRoaXMgaXMganVzdCB0aGUgYmVnaW5uaW5nIG9mIGEgbmV3IGVyYSB3aGVyZWluIEdUUkUgd291bGQgYmUgdGhlIHByaW1lIGRlc2lnbiBhbmQgZGV2ZWxvcG1lbnQgY2VudGVyIG9mIGV4Y2VsbGVuY2UgZm9yIGFsbCB0aGUgZ2FzIHR1cmJpbmUgcmVxdWlyZW1lbnRzIG9mIHRoZSBjb3VudHJ5IC0gZm9yIGFlcm8sIG1hcmluZSwgYW5kIGluZHVzdHJpYWwgYXBwbGljYXRpb25zLiBBdCB0aGlzIGp1bmN0dXJlLCB3aGlsZSB0aGFua2luZyB5b3UgcHJvZnVzZWx5LCBJIHNlZWsgeW91ciBjb250aW51ZWQgcGFydGljaXBhdGlvbiBhbmQgc3VwcG9ydCBmb3IgYWxsIHRoZSBwcmVzZW50IGFuZCB0aGUgZnV0dXJlIHByb2plY3RzIG9mJm5ic3A7IEdUUkUuPC9wPg0KPHA+PHN0cm9uZz5TaHJpIFQuIE1vaGFuYSBSYW8sPGJyIC8+RGlyZWN0b3IsIEdUUkUuPC9zdHJvbmc+PC9wPmQCDg8WAh8BAhoWNGYPZBYCZg8VARsvaW1hZ2VzL0NsaWVudHMvOS9hZXF1cy5qcGdkAgEPZBYCZg8VAR4vaW1hZ2VzL0NsaWVudHMvMi9jbGllbnQwMS5qcGdkAgIPZBYCZg8VARkvaW1hZ2VzL0NsaWVudHMvMTAvYmUuanBnZAIDD2QWAmYPFQEbL2ltYWdlcy9DbGllbnRzLzExL2JlbWwuanBnZAIED2QWAmYPFQEdL2ltYWdlcy9DbGllbnRzLzEyL2JoYXJhdC5qcGdkAgUPZBYCZg8VAR8vaW1hZ2VzL0NsaWVudHMvMjkvY2xpZW50MjEuanBnZAIGD2QWAmYPFQEjL2ltYWdlcy9DbGllbnRzLzIwL0RZTkFNSUMtVEVDSC5qcGdkAgcPZBYCZg8VASEvaW1hZ2VzL0NsaWVudHMvMTMvRXVyb2NvcHRlci5qcGdkAggPZBYCZg8VAR8vaW1hZ2VzL0NsaWVudHMvMjEvRkFJVkVMRVkuanBnZAIJD2QWAmYPFQEeL2ltYWdlcy9DbGllbnRzLzQvY2xpZW50MTAuanBnZAIKD2QWAmYPFQEfL2ltYWdlcy9DbGllbnRzLzIyL0dPT0RSSUNILmpwZ2QCCw9kFgJmDxUBHi9pbWFnZXMvQ2xpZW50cy81L2NsaWVudDA5LmpwZ2QCDA9kFgJmDxUBHS9pbWFnZXMvQ2xpZW50cy8xOS9JbmRpYW4uanBnZAIND2QWAmYPFQEeL2ltYWdlcy9DbGllbnRzLzcvY2xpZW50MTYuanBnZAIOD2QWAmYPFQEjL2ltYWdlcy9DbGllbnRzLzI0L0tJTkVDTy1LQU1BTi5qcGdkAg8PZBYCZg8VARwvaW1hZ2VzL0NsaWVudHMvMTUva25vcnIuanBnZAIQD2QWAmYPFQEeL2ltYWdlcy9DbGllbnRzLzMvY2xpZW50MDUuanBnZAIRD2QWAmYPFQEfL2ltYWdlcy9DbGllbnRzLzE0L21haGluZHJhLmpwZ2QCEg9kFgJmDxUBGS9pbWFnZXMvQ2xpZW50cy8yNS9NRC5qcGdkAhMPZBYCZg8VASMvaW1hZ2VzL0NsaWVudHMvMjYvTUlOSVNUUlktREVGLmpwZ2QCFA9kFgJmDxUBGi9pbWFnZXMvQ2xpZW50cy8yNy9OQUwuanBnZAIVD2QWAmYPFQEcL2ltYWdlcy9DbGllbnRzLzE4L25wY2lsLmpwZ2QCFg9kFgJmDxUBGi9pbWFnZXMvQ2xpZW50cy8yOC9SQ0kuanBnZAIXD2QWAmYPFQEjL2ltYWdlcy9DbGllbnRzLzYvcmVsaWFuY2VfbG9nby5qcGdkAhgPZBYCZg8VARsvaW1hZ2VzL0NsaWVudHMvMTcvdGFhbC5qcGdkAhkPZBYCZg8VAR4vaW1hZ2VzL0NsaWVudHMvOC9jbGllbnQwNi5qcGdkAgcPZBYCZg9kFgICAQ8WAh8BAgMWBmYPZBYEZg8VBCcvaW1hZ2VzL0V2ZW50cy80MS9BZXJvIEluZGlhIC0yMiBORS5qcGcWL25ld3NuZXZlbnRzLWRldGFpbC80MStNRUVUIFRIRSBBVkRFTCBJTkRJQSBURUFNIEFUIEFFUk8gSU5ESUEgLi4uCzAxIEphbiAyMDIzZAIBDxUBFi9uZXdzbmV2ZW50cy1kZXRhaWwvNDFkAgEPZBYEZg8VBCwvaW1hZ2VzL0V2ZW50cy80MC9BdmRlbCBJbmRpYSBDcmliTWFzdGVyLnBuZxYvbmV3c25ldmVudHMtZGV0YWlsLzQwLVNlbWluYXIgb24g4oCcU1RBTkxFWSBDcmliTWFzdGVyIC0gQXV0b21hdC4uLgsxNCBPY3QgMjAyMmQCAQ8VARYvbmV3c25ldmVudHMtZGV0YWlsLzQwZAICD2QWBGYPFQQmL2ltYWdlcy9FdmVudHMvMzkvRklBIGFubm91bmNlbWVudC5qcGcWL25ld3NuZXZlbnRzLWRldGFpbC8zOStNZWV0IHRoZSBBdmRlbCBJbmRpYSBUZWFtIGF0IEZhcm5ib3JvdWdoLi4uCzIyIEp1biAyMDIyZAIBDxUBFi9uZXdzbmV2ZW50cy1kZXRhaWwvMzlkZLU1PrAMj6y9OotSS8lEV3fR9fSPnyxfv3lEqm4MfiuT" />
        </div>

        <script type="text/javascript">
            //<![CDATA[
            var theForm = document.forms['form1'];
            if (!theForm) {
                theForm = document.form1;
            }

            function __doPostBack(eventTarget, eventArgument) {
                if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                    theForm.__EVENTTARGET.value = eventTarget;
                    theForm.__EVENTARGUMENT.value = eventArgument;
                    theForm.submit();
                }
            }
            //]]>
        </script>


        <script src="/WebResource.axd?d=q4KDKXRk2olwckV3UdQ6Q-vgN3iS6caqZdfY7qeceHZl6pEciUCL0VBTdxJ-ckbQbLeHol2zkbUlF7StVMxEjP6pS0iBAjrWQu7Ipzug6BU1&amp;t=638259182771233176" type="text/javascript"></script>


        <script src="/ScriptResource.axd?d=GL_Yxg9uHDRMrJfsZayaoz5q2YwkxBsUd1b-lHfUZJnCJ0171pBN2Ppor33AxEFgdbtGyc80hLxJJhzEyxn6YYp0ROGPRX7PHv-k_7GUuNo3FKF62NwNPzEYqMTj-dqJl-l2zyqxRuytczfawBDooA2&amp;t=ffffffffaa73f696" type="text/javascript"></script>
        <script type="text/javascript">
            //<![CDATA[
            if (typeof(Sys) === 'undefined') throw new Error('ASP.NET Ajax client-side framework failed to load.');
            //]]>
        </script>

        <script src="/ScriptResource.axd?d=xEQKxQcZ0rLvgifKUOJTBiKVBUszKEZTmJLQFEBm0A3YBj9kmEnzobVXsE3GrHu8glWaAsrdagTE9Rany5Xft0utzxaXlr8PYUCVkiTUYnUVHcnQfFqIbulEfNgnEg4div3OcqAeU5J0heoDt1NyIw2&amp;t=ffffffffaa73f696" type="text/javascript"></script>
        <div class="aspNetHidden">

            <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8D0E13E6" />
            <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAa1uCup2qu4jjbvmJmyjovtJZWRbFSKiE2JVsPYPAdLbaZTyCw4X9tt2ct5sug9IB7EiVAwT+FA663TsiNWUPFe5rEs/2LZSYd+S7xJ63+IwzNYR0+Wjlk0iVqzHJifAc9NOKh5qUzEn35Kte5ZXGtqK28/Gg8ecvxp1xV0GIt2DA==" />
        </div>
        <script type="text/javascript">
            //<![CDATA[
            Sys.WebForms.PageRequestManager._initialize('ctl00$ScriptManager1', 'form1', [], [], [], 90, 'ctl00');
            //]]>
        </script>
        @include('nav')
        <script>
            function storeCID(id, urlName) {
                if (id != undefined && id != '') {
                    // alert(id);
                    // alert(urlName);
                    var url = '/wsCommon.asmx/StoreCategoryID'
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: "{ CatID: '" + id + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: "true",
                        cache: "false",
                        success: function(msg) {
                            window.location.href = urlName;
                        },
                        Error: function(x, e) {
                            // On Error
                        }
                    });
                }
            }

            function fnSearch(urlName) {
                //  alert("hiii");
                //  alert(urlName);
                var stxt = $("#txtSearch").val();
                if (stxt == undefined || stxt == '') {
                    alert("Enter Search Text");
                    $("#txtSearch").focus();
                    return false;
                } else {
                    var url = '/wsCommon.asmx/SearchMethod'
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: "{ txt: '" + stxt + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: "true",
                        cache: "false",
                        success: function(msg) {
                            window.location.href = urlName;
                        },
                        Error: function(x, e) {
                            // On Error
                        }
                    });
                    return false;
                }
            }
        </script>
        <div>
            <!--%%%%%%%%%%%%%%%%%% Code Added by amol for Pop up %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->
            <!-- Modal -->
            <div class="modal fade" id="covidpop" role="dialog">
                <div class="modal-dialog modal-xl" width="800px">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <strong>Avdel Aviation under its brand ‘AVCARE’ feels exhilarated to announce special VIP Private Jet & Stay packages for the holiday season.</strong>
                            <button type="button" class="close" data-dismiss="modal">
                                &times;</button>
                        </div>
                        <div class="modal-body">
                            <div align="justify">
                                <p>
                                    "AVCARE -Concierge Services" aims to provide exclusive &amp; finest VIP Private Aviation Services across India &amp; worldwide for our beloved clients.
                                    It assists its clients to plan for the much awaited holiday season with a memorable travel experience.

                                </p>
                                <img src="{{url('images/Avcare_collage.jpg')}}" style="width: 50%; float: right; padding-left: 15px; margin-bottom: 15px;" width="100%">
                                <p>
                                    AVCARE customized VIP Private Jet &amp; Stay Packages are best suited for Couples, Family Holidays, Friends Reunion &amp; Group travel to celebrate special occasions.&nbsp;

                                </p>
                                <h5 style="font-weight: bold;">Some of AVCARE packages are:</h5>
                                <ul style="margin-left: 30px; margin-bottom: 30px;">
                                    <li>Maldives &amp; Dubai VIP Charter &amp; Stay packages</li>
                                    <li>Same Day Return Packages within India</li>
                                    <li>Customized Packages</li>
                                    <li>Seat Sharing basis on Private Jets</li>
                                </ul>
                                <a class="btn btn-primary" href="https://avdel.com/avcare/index.html">Click here to Learn More</a>.

                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->
            <section id="main-slider">
                <div id="banner">
                    <div class="row">
                        <div id="homepage_slider" class="carousel slide">
                            <ol class="carousel-indicators">

                                <li data-target="#homepage_slider" data-slide-to="1" class='active'></li>

                                <li data-target="#homepage_slider" data-slide-to="2" class=''></li>

                                <li data-target="#homepage_slider" data-slide-to="3" class=''></li>

                                <li data-target="#homepage_slider" data-slide-to="4" class=''></li>

                                <li data-target="#homepage_slider" data-slide-to="5" class=''></li>

                                <li data-target="#homepage_slider" data-slide-to="6" class=''></li>

                                <li data-target="#homepage_slider" data-slide-to="7" class=''></li>
                            </ol>
                            <div class="carousel-inner">

                                <div data-slide="1" class='item active'>
                                    <img alt="" src="{{url('images/Masthead/7/banner.jpg')}}" width="100%">
                                    <div class="slidercapt wow slideInLeft" data-wow-duration="1.2s" data-wow-delay="0s">
                                        <h1></h1>
                                    </div>
                                </div>

                                <div data-slide="2" class='item '>
                                    <img alt="" src="{{url('images/Masthead/6/AIRCRAFTCHARTERS.jpg')}}" width="100%">
                                    <div class="slidercapt wow slideInLeft" data-wow-duration="1.2s" data-wow-delay="0s">
                                        <h1>Aircraft Charters <br />& Concierge Services</h1>
                                    </div>
                                </div>

                                <div data-slide="3" class='item '>
                                    <img alt="" src="{{url('images/Masthead/5/background.jpg')}}" width="100%">
                                    <div class="slidercapt wow slideInLeft" data-wow-duration="1.2s" data-wow-delay="0s">
                                        <h1>New Day<br />New Possibilities</h1>
                                    </div>
                                </div>

                                <div data-slide="4" class='item '>
                                    <img alt="" src="{{url('images/Masthead/4/slider2.jpg')}}" width="100%">
                                    <div class="slidercapt wow slideInLeft" data-wow-duration="1.2s" data-wow-delay="0s">
                                        <h1>Small Parts, <br />Big Technology</h1>
                                    </div>
                                </div>

                                <div data-slide="5" class='item '>
                                    <img alt="" src="{{url('images/Masthead/3/slider3.jpg')}}" width="100%">
                                    <div class="slidercapt wow slideInLeft" data-wow-duration="1.2s" data-wow-delay="0s">
                                        <h1>Own your peace <br />in the Sky </h1>
                                    </div>
                                </div>

                                <div data-slide="6" class='item '>
                                    <img alt="" src="{{url('images/Masthead/2/slider4.jpg')}}" width="100%">
                                    <div class="slidercapt wow slideInLeft" data-wow-duration="1.2s" data-wow-delay="0s">
                                        <h1>Delivering Manufacturing Solutions <br> for more than 6 Decades</h1>
                                    </div>
                                </div>

                                <div data-slide="7" class='item '>
                                    <img alt="" src="{{url('images/Masthead/1/slider5.jpg')}}" width="100%">
                                    <div class="slidercapt wow slideInLeft" data-wow-duration="1.2s" data-wow-delay="0s">
                                        <h1>We Deliver <br> to Connect</h1>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control left" href="#homepage_slider" data-slide="prev">&lsaquo;</a> <a class="carousel-control right" href="#homepage_slider" data-slide="next">&rsaquo;</a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Responsive image with left -->
            <section id="responsive" class="padding ">
                <div class="container">

                    <div class="row">
                        <div class="r-test">
                            <h3 class="magin30 wow fadeInDown" data-wow-duration="1.4s" data-wow-delay="0s">Who we are</h3>

                            <div class="wow fadeInUp" data-wow-duration="1.4s" data-wow-delay="0s">
                                <p>Avdel (India) Pvt. Ltd was established in 1961 and is a part of a well-established group of companies with interests</p>

                                <p>in various business sectors including Aerospace, Aviation, Automotive, and Rail amongst others. Avdel India is</p>

                                <p>an AS 9120 B &amp; ISO 9001:2015 Certified company. With headquarters in Mumbai (India), Avdel India has regional offices</p>

                                <p>in Bengaluru (the aerospace hub of the country) and sales office in Hyderabad, and Trivandrum.&nbsp;</p>

                                <p>&nbsp;</p>
                                <quillbot-extension-portal></quillbot-extension-portal>
                            </div>
                            <div class="readmore wow fadeInUp" data-wow-duration="1.4s" data-wow-delay="0s">
                                <a href="/about-us/profile">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="row number-sectionbg">
                <div class="container">
                    <div class="number-section-padding">
                        <div class="row divide-number-section maincounter">
                            <div class="col-sm-12">
                                <div class="col-sm-3 col-md-3 col-xs-12 col-lg-3">
                                    <div class="outsidecircle wow fadeInDown" data-wow-duration="1.5s" data-wow-delay="0s">
                                        <div id="circle">
                                            <div class="mainimgs">
                                                <img src="{{url('images/Experience-Icon.png')}}">
                                            </div>
                                            <span class="count">50</span> <span class="pluss">+</span>
                                            <p>
                                                Years of Experience
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-xs-12 col-lg-3">
                                    <div class="outsidecircle wow fadeInDown" data-wow-duration="1.8s" data-wow-delay="0s">
                                        <div id="circle">
                                            <div class="mainimgs">
                                                <img src="{{url('images/Partners-Icon.png')}}">
                                            </div>
                                            <span class="count">25</span> <span class="pluss">+</span>
                                            <p>
                                                Partners
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-xs-12 col-lg-3">
                                    <div class="outsidecircle wow fadeInDown" data-wow-duration="2.2s" data-wow-delay="0s">
                                        <div id="circle">
                                            <div class="mainimgs">
                                                <img src="{{url('images/customer.png')}}">
                                            </div>
                                            <span class="count">100</span> <span class="pluss">+</span>
                                            <p>
                                                Customers
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-xs-12 col-lg-3">
                                    <div class="outsidecircle wow fadeInDown" data-wow-duration="2.8s" data-wow-delay="0s">
                                        <div id="circle">
                                            <div class="mainimgs">
                                                <img src="{{url('images/Products-Icon.png')}}">
                                            </div>
                                            <span class="count">100000</span> <span class="pluss">+</span>
                                            <p>
                                                Products
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="container">
                <div class="divisions">
                    <div class=" text-center">
                        <h2 class="heading wow fadeInDown" data-wow-duration="1.2s" data-wow-delay="0s">Our Divisions </h2>
                    </div>
                    <div>
                        <div id="atd" class="divblock">
                            <img src="{{url('images/Home/ATD/firstimage.jpg')}}" id="ContentPlaceHolder1_imgAtdFull" class="fullim" />
                            <div class="row">
                                <div class="col-md-6 tabswitcher">

                                    <figure class="effect-apollo">
                                        <img src="{{url('images/Home/ATD/ATD_thumbnail.jpg')}}" id="ContentPlaceHolder1_imgAtdhalf1" alt="img18" />
                                        <figcaption>
                                            <h2>ATD <br />Aerospace, Transportation and Defense Division</h2>
                                            <a href="#" class="atdclick"></a>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-apollo secondefct" style="margin-left:10px;">
                                        <img src="{{url('images/Home/Aviation/Aviation_thumbnail.jpg')}}" id="ContentPlaceHolder1_imgAviationhalf1" alt="img22" />
                                        <figcaption>
                                            <h2>Aviation <br>Division</h2>
                                            <a class="avtclick" href="#"></a>
                                        </figcaption>
                                    </figure>

                                </div>
                                <div class="datainfo col-md-6 col-sm-12">
                                    <h2>Aerospace, Transportation & Defence</h2>

                                    <p>Avdel India- ATD Division was set-up to warehouse and supply fasteners and other hardware including mechanical control cables, and to also supply Advanced Composite Materials.</p>
                                    <p>With Distribution and representation for some of the world&rsquo;s leading brands for all the above products, Avdel India has emerged as a leading supplier to its customers for the same. Not just for the Aerospace segment but also for Defence, Rail, Marine &amp; Alternate Energy Industries.</p>
                                    <p class="readmore">
                                        <a href="/divisions/atd" style="margin:0;">Read More</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div id="aviat" class="divblock">
                            <img src="{{url('images/Home/Aviation/secondimages.jpg')}}" id="ContentPlaceHolder1_imgAviationFull" class="fullim" />
                            <div class="row">
                                <div class="col-md-6 tabswitcher">

                                    <figure class="effect-apollo">
                                        <img src="{{url('images/Home/ATD/ATD_thumbnail.jpg')}}" id="ContentPlaceHolder1_imgAtdhalf2" alt="img18" />
                                        <figcaption>
                                            <h2>ATD <br />Aerospace, Transportation and Defense Division</h2>
                                            <a href="#" class="atdclick"></a>
                                        </figcaption>
                                    </figure>
                                    <figure class="effect-apollo secondefct" style="margin-left:10px;">
                                        <img src="{{url('images/Home/Aviation/Aviation_thumbnail.jpg')}}" id="ContentPlaceHolder1_imgAviationhalf2" alt="img22" />
                                        <figcaption>
                                            <h2>Aviation <br>Division</h2>
                                            <a class="avtclick" href="#"></a>
                                        </figcaption>
                                    </figure>


                                </div>
                                <div class="datainfo col-md-6">
                                    <h2>Aviation</h2>

                                    <p>Avdel Aviation caters to the needs of the growing Commercial Aviation and MRO segment with a special focus on Business (General)&nbsp; Aviation in India. Avdel along with their partners provide Trip Management Services, Business Jet Acquisition &amp; Sales Consultancy, Aircraft Interior refurbishment services, Supply of Aviation grade carpets &amp; surface covering materials, Leather &amp; sheepskins, Aviation fabrics, Mirrors &amp; Glass panels and Metal &amp; Plastic Coatings.</p>
                                    <p id="readmores" class="readmore">
                                        <a href="/divisions/aviation" style="margin:0;">Read More</a>
                                    </p>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
                <!-- ia-container -->
            </section>
            <!-- Latest Publications -->
            <section id="publication" class="section-padding padding">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h2 class="heading wow fadeInDown" data-wow-duration="1.6s" data-wow-delay="0s">NEWS & EVENTS</h2>
                        </div>
                    </div>
                    <div class="row wow fadeInUp" data-wow-duration="1.8s" data-wow-delay="0s">
                        <div id="publication-slider" class="owl-carousel">

                            <div class="item">
                                <div class="image"> <a href="/newsnevents-detail/41"><img src="{{url('images/Events/41/Aero India -22 NE.jpg')}}" alt="publication Image"></a> </div>
                                <h4>MEET THE AVDEL INDIA TEAM AT AERO INDIA SCHEDULED FROM 13-17 TH...</h4>
                                <h5>January 1, 2023 </h5>
                                <hr />
                                <p> Avdel India shall be participating at Aero India scheduled from 13-17th February 2023 at Yelahanka A...</p>
                                <a href="/newsnevents-detail/41">read more</a>
                            </div>

                            <div class="item">
                                <div class="image"> <a href="/newsnevents-detail/40"><img src="{{url('images/Events/40/Avdel India CribMaster.png')}}" alt="publication Image"></a> </div>
                                <h4>Seminar on “STANLEY CribMaster - Automated Inventory Dispensing...</h4>
                                <h5>October 14, 2022 </h5>
                                <hr />
                                <p> Avdel India along with CribMaster is conducting an Online Seminar on Thursday-&nbsp;November 03, 202...</p>
                                <a href="/newsnevents-detail/40">read more</a>
                            </div>

                            <div class="item">
                                <div class="image"> <a href="/newsnevents-detail/39"><img src="{{url('images/Events/39/FIA announcement.jpg')}}" alt="publication Image"></a> </div>
                                <h4>Meet the Avdel India Team at Farnborough International Air Show...</h4>
                                <h5>June 22, 2022 </h5>
                                <hr />
                                <p> &nbsp;

                                    Avdel India Senior management team shall be present at Farnborough International Air show ...</p>
                                <a href="/newsnevents-detail/39">read more</a>
                            </div>

                            <div class="item">
                                <div class="image"> <a href="/newsnevents-detail/37"><img src="{{url('images/Events/37/NAMS AWARD2.jpg')}}" alt="publication Image"></a> </div>
                                <h4> Avdel India participation at National Aerospace Manufacturing ...</h4>
                                <h5>May 25, 2022 </h5>
                                <hr />
                                <p> Avdel India had an active participation and co-sponsored&nbsp;the National Aerospace Manufacturing S...</p>
                                <a href="/newsnevents-detail/37">read more</a>
                            </div>

                            <div class="item">
                                <div class="image"> <a href="/newsnevents-detail/36"><img src="{{url('images/Events/36/SALES CONFAvdel new event image 2022-04.jpg')}}" alt="publication Image"></a> </div>
                                <h4>Avdel India Sales Conference & Workshop.</h4>
                                <h5>April 20, 2022 </h5>
                                <hr />
                                <p> Avdel India scheduled their &quot;Sales Conference &amp; Workshop&quot; at Fountain Head,&nbsp;Aliba...</p>
                                <a href="/newsnevents-detail/36">read more</a>
                            </div>

                            <div class="item">
                                <div class="image"> <a href="/newsnevents-detail/34"><img src="{{url('images/Events/34/SPORTS Avdel new.jpg')}}" alt="publication Image"></a> </div>
                                <h4>Avdel Annual Sports Meet-2022</h4>
                                <h5>March 13, 2022 </h5>
                                <hr />
                                <p> Avdel India organized Annual Sports Event on 13- March-2022 at Oval Maidaan Mumbai for all of its st...</p>
                                <a href="/newsnevents-detail/34">read more</a>
                            </div>

                            <div class="item">
                                <div class="image"> <a href="/newsnevents-detail/33"><img src="{{url('images/Events/33/WOMENS Avdel new event.jpg')}}" alt="publication Image"></a> </div>
                                <h4>Avdel celebrates "International Womens Day" at its facility.</h4>
                                <h5>March 8, 2022 </h5>
                                <hr />
                                <p> Avdel India celebrated &quot;Internatonal Womens Day&quot; at its&nbsp;office premises on 08th March...</p>
                                <a href="/newsnevents-detail/33">read more</a>
                            </div>

                            <div class="item">
                                <div class="image"> <a href="/newsnevents-detail/32"><img src="{{url('images/Events/32/Avdel India - News and Events Banner - PLMSS.jpg')}}" alt="publication Image"></a> </div>
                                <h4>Avdel India to participate in PLMSS-2021 ( Virtual exhibition) ...</h4>
                                <h5>December 1, 2021 </h5>
                                <hr />
                                <p> Meet us at PLMSS 2021

                                    Avdel India is excited to announce our participation in the 8th Internation...</p>
                                <a href="/newsnevents-detail/32">read more</a>
                            </div>

                        </div>
                    </div>
            </section>
            <section id="publications" class="section-padding padding">
                <div class="container">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h2 class="heading wow fadeInDown" data-wow-duration="1.6s" data-wow-delay="0s" style="color:#fff;">Blogs</h2>
                            </div>
                        </div>
                        <div class="row wow fadeInUp" data-wow-duration="1.8s" data-wow-delay="0s">
                            <div id="publication-sliders" class="owl-carousel">

                                <div class="item">
                                    <div class="image">
                                        <div class="blogdate">
                                            <h3>1</h3>
                                            <h4>October, 2021</h4>
                                        </div><a href="/blogs-detail/16"><img src="{{url('images/Blogs/16/Advanced Materials used in Aerospace (1).jpg')}}" alt="publication Image"></a>
                                    </div>
                                    <h4>What are the Advanced materials used in Aerospace?</h4>
                                    <p>When investing in advanced materials for aerospace and aviation units, it is wise to invest in reliable and du
                                    <p> <a href="/blogs-detail/16">read more</a></p>
                                </div>

                                <div class="item">
                                    <div class="image">
                                        <div class="blogdate">
                                            <h3>19</h3>
                                            <h4>May, 2021</h4>
                                        </div><a href="/blogs-detail/15"><img src="{{url('images/Blogs/15/Where to purchase new aircraft and helicopters.jpg')}}" alt="publication Image"></a>
                                    </div>
                                    <h4>Where to purchase new aircraft and helicopters?</h4>
                                    <p>Before you proceed with an investment of this magnitude, there are several variables that one must factor in w
                                    <p> <a href="/blogs-detail/15">read more</a></p>
                                </div>

                                <div class="item">
                                    <div class="image">
                                        <div class="blogdate">
                                            <h3>2</h3>
                                            <h4>March, 2021</h4>
                                        </div><a href="/blogs-detail/11"><img src="{{url('images/Blogs/11/Blog 4 - Air Charter Services(1)(1).jpg')}}" alt="publication Image"></a>
                                    </div>
                                    <h4>Things you should know about before booking an Air Charter S...</h4>
                                    <p><span style="font-weight: 400;">As soon as one contemplates the decision of booking a charter flight, one is b
                                            <p> <a href="/blogs-detail/11">read more</a></p>
                                </div>

                                <div class="item">
                                    <div class="image">
                                        <div class="blogdate">
                                            <h3>24</h3>
                                            <h4>February, 2021</h4>
                                        </div><a href="/blogs-detail/8"><img src="{{url('images/Blogs/8/Why is it necessary to have seat cover for airplane seats(1).png')}}" alt="publication Image"></a>
                                    </div>
                                    <h4>Why is it necessary to have seat cover for airplane seats?</h4>
                                    <p><span style="font-weight: 400;">Just how we make sure to protect our living room couches and cushions are alwa
                                            <p> <a href="/blogs-detail/8">read more</a></p>
                                </div>

                                <div class="item">
                                    <div class="image">
                                        <div class="blogdate">
                                            <h3>24</h3>
                                            <h4>February, 2021</h4>
                                        </div><a href="/blogs-detail/9"><img src="{{url('images/Blogs/9/Which company supplies airplane interior design parts(1).png')}}" alt="publication Image"></a>
                                    </div>
                                    <h4>Which company supplies airplane interior design parts?</h4>
                                    <p><strong>What are some of the major features that set out to define any aircraft interior?</strong></p>
                                    <ol>

                                        <p> <a href="/blogs-detail/9">read more</a></p>
                                </div>

                                <div class="item">
                                    <div class="image">
                                        <div class="blogdate">
                                            <h3>24</h3>
                                            <h4>December, 2020</h4>
                                        </div><a href="/blogs-detail/6"><img src="{{url('images/Blogs/6/Rivets & Fasterners(1).png')}}" alt="publication Image"></a>
                                    </div>
                                    <h4>A Guide on Rivets and Fasteners</h4>
                                    <p><span style="font-weight: 400;">If you want to know more about Rivets and Fasteners, this guide is for you.</s> <p> <a href="/blogs-detail/6">read more</a></p>
                                </div>

                                <div class="item">
                                    <div class="image">
                                        <div class="blogdate">
                                            <h3>24</h3>
                                            <h4>December, 2020</h4>
                                        </div><a href="/blogs-detail/7"><img src="{{url('images/Blogs/7/Power Operated Tools(1).png')}}" alt="publication Image"></a>
                                    </div>
                                    <h4>What are the power operated tools in Aircraft? </h4>
                                    <p><span style="font-weight: 400;">All the aircrafts need to be maintained from time to time and for that power o
                                            <p> <a href="/blogs-detail/7">read more</a></p>
                                </div>

                                <div class="item">
                                    <div class="image">
                                        <div class="blogdate">
                                            <h3>14</h3>
                                            <h4>October, 2019</h4>
                                        </div><a href="/blogs-detail/5"><img src="{{url('images/Blogs/5/Acg.png')}}" alt="publication Image"></a>
                                    </div>
                                    <h4>Technological Upgrade: Real Glass mirrors for the Aviation I...</h4>
                                    <p><strong>Lightweight silver mirrors in demand</strong><br />Air-Craftglass was recently commissioned to supply
                                    <p> <a href="/blogs-detail/5">read more</a></p>
                                </div>

                                <div class="item">
                                    <div class="image">
                                        <div class="blogdate">
                                            <h3>1</h3>
                                            <h4>March, 2018</h4>
                                        </div><a href="/blogs-detail/2"><img src="{{url('images/Blogs/2/udan.jpg')}}" alt="publication Image"></a>
                                    </div>
                                    <h4>UDAN- Regional Connectivity Scheme</h4>
                                    <p>In 2017, the Civil Aviation Ministry first announced the UDAN scheme for regional air connectivity. UDAN (Ude
                                    <p> <a href="/blogs-detail/2">read more</a></p>
                                </div>

                                <div class="item">
                                    <div class="image">
                                        <div class="blogdate">
                                            <h3>1</h3>
                                            <h4>February, 2018</h4>
                                        </div><a href="/blogs-detail/3"><img src="{{url('images/Blogs/3/sky.jpg')}}" alt="publication Image"></a>
                                    </div>
                                    <h4>Internet Search on Indian Skies, A Reality Now</h4>
                                    <p>Early this year in January, the Telecom Regulatory Authority of India (TRAI) cleared the decks for in-flight c
                                    <p> <a href="/blogs-detail/3">read more</a></p>
                                </div>

                                <div class="item">
                                    <div class="image">
                                        <div class="blogdate">
                                            <h3>1</h3>
                                            <h4>January, 2018</h4>
                                        </div><a href="/blogs-detail/4"><img src="{{url('images/Blogs/4/Avdel-Ad-For-web.jpg')}}" alt="publication Image"></a>
                                    </div>
                                    <h4>Avdel India’s support to Indian Aerospace and Aviation secto...</h4>
                                    <p>Progress is unpredictable, whether global or closer to home it follows an uneven path.&nbsp; But there&rsquo;s
                                    <p> <a href="/blogs-detail/4">read more</a></p>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12 text-center">
                            <h2 class="heading wow fadeInDown" data-wow-duration="1.6s" data-wow-delay="0s" style="color:#fff;">Testimonials</h2>
                        </div>
                        <section id="testinomial" class="padding50 wow fadeInUp" data-wow-duration="1.8s" data-wow-delay="0s">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <!--<h2 class="heading">our happy clients</h2>-->
                                    <div id="testinomial-slider" class="owl-carousel">

                                        <div class="item">
                                            <p>Thank you for your congratulatory message on the successful launch of GSLV MkIII-M1/ Chandrayaan-2. It is indeed a proud moment for every Indian and we at ISRO rededicate ourselves to serve our country and bring the benefits of Space technology to the common man.</p>
                                            <p>I take this opportunity to acknowledge the contribution of Avdel India Pvt Ltd. to ISRO's mission and look forward to your continued support for all your future space endeavors.</p>
                                            <p><strong>Dr. K. Sivan</strong></p>
                                            <p><strong>Chairman, ISRO</strong></p>
                                            <p>&nbsp;</p>
                                        </div>

                                        <div class="item">
                                            <p>On the occasion of achieving this significant milestone, I would like to thank you and your team for the unstinted support, cooperation and contribution right through the Kaveri engine program, which has culminated into this success. I do believe that this is just the beginning of a new era wherein GTRE would be the prime design and development center of excellence for all the gas turbine requirements of the country - for aero, marine, and industrial applications. At this juncture, while thanking you profusely, I seek your continued participation and support for all the present and the future projects of&nbsp; GTRE.</p>
                                            <p><strong>Shri T. Mohana Rao,<br />Director, GTRE.</strong></p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
            <!------- CUSTOMERS --------->
            <div class="container">
                <div class="row mar-b-50 our-clients">
                    <div class="col-md-12 text-center">
                        <h2 class="heading wow fadeInDown" data-wow-duration="1.6s" data-wow-delay="0s">OUR CLIENTS</h2>
                    </div>
                    <div class="col-md-12">
                        <div class="outside">
                            <p>
                                <span id="slider-prev"></span>| <span id="slider-next"></span>
                            </p>
                        </div>
                        <div id="clients-sliders" class="owl-carousel">

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/9/aequs.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/2/client01.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/10/be.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/11/beml.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/12/bharat.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/29/client21.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/20/DYNAMIC-TECH.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/13/Eurocopter.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/21/FAIVELEY.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/4/client10.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/22/GOODRICH.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/5/client09.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/19/Indian.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/7/client16.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/24/KINECO-KAMAN.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/15/knorr.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/3/client05.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/14/mahindra.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/25/MD.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/26/MINISTRY-DEF.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/27/NAL.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/18/npcil.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/28/RCI.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/6/reliance_logo.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/17/taal.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                            <div class="item">
                                <a href="#" target="_blank">
                                    <img src="{{url('images/Clients/8/client06.jpg')}}" width="100%" alt="Image">
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(window).scroll(function() {
                    var hT = $('#circle').offset().top,
                        hH = $('#circle').outerHeight(),
                        wH = $(window).height(),
                        wS = $(this).scrollTop();
                    console.log((hT - wH), wS);
                    if (wS > (hT + hH - wH)) {
                        $('.count').each(function() {
                            $(this).prop('Counter', 0).animate({
                                Counter: $(this).text()
                            }, {
                                duration: 900,
                                easing: 'swing',
                                step: function(now) {
                                    $(this).text(Math.ceil(now));
                                }
                            });
                        });
                        {
                            $('.count').removeClass('count').addClass('counted');
                        };
                    }
                });

                // without this script, the slider doesn't start on it's own:
                ! function($) {
                    $(function() {
                        $('#homepage_slider').carousel()
                    })
                }(window.jQuery)

                $(function() {
                    //caches a jQuery object containing the header element
                    $('.atdclick').click(function() {
                        $('#aviat').hide();
                        $('#atd').show();

                        return false;
                    });

                    $('.avtclick').click(function() {
                        $('#atd').hide();
                        $('#aviat').show();
                        $('#readmores').show();

                        return false;
                    });
                    $("#Menu1").addClass("active");
                });
            </script>
            <script src="{{url('assets/owlcarousel/owl.carousel.js')}}"></script>

        </div>
@include('footer')
</body>

</html>