@include('head')
@include('nav')

<script>
    function storeCID(id, urlName) {
        if (id != undefined && id != '') {
            // alert(id);
            // alert(urlName);
            var url = '/wsCommon.asmx/StoreCategoryID'
            $.ajax({
                type: "POST",
                url: url,
                data: "{ CatID: '" + id + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: "true",
                cache: "false",
                success: function(msg) {
                    window.location.href = urlName;
                },
                Error: function(x, e) {
                    // On Error
                }
            });
        }
    }

    function fnSearch(urlName) {
        //  alert("hiii");
        //  alert(urlName);
        var stxt = $("#txtSearch").val();
        if (stxt == undefined || stxt == '') {
            alert("Enter Search Text");
            $("#txtSearch").focus();
            return false;
        } else {
            var url = '/wsCommon.asmx/SearchMethod'
            $.ajax({
                type: "POST",
                url: url,
                data: "{ txt: '" + stxt + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: "true",
                cache: "false",
                success: function(msg) {
                    window.location.href = urlName;
                },
                Error: function(x, e) {
                    // On Error
                }
            });
            return false;
        }
    }
</script>


<div>

    <div id="companyprofile" class="breadcrumbs" style="background-image: url('{{ asset("images/UpdatePages/15/CAREER.jpg") }}')">
        <div class="row breadinn">
            <div class="col-md-9 pad0">
                <div class="mainbread">
                    <ol class="breadcrumb">
                        <li><a href="/home">Home</a></li>
                        <li class="active">Careers</li>
                    </ol>
                    <h2>Careers</h2>
                </div>
            </div>
            <div class="col-md-3 topspace50">
            </div>
        </div>
    </div>
    <section id="responsive" class="innerpadding">
        <div class="container">

            <div class="row">
                <div class="col-md-9">
                    <div class="maintitlepage">
                        <h2></h2>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">

                            </div>
                        </div>

                        <div class="companyabout" id="mainid">
                        @foreach($pages as $page)
                    @if($page->page_name == 'Careers')
                    {!! $page->description !!}
                    @endif
                    @endforeach
                        </div>

                        <div class="companyabout" id="careerform">
                            <div class="row">
                                <div class="col-md-12">

                                </div>
                            </div>
                            <div id="dvContent" class="creerform">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Select Job Position</label>
                                            <select class="div-toggle form-control careerforms" name="jobposi" data-target=".my-info-1">
                                                <option value="12" data-show=".title12">Executive –Assistant to Director, Location: Mumbai</option>

                                                <option value="10" data-show=".title10">Manager - Marketing</option>

                                                <option value="9" data-show=".title9">Manager-Sales. (Location: Mumbai)</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="maintitlepage my-info-1">

                                    <div class="title12 hide">
                                        <h2>Executive –Assistant to Director, Location: Mumbai</h2>
                                        <p>This is a full-time on-site role for an Executive Assistant to Director and Shareholder of The Bulchandani Group of companies. The Executive Assistant will be responsible for executive administrative assistance, executive support, expense reports, diary management, and administrative assistance to the Director.</p>

                                        <p><strong>Qualifications</strong></p>

                                        <ul>
                                            <li>Experience in executive administrative assistance and executive support</li>
                                            <li>Proficiency in expense reports and diary management</li>
                                            <li>Excellent administrative assistance skills</li>
                                            <li>Exceptional communication and interpersonal skills</li>
                                            <li>Ability to work in a fast-paced environment with attention to detail</li>
                                            <li>B.Com or other bachelor&#39;s degree</li>
                                            <li>Experience in Executive Administration</li>
                                            <li>Proficient in Microsoft Office Suite and other software applications.
                                                <p>&nbsp;</p>
                                            </li>
                                        </ul>

                                    </div>

                                    <div class="title10 hide">
                                        <h2>Manager - Marketing</h2>
                                        <h4>JOB PROFILE:</h4>
                                        <ul>
                                            <li>Conduct market research to find answers about consumer requirements, habits and trends</li>
                                            <li>Brainstorm and develop ideas for creative marketing campaigns</li>
                                            <li>Assist in online/offline marketing activities by demonstrating expertise in various areas (content development and optimization, advertising, events planning etc.)</li>
                                            <li>Liaise with external vendors to execute promotional events and campaigns</li>
                                            <li>Collaborate with marketing and other professionals to coordinate brand awareness and marketing efforts</li>
                                            <li>Plan and execute initiatives to reach the target audience through appropriate channels (social media, e-mail etc.)</li>
                                            <li>Assist in analyzing marketing data (campaign results, conversion rates, traffic etc.) to help shape future marketing strategies</li>
                                        </ul>
                                        <h4>DESIRED ATTRIBUTES:</h4>
                                        <p>Age: Upto 25 Years</p>
                                        <ul>
                                            <li>Bachelor in marketing, communications or equivalent</li>
                                            <li>Proven experience as marketing specialist or similar role</li>
                                            <li>Thorough understanding of marketing elements (including traditional and digital marketing such as SEO/Social media etc.) and market research methods</li>
                                            <li>Demonstrable experience in marketing data analytics and tools</li>
                                            <li>Solid computer skills, including MS Office, marketing software (Adobe Creative Suite &amp; CRM) and applications (Web analytics, Google Adwords etc.)</li>
                                            <li>Knowledge of HTML, CSS and web development tools desired</li>
                                            <li>Well-organized and detail oriented</li>
                                            <li>Exceptional communication and writing skills</li>
                                            <li>Commercial awareness partnered with a creative mind</li>
                                        </ul>
                                    </div>

                                    <div class="title9 hide">
                                        <h2>Manager-Sales. (Location: Mumbai)</h2>
                                        <h4>Job Profile:</h4>
                                        <ul>
                                            <li>Generation of RFQ&rsquo;s</li>
                                            <li>Product Presentation &amp; Demos</li>
                                            <li>Follow up with potential customers</li>
                                            <li>Submission of Quote &amp; Negotiation</li>
                                            <li>Confirming Order &amp; Payment Collection</li>
                                            <li>Maintaining Customer Relationships</li>
                                            <li>Responsible for achieving sales targets</li>
                                            <li>Addressing customer&rsquo;s queries</li>
                                            <li>Making sales call/visits</li>
                                            <li>Identifying new customers and new target areas</li>
                                        </ul>
                                        <p>&nbsp;</p>
                                        <h4>Desired Attributes:</h4>
                                        <p>Age: Upto 35 Years</p>
                                        <ul>
                                            <li>Experience of industrial selling preferably in Fasteners or Heavy/ Light Engg, Industry background not essential.</li>
                                            <li>Aerospace industry familiarity would be an added advantage</li>
                                            <li>Fluent in English</li>
                                            <li>Good communication &amp; negotiation skills</li>
                                            <li>Extrovert / Dynamic personality</li>
                                            <li>Physically fit &amp; found of traveling</li>
                                            <li>Computer Savvy &ndash; PPT. MS office etc</li>
                                            <li>Good presentation skill</li>
                                            <li>Analytical skills with sound technical knowledge</li>
                                        </ul>
                                    </div>

                                    <input type="hidden" name="ctl00$ContentPlaceHolder1$hidCareerID" id="hidCareerID" />
                                    <input type="hidden" name="ctl00$ContentPlaceHolder1$hidCareerTitle" id="hidCareerTitle" />
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input name="ctl00$ContentPlaceHolder1$txtFirstName" type="text" id="ContentPlaceHolder1_txtFirstName" class="form-control careerforms" placeholder="First Name" required="" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input name="ctl00$ContentPlaceHolder1$txtLastName" type="text" id="ContentPlaceHolder1_txtLastName" class="form-control careerforms" placeholder="Last Name" required="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Phone Number</label>
                                            <input name="ctl00$ContentPlaceHolder1$txtPhone" type="text" id="ContentPlaceHolder1_txtPhone" class="form-control careerforms txtNumericonly" placeholder="Phone Number" required="" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Emailed Address</label>
                                            <input name="ctl00$ContentPlaceHolder1$txtEmail" type="text" id="ContentPlaceHolder1_txtEmail" class="form-control careerforms" placeholder="Emailed Address" required="" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Current CTC</label>
                                            <input name="ctl00$ContentPlaceHolder1$txtCurrentCTC" type="text" id="ContentPlaceHolder1_txtCurrentCTC" class="form-control careerforms" placeholder="Current CTC" required="" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Expected CTC</label>
                                            <input name="ctl00$ContentPlaceHolder1$txtExpectedCTC" type="text" id="ContentPlaceHolder1_txtExpectedCTC" class="form-control careerforms" placeholder="Expected CTC" required="" />

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4" style="padding-left:0;">
                                    <div class="form-group">
                                        <label>Upload Resume</label>
                                        <input type="file" name="ctl00$ContentPlaceHolder1$flnResume" id="ContentPlaceHolder1_flnResume" placeholder="Upload Resume" required="" />
                                        <span id="ContentPlaceHolder1_RegularExpressionValidator2" style="color:Red;visibility:hidden;">Please upload resume in .doc or .docx or .pdf</span>
                                    </div>
                                </div>
                                <div class="col-md-4" style="text-align:right; margin-top:15px;">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <input type="submit" name="ctl00$ContentPlaceHolder1$btnSubmit" value="Submit" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btnSubmit&quot;, &quot;&quot;, true, &quot;vResume&quot;, &quot;&quot;, false, false))" id="ContentPlaceHolder1_btnSubmit" class="btn btn-primary submitbtn" />
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-3">
                    <div id="newsevents">
                        <div class="footer-item recent-posts">
                            <h4>News &amp; Events</h4>

                            <div class="footer__widget">
                                <div class="footer__innner">
                                    <div class="ftr__latest__post">

                                        <div class="single__ftr__post d-flex">
                                            <div class="ftr__post__thumb">
                                                <a href="newsnevents.html">
                                                    <img src="{{url('images/Events/41/Aero India -22 NE.jpg')}}" alt="post images" style="width: 78px; height: 78px;">
                                                </a>
                                            </div>
                                            <div class="ftr__post__details">
                                                <h6>
                                                    <a href="/newsnevents-detail/41">
                                                        MEET THE AVDEL INDIA TEAM AT AERO INDIA ...</a>
                                                </h6>
                                                <span><i class="fa fa-calendar"></i>
                                                    01 Jan 2023</span>

                                                <div class="readsmore">
                                                    <a href="/newsnevents-detail/41">Read More <i class="fa fa-angle-double-right"></i></a>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="readsmore" style="text-align: left;"><a href="/newsnevents">View All</i></a></div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <hr>
                    <div id="newsevents">
                        <div class="footer-item recent-posts">
                            <h4>Blogs</h4>

                            <div class="footer__widget">
                                <div class="footer__innner">
                                    <div class="ftr__latest__post">
                                        <!-- Start Single -->

                                        <div class="single__ftr__post d-flex">
                                            <div class="ftr__post__thumb">
                                                <a href="/blogs-detail/16">
                                                    <img src="{{url('images/Blogs/16/Advanced Materials used in Aerospace (1).jpg')}}" alt="post images" style="width: 78px; height: 78px;">
                                                </a>
                                            </div>
                                            <div class="ftr__post__details">
                                                <h6>
                                                    <a href="/blogs-detail/16">What are the Advanced materials used in ..</a>
                                                </h6>
                                                <span><i class="fa fa-calendar"></i>October 1, 2021</span>
                                                <div class="readsmore">
                                                    <a href="/blogs-detail/16">Read More <i class="fa fa-angle-double-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="readsmore" style="text-align: left;"><a href="/blogs">View All</i></a></div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <hr />
                    <div id="newsevents">
                        <div class="footer-item recent-posts">
                            <h4>Inventory</h4>

                            <div class="footer__widget">
                                <div class="footer__innner">
                                    <div class="ftr__latest__post">
                                        <!-- Start Single -->

                                        <div class="">
                                            <div class="ftr__post__thumb">
                                                <a href="/inventory/3">
                                                    <img src="{{url('images/Inventory/3/thumb.jpg')}}" alt="post images" style="width: 100%; height: 80px;">
                                                </a>
                                            </div>
                                            <br />
                                            <div class="ftr__post__details">
                                                <h6>
                                                    <a href="/inventory/3">Learjet 60XR</a>
                                                </h6>

                                                <div class="readsmore">
                                                    <a href="/inventory/3">Know More <i class="fa fa-angle-double-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="readsmore" style="text-align: left;"><a href="/services/aircraft#tabInventory">View All</i></a></div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="/assets/owlcarousel/owl.carousel.js"></script>
    <script type="text/javascript">
        $(document).on('change', '.div-toggle', function() {

            $("#hidCareerID").val($(this).val());
            $("#hidCareerTitle").val($(this).text());
            //
            var target = $(this).data('target');
            var show = $("option:selected", this).data('show');
            $(target).children().addClass('hide');
            $(show).removeClass('hide');
        });

        $(document).ready(function() {
            $('.div-toggle').trigger('change');
            $("#Menu7").addClass("active");
        });
    </script>

</div>
@include('footer')
</body>

</html>