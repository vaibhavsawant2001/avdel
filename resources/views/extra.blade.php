    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'csv_file' => 'required|mimes:csv,txt|max:2048',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }
        $file = $request->file('csv_file');
        $filePath = $file->getPathname();
        $csvData = array_map('str_getcsv', file($filePath));
        $header = array_shift($csvData);
        $product_name_index = array_search('product_name', $header);
        $product_description_index = array_search('product_description', $header);
        $cat_name_index = array_search('cat_name', $header);
        $subcat_index = array_search('subcat', $header);
        $image_name_index = array_search('image_name', $header);
        $detail_index = array_search('detail', $header);
        foreach ($csvData as $row) {
            $product = new Product;
            $product->product_name = $row[$product_name_index];
            $product->product_description = $row[$product_description_index];
            $product->cat_name = $row[$cat_name_index];
            $product->subcat = $row[$subcat_index];
            $product->image_name = $row[$image_name_index];
            $product->detail = $row[$detail_index];
            $product->save();
        }
        return response()->json(['message' => 'Products saved successfully', $product]);
    }


<!-- corrected code  -->
    public function storeOrUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'csv_file' => 'required|mimes:csv,txt|max:2048',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }
        $file = $request->file('csv_file');
        $filePath = $file->getPathname();
        $csvData = array_map('str_getcsv', file($filePath));
        $header = array_shift($csvData);
        $product_name_index = array_search('product_name', $header);
        $product_description_index = array_search('product_description', $header);
        $cat_name_index = array_search('cat_name', $header);
        $subcat_index = array_search('subcat', $header);
        $image_name_index = array_search('image_name', $header);
        $detail_index = array_search('detail', $header);
        foreach ($csvData as $row) {
            $existingProduct = Product::where('product_name', $row[$product_name_index])->first();
            if ($existingProduct) {
                $existingProduct->update([
                    'product_description' => $row[$product_description_index],
                    'cat_name' => $row[$cat_name_index],
                    'subcat' => $row[$subcat_index],
                    'image_name' => $row[$image_name_index],
                    'detail' => $row[$detail_index],
                ]);
            } else {
                $product = new Product;
                $product->product_name = $row[$product_name_index];
                $product->product_description = $row[$product_description_index];
                $product->cat_name = $row[$cat_name_index];
                $product->subcat = $row[$subcat_index];
                $product->image_name = $row[$image_name_index];
                $product->detail = $row[$detail_index];
                $product->save();
            }
        }
        return response()->json([
            'message' => 'Products saved or updated successfully',
        ]);
    }
    <!-- corrected code end here -->