@include('head')
@include('nav')
<script>
    function storeCID(id, urlName) {
        if (id != undefined && id != '') {
            // alert(id);
            // alert(urlName);
            var url = '/wsCommon.asmx/StoreCategoryID'
            $.ajax({
                type: "POST",
                url: url,
                data: "{ CatID: '" + id + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: "true",
                cache: "false",
                success: function(msg) {
                    window.location.href = urlName;
                },
                Error: function(x, e) {
                    // On Error
                }
            });
        }
    }

    function fnSearch(urlName) {
        //  alert("hiii");
        //  alert(urlName);
        var stxt = $("#txtSearch").val();
        if (stxt == undefined || stxt == '') {
            alert("Enter Search Text");
            $("#txtSearch").focus();
            return false;
        } else {
            var url = '/wsCommon.asmx/SearchMethod'
            $.ajax({
                type: "POST",
                url: url,
                data: "{ txt: '" + stxt + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: "true",
                cache: "false",
                success: function(msg) {
                    window.location.href = urlName;
                },
                Error: function(x, e) {
                    // On Error
                }
            });
            return false;
        }
    }
</script>
<div>
    <div id="companyprofile" class="breadcrumbs aviationback" style="background-image: url('{{ asset("images/UpdatePages/6/ATD-DIVISION.JPG") }}')">
        <div class="row breadinn">
            <div class="col-md-9 pad0">
                <div class="mainbread">
                    <ol class="breadcrumb">
                        <li><a href="/home">Home</a></li>
                        <li class="active">About Us</li>
                        <li class="active">
                            <span id="ContentPlaceHolder1_lblCurrentPage">ATD <small style='color:#fff;'>(Aerospace/Transportation/Defence)</small></span>
                        </li>
                    </ol>
                    <h2>
                        <span id="ContentPlaceHolder1_lblCurrentPageTitle">ATD <small style='color:#fff;'>(Aerospace/Transportation/Defence)</small></span>
                    </h2>
                </div>
            </div>
            <div class="col-md-3 topspace50">
                <ul class="menuitems">
                    <li><a href="/divisions/atd"><i class="fa fa-angle-double-right"></i>ATD <small>(Aerospace/Transportation/Defence)
                            </small></a></li>
                    <li><a href="/divisions/aviation"><i class="fa fa-angle-double-right"></i>Aviation </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <section id="responsive" class="innerpadding AviationPage">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                @foreach($pages as $page)
                    @if($page->page_name == 'ATD (AEROSPACE/TRANSPORTATION/DEFENCE)')
                    {!! $page->description !!}
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <script src="/assets/owlcarousel/owl.carousel.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#Menu3").addClass("active");
        });
    </script>
</div>
@include('footer')
</body>

</html>