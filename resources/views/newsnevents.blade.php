@include('head')
@include('nav')
<script>
  function storeCID(id, urlName) {
    if (id != undefined && id != '') {
      // alert(id);
      // alert(urlName);
      var url = '/wsCommon.asmx/StoreCategoryID'
      $.ajax({
        type: "POST",
        url: url,
        data: "{ CatID: '" + id + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: "true",
        cache: "false",
        success: function(msg) {
          window.location.href = urlName;
        },
        Error: function(x, e) {
          // On Error
        }
      });
    }
  }

  function fnSearch(urlName) {
    //  alert("hiii");
    //  alert(urlName);
    var stxt = $("#txtSearch").val();
    if (stxt == undefined || stxt == '') {
      alert("Enter Search Text");
      $("#txtSearch").focus();
      return false;
    } else {
      var url = '/wsCommon.asmx/SearchMethod'
      $.ajax({
        type: "POST",
        url: url,
        data: "{ txt: '" + stxt + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: "true",
        cache: "false",
        success: function(msg) {
          window.location.href = urlName;
        },
        Error: function(x, e) {
          // On Error
        }
      });
      return false;
    }
  }
</script>


<div>

  <div id="companyprofile" class="breadcrumbs">
    <div class="row breadinn">
      <div class="col-md-9 pad0">
        <div class="mainbread">
          <ol class="breadcrumb">
            <li><a href="/home">Home</a></li>
            <li class="active">News & Events</li>
          </ol>
          <h2>News & Events</h2>
        </div>
      </div>
      <div class="col-md-3 topspace50">
      </div>
    </div>
  </div>
  <!-- Responsive image with left -->
  <section id="managements" class="innerpadding">
    <div class="container">
      <div id="ContentPlaceHolder1_UpdatePanel1">
        @foreach($pages as $page)
        @if($page->page_name == 'News & Events')
        {!! $page->description !!}
        @endif
        @endforeach
        <div class="row">
          <div class="col-md-10">
            <div class="maintitlepage">
            </div>
          </div>
          <div class="col-md-2">
            <select name="ctl00$ContentPlaceHolder1$ddlFilter" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;ctl00$ContentPlaceHolder1$ddlFilter\&#39;,\&#39;\&#39;)&#39;, 0)" id="ddlFilter" class="form-control">
              <option selected="selected" value="All">All</option>
              <option value="Upcoming">Upcoming</option>
              <option value="2024">2024</option>
              <option value="2023">2023</option>
              <option value="2022">2022</option>
              <option value="2021">2021</option>
              <option value="2020">2020</option>
              <option value="2019">2019</option>
              <option value="2018">2018</option>
              <option value="2017">2017</option>
              <option value="2016">2016</option>
            </select>
          </div>
        </div>
        <div class="row wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0s">
          <div id="newsevents">
            @foreach($newsletters as $newsletter)
            <div class="col-md-3 col-xs-12 col-sm-6">
              <div class="image">
                <a href="{{ url('newsnevents_detail') }}">
                  <img src="{{ url('storage/app/' . $newsletter->image) }}" alt="publication Image" width="100%" height="210px">
                </a>
              </div>
              <h4>{{ $newsletter->album_name }}</h4>
              <h5>{{ $newsletter->date }}</h5>
              <hr />
              <p>{!! substr($newsletter->description, 0, 30) !!}</p>
              <a href="/newsnevents-detail/{{ $newsletter->id }}">read more</a>
            </div>
            @endforeach

          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="/assets/owlcarousel/owl.carousel.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $("#Menu6").addClass("active");
    });
  </script>
</div>
@include('footer')
</body>

</html>