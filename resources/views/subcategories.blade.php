<style>
    :root {
        --grey: rgba(0, 0, 0, 0.3);
        --focus: rgba(0, 0, 0, 0.025);
    }

    * {
        padding: 0;
        margin: 0;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }

    body {
        height: 110vh;
        display: grid;
        place-items: center;
        background: rgba(0, 0, 0, 0.025);
    }

    .wrapper {
        width: 350px;
        font: 0.75rem 'helvetica neue', sans-serif;
        color: rgba(0, 0, 0, 0.6);
        border: 1px solid rgba(0, 0, 0, 0.1);
        padding: 2em;
        background: white;
        transition: transform 250ms ease-in;

        &:focus-within {
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            transform: scale(1.05);
        }

        & header {
            display: grid;
            grid-template-rows: repeat(2, auto);
            grid-template-columns: 3fr 1fr;
            grid-template-areas: 'h2 p' 'h3 h3';
            align-items: center;

            & h2 {
                grid-area: h2;
                font-weight: 200;
            }

            & p {
                grid-area: p;
                font-weight: 300;
                font-size: .65rem;
                justify-self: right;
                color: var(--grey);
            }

            & h3 {
                grid-area: h3;
                font-size: .95rem;
                border-top: 0.5px solid var(--grey);
                margin: 1em 0;
                padding: 1em 0 0;
                color: var(--grey);
            }
        }
    }

    form {
        font-weight: 300;
        color: rgba(0, 0, 0, 0.6);

        & p {
            margin: 0.75em 0;
        }

        & input,
        textarea,
        button {
            border: 0.5px solid rgba(0, 0, 0, 0.2);
            outline: 0;
            padding: 1em;
            transition: background 200ms ease-in;
        }

        & input:focus,
        textarea:focus {
            background: var(--focus);
        }

        & input {
            width: 100%;
            margin-top: .75em;
            color: rgba(0, 0, 0, 0.6);
        }

        & textarea {
            resize: none;
            width: 100%;
            height: 20vh;
            margin-top: 1em;
        }

        & button {
            font: 400 .75rem 'helvetica neue', sans-serif;
            color: rgba(0, 0, 0, 0.4);
            float: right;
            cursor: pointer;
            background: none;

            &:hover {
                background: var(--focus);
            }
        }
    }

    .select-dropdown,
    .select-dropdown * {
        margin: 0;
        padding: 0;
        position: relative;
        box-sizing: border-box;
    }

    .select-dropdown {
        position: relative;
        background-color: #E6E6E6;
        border-radius: 4px;
    }

    .select-dropdown select {
        font-size: 1rem;
        font-weight: normal;
        max-width: 100%;
        padding: 8px 24px 8px 10px;
        border: none;
        background-color: transparent;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
    }

    .select-dropdown select:active,
    .select-dropdown select:focus {
        outline: none;
        box-shadow: none;
    }

    .select-dropdown:after {
        content: "";
        position: absolute;
        top: 50%;
        right: 8px;
        width: 0;
        height: 0;
        margin-top: -2px;
        border-top: 5px solid #aaa;
        border-right: 5px solid transparent;
        border-left: 5px solid transparent;
    }
</style>

<main class='wrapper'>
    <header>
        <h2>Add Category</h2>
        <p></p>
    </header>

    <form action="{{route('subcategories.store')}}" method="POST">
        <p> Select Category <br>
        <div class="select-dropdown">
            <select name="category_id">
                @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->cat_name}}</option>
                @endforeach
            </select>
        </div>
        </p>
        <p> Name <br>
      <input type='text' name="subcat_name">
    </p>
        <p><button type='submit'>SUBMIT</button></p>
        <p><a href="{{url('category_display')}}" style="text-decoration:none; color:black;" >Add Category</a></p>
        <p><a href="{{url('sub_subcategory')}}" style="text-decoration:none; color:black;" >Add Sub-SubCategory</a></p>
    </form>
</main>