<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" href="{{ asset('css/dist/img/management.png') }}">
  <title>Yaaaro PMS </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('css/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('css/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('css/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="{{ asset('css/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('css/bower_components/select2/dist/css/select2.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('css/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/dist/css/mystyle.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('css/dist/css/skins/_all-skins.min.css') }}">
  <script src="{{ url('css/ckeditor/ckeditor.js') }}"></script>
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="{{ url('yaaaro_pms/admin') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
          <img src="{{ asset('css/dist/img/management.png') }}" class="logo-img" alt="User Image" style="width: 210%; margin-left: -55%; margin-top: 20%;">
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
          <img src="{{ asset('css/dist/img/yaaaro.png') }}" class="logo-img" alt="User Image" style="height:35px">
        </span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{ asset('css/dist/img/management.png') }}" class="user-image" alt="User Image">
                <span class="hidden-xs">Yaaaro PMS</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="{{ asset('css/dist/img/yaaaro.png') }}" class="" alt="User Image">
                  <p>
                    Yaaaro Management System
                  </p>
                  <p>
                    <a style="color: white;" href="http://bluesuninfo.com/" target="_blank">bluesuninfo.com</a>
                  </p>
                  <p>
                    Contact No. : <a style="color: white;" href="tel:91-22-66154433">91-22-66154433</a>
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <!-- <a href="change_password.php" class="btn btn-default btn-flat">Change Password</a> -->
                  </div>
                  <div class="pull-right">
                    <form action="" method="POST">
                      @csrf
                      <button type="submit" class="btn btn-default btn-flat">Sign out</button>
                    </form>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <aside class="main-sidebar">
                <section class="sidebar">
                    <ul class="sidebar-menu" data-widget="tree">
                        <li><a href="{{url('yaaaro_pms/admin')}}"><i class="fa fa-circle-o text-aqua"></i><span>Dashboard</span></a></li>
                        <li><a href="{{url('yaaaro_pms/company_profile_edit')}}"><i class="fa fa-circle-o text-aqua"></i><span>Update Pages</span></a></li>
                         <li class="treeview">
                            <a href="#">
                                <i class="fa fa-pie-chart"></i>
                                <span>Home</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Admin/Masthead.aspx"><i class="fa fa-circle-o"></i>Masthead</a></li>
                                <li><a href="/Admin/Testimonials.aspx"><i class="fa fa-circle-o"></i>Testimonials</a></li>
                                <li><a href="/Admin/AboutUS.aspx"><i class="fa fa-circle-o"></i>Who we are</a></li>
                                <li><a href="/Admin/Divisions.aspx"><i class="fa fa-circle-o"></i>Divisions</a></li>
                            </ul>
                        </li>
                        <li><a href="{{url('yaaaro_pms/newsnletters')}}"><i class="fa fa-circle-o text-aqua"></i><span>News & Events</span></a></li>
                        <li><a href="/Admin/Management.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Management</span></a></li>
                         <li><a href="/Admin/Services.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Services</span></a></li>
                         <li><a href="/Admin/Inventory.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Inventory</span></a></li>
                         <li class="treeview">
                            <a href="#">
                                <i class="fa fa-pie-chart"></i>
                                <span>Product</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Admin/FirstSection.aspx"><i class="fa fa-circle-o text-aqua"></i><span>First Section</span></a></li>
                                 <li><a href="/Admin/SecondSection.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Second Section</span></a></li>
                                 <li><a href="/Admin/ThirdSection.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Third Section</span></a></li>
                                 <li><a href="/Admin/FourthSection.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Fourth Section</span></a></li>
                                 <li><a href="/Admin/FifthSection.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Fifth Section</span></a></li>
                                 <li><a href="/Admin/SixthSection.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Sixth Section</span></a></li>
                                 <li><a href="/Admin/Product.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Product</span></a></li>
                            </ul>
                        </li>
                         <li class="treeview">
                            <a href="#">
                                <i class="fa fa-pie-chart"></i>
                                <span>Blog</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Admin/AddBlog.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Add Blog</span></a></li>
                                 <li><a href="/Admin/ListBlog.aspx"><i class="fa fa-circle-o text-aqua"></i><span>List Blog</span></a></li>
                            </ul>
                        </li>
                        <li><a href="/Admin/Supplier.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Supplier</span></a></li>
                        <li><a href="/Admin/Careers.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Careers</span></a></li>
                        <li><a href="/Admin/Clients.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Clients</span></a></li>
                        <li><a href="/Admin/EnquiryList.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Enquiry</span></a></li>
                         <li><a href="/Admin/ChangePassword.aspx"><i class="fa fa-circle-o text-aqua"></i><span>Change Password</span></a></li>
                       
                    </ul>
                </section>
            </aside>