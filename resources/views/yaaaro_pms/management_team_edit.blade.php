@include('yaaaro_pms/head')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      {{$user->page_name}}
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="tag.php"> Management Team</a></li>
      <li class="active">Management Team</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
          </div>
          <form enctype="multipart/form-data" action="{{route('pages.update',$user->id)}}" method="POST">
          @method('PUT')
          @csrf
            <div class="box-body">
              <div class="row">
              <div class="col-md-6 form-group">
                  <label for="Offer Type"> Page Name : </label>
                  <input type="text" class="form-control" name="page_name" value="{{$user->page_name}}" required>              
                </div>
                <div class="col-md-6 form-group">
                  <label for="Offer Type"> Image Upload : </label>
                  <input type="file" class="form-control" name="image" value="{{$user->image}}">
                </div>
                <div class="form-group col-md-12">
                  <label for="Content">Description : </label>
                  <textarea class="form-control" rows="4" id="editor" name="description">{{$user->description}}</textarea>
                </div>
              </div>
              <div class="box-footer" align="center">
                <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
              </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>
@include('yaaaro_pms/footer')