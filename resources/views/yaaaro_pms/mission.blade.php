@include('yaaaro_pms/head')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
Mission
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="tag.php">Mission</a></li>
      <li class="active">Mission</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
          </div>
          <form enctype="multipart/form-data" action="{{route('mission.store')}}" method="POST">
            <div class="box-body">
              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="Offer Type"> Image Upload : </label>
                  <input type="file" class="form-control" name="image" value="" required>
                </div>
                <div class="form-group col-md-12">
                  <label for="Content">Description : </label>
                  <textarea class="form-control" rows="4" id="editor" name="description"></textarea>
                </div>
              </div>
              <div class="box-footer" align="center">
                <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
              </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>
@include('yaaaro_pms/footer')