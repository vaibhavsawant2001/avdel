@include('yaaaro_pms/head')
<style type="text/css">
    .company-detail {
        padding: 2% 5%;
        font-size: 16px;
    }

    .company-detail .image {
        margin-top: 10%;
    }

    .company-detail .image img {
        width: 50%;
    }

    .company-detail .time {
        margin-top: 5%;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <!-- <section class="content"> -->

        <!-- /.row -->
        <!-- <div class="box">
            <div class="col-md-12">
                <center>
                    <h2>Welcome to Yaaaro Management</h2>
                </center>
            </div>
            <div class="company-detail">
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="">
                            <img src="" alt="Company logo" class="img-responsive" style="border-radius:10px;width: 400px;height: 400px;">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Company Details</h3>
                        <p>
                            <strong>Company Name :   </strong>
                        </p>
                        <p>
                            <strong>Company CEO : </strong> <font color="blue"></font>
                        </p>
                        <p>
                            <strong>Company Address : </strong>
                        </p>
                        <p>
                            <strong>Company Email Id :  </strong>
                        </p>
                        <p>
                            <strong>Company Contact No.(Tel) :  </strong>
                        </p>
                        <p>
                            <strong>Company Contact No.(WhatsApp) :</strong>
                        </p>
                        <p>
                            <strong>Company Location (On GoogleMap) : </strong>
                        </p>
                    </div>
                    <div class="col-md-12">
                        <h3>Social Media</h3>
                        <div class="col-md-3">
                        <a href="">
                            <label class="fa fa-facebook"> Facebook</label>
                        </a>
                            <p></p>
                        </div>
                        <div class="col-md-3">
                            <a href="">
                            <label class="fa fa-twitter">Twitter</label>
                            </a>
                            <p></p>
                        </div>
                        <div class="col-md-3">
                        <a href="">
                            <label class="fa fa-instagram">Instagram</label>
                        </a>
                            <p></p>
                        </div>
                        <div class="col-md-3">
                        <a href="">
                            <label class="fa fa-linkedin">Linkedin</label>
                        </a>
                            <p></p>
                        </div>
                    </div>
                    <div class="col-md-12 ">
                        <a href="" class="btn btn-success"> Update
                            Detail</a>
                    </div>
                </div>

            </div>
        </div>
    </section> -->
    <!-- /.content -->
</div>

@include('yaaaro_pms/footer')