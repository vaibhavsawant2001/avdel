@include('yaaaro_pms/head')
<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <div id="ContentPlaceHolder1_TableData" class="box  box-info">
          <div class="box-header">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12">
              <!-- /.box-header -->
              <div class="table-responsive">
                <div>
                <button style="background: #367FA9;float:right;"><a href="{{url('yaaaro_pms/newsletters_add')}}" style="background: #367FA9; color:white;">Add News & Letters</a></button>
                  <table class="table table-bordered table-striped" cellspacing="0" rules="all" border="1" id="ContentPlaceHolder1_gvNews" style="border-collapse:collapse;">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Location</th>
                        <th>Description</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($newsletters as $newsletters)
                      <tr>
                        <th>{{$newsletters->album_name}}</th>
                        <th>{{$newsletters->date}}</th>
                        <th>{{$newsletters->location}}</th>
                        <th>{!! $newsletters->description !!}</th>
                        <th><a href="{{route('newsletters.edit',$newsletters->id)}}" class="fa fa-success">Edit</a></th>
                        <th><a href="" class="fa fa-success">Delete</a></th>
                        <th><a href="" class="fa fa-success">Hide</a></th>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@include('yaaaro_pms/footer')