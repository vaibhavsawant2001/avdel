@include('yaaaro_pms/head')
<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="col-xs-12">

        <div id="ContentPlaceHolder1_TableData" class="box  box-info">
          <div class="box-header">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-12">
              <!-- /.box-header -->
              <div class="table-responsive">
                <div>
                  <table class="table table-bordered table-striped" cellspacing="0" rules="all" border="1" id="ContentPlaceHolder1_gvNews" style="border-collapse:collapse;">
                    <thead>
                      <tr>
                        <th>Page Name</th>
                        <th>Description</th>
                        <th>Edit</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($pages as $pages)
                      <tr>
                        <th>{{$pages->page_name}}</th>
                        <th>{!! substr($pages->description, 0, 220) !!}</th>
                        <th><a href="{{route('pages.edit',$pages->id)}}" class="fa fa-success">Edit</a></th>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@include('yaaaro_pms/footer')