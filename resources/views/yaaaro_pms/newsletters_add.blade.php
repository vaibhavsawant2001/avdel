@include('yaaaro_pms/head')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="tag.php"> Management Team</a></li>
            <li class="active">Management Team</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                    </div>
                    <form enctype="multipart/form-data" action="{{route('newsletters.store')}}" method="POST">
                        @method('POST')
                        @csrf
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-8 form-group">
                                    <label for="Offer Type"> Album Name : </label>
                                    <input type="text" class="form-control" name="album_name" value="" required>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="Offer Type"> Date : </label>
                                    <input type="date" class="form-control" name="date" value="" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 form-group">
                                    <label for="Offer Type"> Location : </label>
                                    <textarea class="form-control" rows="3" name="location"></textarea>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Content">Upload Album Image (Width: 800px and Height: 639px) : </label>
                                    <input type="file" class="form-control" name="image" value="" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="Content">Description : </label>
                                    <textarea class="form-control" rows="4" id="editor" name="description"></textarea>
                                </div>
                            </div>
                            <div class="box-footer" align="center">
                                <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@include('yaaaro_pms/footer')