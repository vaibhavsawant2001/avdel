@include('head')
@include('nav')

<script>
    function storeCID(id, urlName) {
        if (id != undefined && id != '') {
            // alert(id);
            // alert(urlName);
            var url = '/wsCommon.asmx/StoreCategoryID'
            $.ajax({
                type: "POST",
                url: url,
                data: "{ CatID: '" + id + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: "true",
                cache: "false",
                success: function(msg) {
                    window.location.href = urlName;
                },
                Error: function(x, e) {}
            });
        }
    }

    function fnSearch(urlName) {
        //  alert("hiii");
        //  alert(urlName);
        var stxt = $("#txtSearch").val();
        if (stxt == undefined || stxt == '') {
            alert("Enter Search Text");
            $("#txtSearch").focus();
            return false;
        } else {
            var url = '/wsCommon.asmx/SearchMethod'
            $.ajax({
                type: "POST",
                url: url,
                data: "{ txt: '" + stxt + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: "true",
                cache: "false",
                success: function(msg) {
                    window.location.href = urlName;
                },
                Error: function(x, e) {
                    // On Error
                }
            });
            return false;
        }
    }
</script>
<div>
    <div id="companyprofile" class="breadcrumbs" style="background-image: url('{{ asset('images/UpdatePages/1/COMPANY-PROFILE.JPG') }}')">
        <div class="row breadinn">
            <div class="col-md-9 pad0">
                <div class="mainbread">
                    <ol class="breadcrumb">
                        <li><a href="/home">Home</a></li>
                        <li class="active">About Us</li>
                        <li class="active">
                            <span id="ContentPlaceHolder1_lblCurrentPage">Company Profile</span>
                        </li>
                    </ol>
                    <h2>
                        <span id="ContentPlaceHolder1_lblCurrentPageTitle">Company Profile</span>
                    </h2>
                </div>
            </div>
            <div class="col-md-3 topspace50">
                <ul class="menuitems">
                    <li><a href="/about-us/profile"><i class="fa fa-angle-double-right"></i>Company Profile </a></li>
                    <li><a href="/about-us/management"><i class="fa fa-angle-double-right"></i>Mangement Team </a></li>
                    <li><a href="/about-us/mission"><i class="fa fa-angle-double-right"></i>Mission, Vision & Strategy </a></li>

                    <li><a href="/about-us/partners"><i class="fa fa-angle-double-right"></i>Our Partners </a></li>
                    <li><a href="/about-us/clients"><i class="fa fa-angle-double-right"></i>Clients </a></li>
                    <li><a href="/about-us/approvals"><i class="fa fa-angle-double-right"></i>Approvals & Manuals </a></li>
                    <li><a href="/about-us/associations"><i class="fa fa-angle-double-right"></i>Associations</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Responsive image with left -->
    <section id="responsive" class="innerpadding vision">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="maintitlepage borderbtn">
                        @foreach($pages as $page)

                        @if($page->page_name == 'Company Profile')
                        {!! $page->description !!}
                        @endif

                        @endforeach

                        <p>&nbsp;</p>
                    </div>





                    <div class="row wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0s">


                    </div>



                    <div class="row">

                    </div>



                </div>
                <div class="col-md-3">
                    <div id="newsevents">
                        <div class="footer-item recent-posts">
                            <h4>News &amp; Events</h4>

                            <div class="footer__widget">
                                <div class="footer__innner">
                                    <div class="ftr__latest__post">

                                        <div class="single__ftr__post d-flex">
                                            <div class="ftr__post__thumb">
                                                <a href="newsnevents.html">
                                                    <img src="{{url('images/Events/41/Aero India -22 NE.jpg')}}" alt="post images" style="width: 78px; height: 78px;">
                                                </a>
                                            </div>
                                            <div class="ftr__post__details">
                                                <h6>
                                                    <a href="/newsnevents-detail/41">
                                                        MEET THE AVDEL INDIA TEAM AT AERO INDIA ...</a>
                                                </h6>
                                                <span><i class="fa fa-calendar"></i>
                                                    01 Jan 2023</span>

                                                <div class="readsmore">
                                                    <a href="/newsnevents-detail/41">Read More <i class="fa fa-angle-double-right"></i></a>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="readsmore" style="text-align: left;"><a href="/newsnevents">View All</i></a></div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <hr>
                    <div id="newsevents">
                        <div class="footer-item recent-posts">
                            <h4>Blogs</h4>

                            <div class="footer__widget">
                                <div class="footer__innner">
                                    <div class="ftr__latest__post">
                                        <!-- Start Single -->

                                        <div class="single__ftr__post d-flex">
                                            <div class="ftr__post__thumb">
                                                <a href="/blogs-detail/16">
                                                    <img src="{{url('images/Blogs/16/Advanced Materials used in Aerospace (1).jpg')}}" alt="post images" style="width: 78px; height: 78px;">
                                                </a>
                                            </div>
                                            <div class="ftr__post__details">
                                                <h6>
                                                    <a href="/blogs-detail/16">What are the Advanced materials used in ..</a>
                                                </h6>
                                                <span><i class="fa fa-calendar"></i>October 1, 2021</span>
                                                <div class="readsmore">
                                                    <a href="/blogs-detail/16">Read More <i class="fa fa-angle-double-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="readsmore" style="text-align: left;"><a href="/blogs">View All</i></a></div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <hr />
                    <div id="newsevents">
                        <div class="footer-item recent-posts">
                            <h4>Inventory</h4>

                            <div class="footer__widget">
                                <div class="footer__innner">
                                    <div class="ftr__latest__post">
                                        <!-- Start Single -->

                                        <div class="">
                                            <div class="ftr__post__thumb">
                                                <a href="/inventory/3">
                                                    <img src="{{url('images/Inventory/3/thumb.jpg')}}" alt="post images" style="width: 100%; height: 80px;">
                                                </a>
                                            </div>
                                            <br />
                                            <div class="ftr__post__details">
                                                <h6>
                                                    <a href="/inventory/3">Learjet 60XR</a>
                                                </h6>

                                                <div class="readsmore">
                                                    <a href="/inventory/3">Know More <i class="fa fa-angle-double-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="readsmore" style="text-align: left;"><a href="/services/aircraft#tabInventory">View All</i></a></div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Latest Publications -->
    <!------- CUSTOMERS --------->
    <script src="/js/owl.carousel.min.js"></script>
    <script type="text/javascript">
        $(".MgmtImage").click(function() {
            var id = $(this).attr('id').replace("teame", "");
            $(".temdetail").hide();
            $("#detail" + id).show();
            $(".mainmember").removeClass('active');
            $("#image" + id).addClass('active');

            return false;
        });
        $(document).ready(function() {
            $("#Menu2").addClass("active");
        });
    </script>
</div>
@include('footer')
</body>

</html>