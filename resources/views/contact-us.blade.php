@include('head')
@include('nav')
<script>
    function storeCID(id, urlName) {
        if (id != undefined && id != '') {
            // alert(id);
            // alert(urlName);
            var url = '/wsCommon.asmx/StoreCategoryID'
            $.ajax({
                type: "POST",
                url: url,
                data: "{ CatID: '" + id + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: "true",
                cache: "false",
                success: function (msg) {
                    window.location.href = urlName;
                },
                Error: function (x, e) {
                    // On Error
                }
            });
        }
    }

    function fnSearch(urlName) {
        //  alert("hiii");
        //  alert(urlName);
        var stxt = $("#txtSearch").val();
        if (stxt == undefined || stxt == '') {
            alert("Enter Search Text");
            $("#txtSearch").focus();
            return false;
        }
        else {
            var url = '/wsCommon.asmx/SearchMethod'
            $.ajax({
                type: "POST",
                url: url,
                data: "{ txt: '" + stxt + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: "true",
                cache: "false",
                success: function (msg) {
                    window.location.href = urlName;
                },
                Error: function (x, e) {
                    // On Error
                }
            });
            return false;
        }
    }
</script>


        <div>
            
    <div id="companyprofile" class="breadcrumbs" style="background-image: url('{{ asset("images/UpdatePages/18/CONTACT-US.JPG") }}')">
        <div class="row breadinn">
            <div class="col-md-9 pad0">
                <div class="mainbread">
                    <ol class="breadcrumb">
                        <li><a href="/home">Home</a></li>
                        <li class="active">Contact Us</li>
                    </ol>
                    <h2>Contact Us</h2>
                </div>
            </div>
            <div class="col-md-3 topspace50">
            </div>
        </div>
    </div>

    <section id="responsive" class="contacts innerpadding">
  <div class="container">
     <div class="row">
        <div class="col-md-12">
            <div class="maintitlepage">
               
                 
            </div>
        </div>
    </div>
    <div class="row">
      <div>
        <ul class="contactstabs nav nav-tabs " role="tablist">
            <li role="presentation" class="active">
            <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
            <div class="">
      	         <div class="maintitlepage">
                  <h4>CORPORATE OFFICE</h4>
                </div>
                <div class="footer-item contact-info maincontact">
                  <ul>
                    <li><i class="fa fa-building-o"></i><span>AVDEL (INDIA) PRIVATE LIMITED</span></li>
                    <li><i class="fa fa-map-marker"></i><span>6th  Floor, Ramon House, 
                      <br />169 Backbay Reclamation,<br />
                      Churchgate,<br /> Mumbai-400020,
                     <br />Maharashtra, India.</span></li>
                  </ul>
                </div>
              </div></a></li>
            <li role="presentation">
    	        <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
    	        <div class="">
      	         <div class="maintitlepage">
                  <h4>REGIONAL OFFICE</h4>
                </div>
                <div class="footer-item contact-info maincontact">
                  <ul>
                    <li><i class="fa fa-building-o"></i><span>AVDEL (INDIA) PRIVATE LIMITED</span></li>
                    <li><i class="fa fa-map-marker"></i><span>No. 13, 3rd Floor,
                     <br /> 6th Cross (Off Sampige Road),
                    <br />  Malleswaram,
                      <br />Bengaluru- 560 003,<br /> India.</span></li>
                  </ul>
                </div>
              </div></a></li>
            <li role="presentation"> <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">
    	        <div class="">
      	         <div class="maintitlepage">
                  <h4>WAREHOUSE</h4>
                </div>
                <div class="footer-item contact-info maincontact">
                  <ul>
                    <li><i class="fa fa-building-o"></i><span>AVDEL (INDIA) PRIVATE LIMITED</span></li>
                    <li><i class="fa fa-map-marker"></i><span>Plot No.A165/166, Road No.27,
                     <br /> Wagle Industrial Estate, 
                      <br />Thane – 400604, <br />Maharashtra, India<br />&nbsp;</span></li>
                  </ul>
                </div>
              </div></a></li>
            <li role="presentation"> <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">
    	        <div class="">
      	         <div class="maintitlepage">
                  <h4 style="float:left; margin-right:20px;">SALES OFFICE </h4> 
                  <select id="citys">
                      <option value="hyd" id="hyd">Hyderabad </option>
                      <option value="tri" id="tri">Trivandrum</option>
                    </select>
                </div>
               
               <div>
        	        <p id="hydon" class="bothoff">AVDEL (INDIA) PRIVATE LIMITED <br/> 17-2-628/13,<br /> Madannapet,<br /> Saidabad,<br /> Hyderabad – 500059,<br /> India.<br />&nbsp;</p>
                    <p id="trion" class="bothoff" style="display:none;">AVDEL (INDIA) PRIVATE LIMITED <br/> Oasis RG-107 A,<br /> Hospital Road, Pongumoodu,<br /> Trivandrum-695011,<br /> India.<br />&nbsp;<br />&nbsp;</p>
                </div>
              </div></a></li>
          </ul>
      </div>
      </div>
      	<div class="col-md-12" style="margin-top:20px;">
      </div>
      <div class="clearboths">
      	    <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
        	        <div class="maps">
                     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27687.431239673904!2d72.83437213067458!3d18.941856908105958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7ce1eedeaaaab%3A0x51e41fa9d5655b3e!2sAvdel+India+Private+Limited!5e0!3m2!1sen!2sin!4v1536994924033" width="100%" height="420" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                 </div>
                <div role="tabpanel" class="tab-pane" id="profile">
        	        <div class="maps">
                      
                            <iframe src="https://maps.google.com/maps?q=12.9975797,77.5704988&hl=es;z=20&amp;output=embed" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="messages">
        	        <div class="maps">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3767.9854955410074!2d72.94322201457814!3d19.195835853183727!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7b9a839071c4d%3A0x74dde13125973e26!2sAvdel+India+Private+Limited!5e0!3m2!1sen!2sin!4v1536998071084" width="100%" height="420" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="settings">
        	        <div class="maps">
                        <div id="hydons" class="bothoff">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30459.594107353758!2d78.60500797526868!3d17.390214380496037!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb9e54885e6975%3A0x5b3c71a9ad2db22!2sEAST+LAXMINAGAR+COLONY!5e0!3m2!1sen!2sin!4v1537425503373" width="100%" height="420" frameborder="0" style="border:0" allowfullscreen></iframe>
                      </div>
                      <div id="trions" class="bothoff" style="display:none;">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3945.6023441891293!2d76.92670441448304!3d8.537929698838107!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b05b94f9dcc982b%3A0x74f59f23d71b0e7e!2sHospital+Rd%2C+Ulloor%2C+Thiruvananthapuram%2C+Kerala+695017!5e0!3m2!1sen!2sin!4v1537429366811" width="100%" height="420" frameborder="0" style="border:0" allowfullscreen></iframe>
                      </div>
                    </div>
                </div>
            </div>
        
      </div>
    </div>
</section>
    <script src="/assets/owlcarousel/owl.carousel.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Menu8").addClass("active");

            $('body').on('change', '#citys', function () {
                var thevalue = $(this).val();
                if (thevalue == 'hyd') {
                    $('#trion').hide();
                    $('#trions').hide();
                    $('#hydon').show();
                    $('#hydons').show();
                } else {
                    $('#hydon').hide();
                    $('#hydons').hide();
                    $('#trion').show();
                    $('#trions').show();
                }
            });
        });
    </script>

        </div>
@include('footer')
</body>
</html>
