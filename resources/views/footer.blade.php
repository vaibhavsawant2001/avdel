
        <!--
footer widget
==================================== -->
<section class="footer-widget">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <div class="col-md-12 with-border">
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                <div class="footer-item setcolorfooterp wow fadeInUp" data-wow-duration="1s" data-wow-delay="0s">
                                    <h4>About Us</h4>
                                    <p style="color: #999;">Avdel (India) Pvt. Ltd was established in 1961 and is apart of a well established group of companies with interests in various business sectors including Aerospace, Aviation and Automotive and Rail amongst others. Avdel India is an AS 9120 B & ISO 9001 Certified company. With head quarters in Mumbai (India), Avdel India has regional offices in Bengaluru (the aerospace hub of the country) and sales office in Kanpur, Hyderabad and Trivandrum. </p>

                                    <a href="/about-us/profile">Read More</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                <div class="footer-item contact-info wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0s">
                                    <h4>contact info</h4>
                                    <ul>
                                        <li><i class="fa fa-building-o"></i><span>Avdel (India) Private Limited </span></li>
                                        <li><i class="fa fa-map-marker"></i><span>Ramon House, 6th Floor,
                                                <br />
                                                169 Backbay Reclamation,
                                                <br />
                                                Churchgate
                                                Mumbai 400 020</span></li>
                                        <li><i class="fa fa-phone"></i><span>91-022-66345611/ 66345612</span></li>
                                        <li><i class="fa fa-print"></i><span>91-022-66345622</span></li>
                                        <li><i class="fa fa-envelope-o"></i><span>ATD Division:
                                                <br />
                                                <a href="mailto:support@avdel.com">support@avdel.com</a></span></li>
                                        <li><i class="fa fa-envelope-o"></i><span>Aviation Division:
                                                <br />
                                                <a href="mailto:aviation@avdel.com">aviation@avdel.com</a></span></li>
                                        <li><i class="fa fa-envelope-o"></i><span>Marketing & General Inquiries:
                                                <br />
                                                <a href="mailto:info@avdel.com">info@avdel.com</a></span></li>
                                    </ul>
                                    CIN NO: U28990MH1961PTC011971
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                <div class="footer-item recent-posts wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0s">
                                    <h4>Pages</h4>
                                    <ul>
                                        <li><a href="/about-us/profile">About Us </a></li>
                                        <li><a href="/divisions/atd">Divisions </a></li>
                                        <li><a href="#">Products</a></li>
                                        <li><a href="/services/trip">Services </a></li>
                                        <li><a href="/newsnevents">News & Events </a></li>
                                        <li><a href="/careers">Careers </a></li>
                                        <li><a href="/contact-us">Contact Us </a></li>
                                        <li><a href="/blogs">Blogs</a></li>
                                        <li class="downloads"><a href="{{url('about-us/downloads')}}">Download Brochures
                                                <!--<i class="fa fa-angle-right"></i>-->
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                <div class="footer-item recent-posts wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0s">
                                    <h4>News & Events</h4>

                                    <div class="footer__widget">
                                        <div class="footer__innner">
                                            <div class="ftr__latest__post">

                                                <div class="single__ftr__post d-flex">
                                                    <div class="ftr__post__thumb">
                                                        <a href="newsnevents.html">
                                                            <img src="{{url('images/Events/41/Aero India -22 NE.jpg')}}" alt="post images" style="width: 78px; height: 78px;">
                                                        </a>
                                                    </div>
                                                    <div class="ftr__post__details">
                                                        <h6>
                                                            <a href="/newsnevents-detail/41">
                                                                MEET THE AVDEL INDIA TEAM AT AERO INDIA ...</a>
                                                        </h6>
                                                        <span><i class="fa fa-calendar"></i>
                                                            01 Jan 2023</span>

                                                    </div>

                                                </div>

                                                <div class="single__ftr__post d-flex">
                                                    <div class="ftr__post__thumb">
                                                        <a href="newsnevents.html">
                                                            <img src="{{url('images/Events/40/Avdel India CribMaster.png')}}" alt="post images" style="width: 78px; height: 78px;">
                                                        </a>
                                                    </div>
                                                    <div class="ftr__post__details">
                                                        <h6>
                                                            <a href="/newsnevents-detail/40">
                                                                Seminar on “STANLEY CribMaster - Automat...</a>
                                                        </h6>
                                                        <span><i class="fa fa-calendar"></i>
                                                            14 Oct 2022</span>

                                                    </div>

                                                </div>

                                                <div class="single__ftr__post d-flex">
                                                    <div class="ftr__post__thumb">
                                                        <a href="newsnevents.html">
                                                            <img src="{{url('images/Events/39/FIA announcement.jpg')}}" alt="post images" style="width: 78px; height: 78px;">
                                                        </a>
                                                    </div>
                                                    <div class="ftr__post__details">
                                                        <h6>
                                                            <a href="/newsnevents-detail/39">
                                                                Meet the Avdel India Team at Farnborough...</a>
                                                        </h6>
                                                        <span><i class="fa fa-calendar"></i>
                                                            22 Jun 2022</span>

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--
End footer widget
==================================== -->
        <!------------------------------------------------------------------------- POPUP CODE START -------------------------------------------->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2>Drop us a line</h2>
                        <button type="button" class="close" data-dismiss="modal">
                            &times;</button>
                    </div>
                    <div class="modal-body" style="max-height: 700px; overflow: scroll;">

                        <div class="form-group">
                            <label>
                                Name : <span style="color: red;">*</span></label>
                            <input name="ctl00$ucFooter1$txtName" type="text" id="ucFooter1_txtName" class="form-control" autocomplete="off" />
                            <span id="contact1" style="color: red;"></span>
                        </div>
                        <div class="form-group">
                            <label>
                                Email : <span style="color: red;">*</span></label>
                            <input name="ctl00$ucFooter1$txtEmail" type="text" id="ucFooter1_txtEmail" class="form-control" autocomplete="off" />
                            <span id="contact2" style="color: red;"></span>
                        </div>
                        <div class="form-group">
                            <label>
                                Phone : <span style="color: red;">*</span></label>
                            <input name="ctl00$ucFooter1$txtNumber" type="text" maxlength="15" id="ucFooter1_txtNumber" class="form-control txtNumericonly" autocomplete="off" />
                            <span id="contact4" style="color: red;"></span>
                        </div>
                        <div class="form-group">
                            <label>
                                Message : <span style="color: red;">*</span></label>
                            <textarea name="ctl00$ucFooter1$txtMessage" rows="8" cols="20" id="ucFooter1_txtMessage" class="form-control">
</textarea>
                            <span id="contact3" style="color: red;"></span>
                        </div>
                        <div class="form-group setvtnright">
                            <button type="button" name="btnEnquiry" id="btnEnquiry" onclick="EnquirySubmit();" class="btn contactbtn btn-lg">
                                Submit</button>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!------------------------------------------------------------------------- POPUP CODE END -------------------------------------------->

        <!--
footer
==================================== -->
        <footer class="footer">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <p class="copyright pull-left item_left">Copyright &copy; 2019 Avdel. All right reserved.</p>
                                    <div class="pull-right footer-item contact-info socials downcontact">
                                        <p>
                                            <a href="https://twitter.com/indiaavdel " target="_blank"><i class="fa fa-twitter twitters"></i></a>
                                            <a href="https://www.linkedin.com/company/avdel" target="_blank"><i class="fa fa-linkedin linkedins"></i></a>
                                            <a href="http://www.fb.com/indiaavdel" target="_blank"><i class="fa fa-facebook facebooks"></i></a>
                                            <a href="https://www.instagram.com/avdelindia" target="_blank">
                                                <img class="instagrams" src="{{url('Images/instagram.png')}}" alt="" style="margin-bottom: 9px;" /></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--
End footer
==================================== -->
        <div id="feedback">
            <a href="" data-toggle="modal" data-target="#myModal" target="_blank">Enquiry </a>
        </div>
        <a href="#." class="go-top text-center"><i class="fa fa-angle-double-up"></i></a>
        <script type="text/javascript">
            $(function() {
                $('#myModal').on('hidden.bs.modal', function() {
                    $(this).find("input,textarea,select").val('').end();
                });
            });

            function EnquirySubmit() {
                var name = $('#ucFooter1_txtName').val();
                var Email = $('#ucFooter1_txtEmail').val();
                var Phone = $('#ucFooter1_txtNumber').val();
                var Message = $('#ucFooter1_txtMessage').val();



                if (name == undefined || name == "") {
                    alert("Please Enter Contact Person");
                    $('#ucFooter1_txtName').focus();
                    return false;
                }

                if (Email == undefined || Email == "") {
                    alert("Please Email Address");
                    $('#ucFooter1_txtEmail').focus();
                    return false;
                }

                if (Phone == undefined || Phone == "") {
                    alert("Please Enter Contact Number");
                    $('#ucFooter1_txtNumber').focus();
                    return false;
                }


                if (Message == undefined || Message == "") {
                    alert("Please Enter Message");
                    $('#ucFooter1_txtMessage').focus();
                    return false;
                }


                //  if (name != '' && Email != '' && Phone != '' && Message != '') {

                var url = '/wsCommon.asmx/SaveEnquiry'
                $.ajax({
                    type: "POST",
                    url: url,
                    data: "{ Name: '" + name + "',EmailId: '" + Email + "',Phone:'" + Phone + "',Address:'" + Message + "'}",
                    // data: "{ Name: '" + name + "',EmailId: '" + Email + "',Phone:'" + Phone + "',Remark:'" + Message + "',Division:'" + Division + "',CompanyName:'" + Company + "',Designation:'" + Designation + "',Mobile:'" + Mobile + "',Address:'" + Address + "',City:'" + City + "',State:'" + State + "',Country:'" + Country + "',ProductName:'" + Product + "',Quantity:'" + Quantity + "',OtherDetails:'" + OtherDetails + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: "true",
                    cache: "false",
                    success: function(msg) {

                        //alert("Thank you for your enquiry. We shall get back to you soon.");
                        $("#myModal").modal('hide');
                        window.location.href = '/thankyou';
                    },
                    Error: function(x, e) {
                        // On Error
                    }
                });
                //        }
                //        else {
                //            alert("Please fill all the fields");
                //            $('#ucFooter1_txtName').focus();
                //            return false;
                //        }
            }
        </script>

    </form>
    <script src="{{url('js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{url('js/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{url('js/jquery.easing.min.js')}}"></script>
    <script src="{{url('js/jquery.appear.js')}}"></script>
    <script src="{{url('js/jquery.mixitup.min.js')}}"></script>
    <script src="{{url('js/wow.min.js')}}"></script>
    <script src="{{url('js/jquery.parallax-1.1.3.js')}}"></script>
    <script src="{{url('js/jquery.fancybox.js')}}"></script>
    <script src="{{url('js/jPushMenu.js')}}"></script>
    <script src="{{url('js/functions.js')}}"></script>
    <script src="{{url('js/script.js')}}"></script>
    <script>
        new WOW().init();
    </script>
    <!-- Default Statcounter code for Avdel India
        http://www.avdel.com -->
    <script type="text/javascript">
        var sc_project = 11916194;
        var sc_invisible = 1;
        var sc_security = "2891ff24";
    </script>
    <script type="text/javascript" src="https://www.statcounter.com/counter/counter.js" async></script>
    <noscript>
        <div class="statcounter">
            <a title="Web Analytics" href="http://statcounter.com/" target="_blank">
                <img class="statcounter" src="//c.statcounter.com/11916194/0/2891ff24/1/" alt="Web
        Analytics"></a>
        </div>
    </noscript>
    <!-- End of Statcounter Code -->