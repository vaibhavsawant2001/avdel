-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2024 at 02:58 PM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `avdel-app`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Fruits', 1, '2024-01-16 04:20:28', '2024-01-16 04:20:28'),
(2, 'Color', 1, '2024-01-16 04:20:28', '2024-01-16 04:20:28'),
(3, 'Electronics', 1, '2024-01-16 04:21:24', '2024-01-16 04:21:24'),
(4, 'Furniture', 1, '2024-01-17 00:15:24', '2024-01-17 00:15:24'),
(5, 'fastener', 1, '2024-01-17 01:23:50', '2024-01-17 01:23:50'),
(6, 'new', 1, '2024-01-17 02:05:34', '2024-01-17 02:05:34');

-- --------------------------------------------------------

--
-- Table structure for table `company_profiles`
--

CREATE TABLE `company_profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_profiles`
--

INSERT INTO `company_profiles` (`id`, `image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(5, 'images/EGZlynRicnKVrcx1QSNDBr7Nh5GZf9pu8t9xIu5E.png', '<p>Avdel (India) Pvt. Ltd was established in 1961 and is apart of a well established group of companies with interests in various business sectors including Aerospace, Aviation, Automotive and Rail amongst others. Avdel India is an AS 9120 B &amp; ISO 9001 Certified company. With head quarters in Mumbai (India), Avdel India has regional offices in Bengaluru (the aerospace hub of the country) and sales office in Hyderabad and Trivandrum.&nbsp;</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>1961 Established in technical collaboration with Aviation Development Ltd, UK. Name of collaborators also changed the same year to Avdel Systems Ltd, UK.</p>\r\n	</li>\r\n	<li>\r\n	<p>1962 Began Production of Chobert Rivets for Transport Industry</p>\r\n	</li>\r\n	<li>\r\n	<p>1964 Began Production of Avex Rivets for the Transport Industry (For application in Buses)</p>\r\n	</li>\r\n	<li>\r\n	<p>1964/1965 Began Production of Installation Tools for Blind Rivets and Avdel Self Plugging Rivets for Aviation Industry (For Hindustan Aeronautics Ltd.)</p>\r\n	</li>\r\n	<li>\r\n	<p>1991 Import and stocking all types of speciality fasteners for the Automotive, Aerospace and Transport Industry in India.</p>\r\n	</li>\r\n	<li>\r\n	<p>1991-1999 Expanded Customer base to include some Electrical, Electronics, White Goods leading manufacturers in India.</p>\r\n	</li>\r\n	<li>\r\n	<p>1998 Separated Aviation and Commercial Products Division</p>\r\n	</li>\r\n	<li>\r\n	<p>1998-2008 Focus on Aerospace - Entered into strategic sales partnerships with various Aerospace Fastener and Speciality Hardware Manufacturers around the world to supply to the growing Aerospace market in India.</p>\r\n	</li>\r\n	<li>\r\n	<p>2008 Entered into strategic sales partnerships with leading Composite Material Manufacturers to sell Advanced Composite Materials to the Aerospace and Rail Industry in India.</p>\r\n	</li>\r\n	<li>\r\n	<p>2009 Demerged/spun off the Commercial and Manufacturing business into a separate entity (Avfast India Pvt. Ltd). In order to focus on the distribution of Hardware and Advanced Composite materials to Aerospace and Rail.</p>\r\n	</li>\r\n	<li>\r\n	<p>2010-13 Set-up Bonded Warehouse in a special economic zone in Maharashtra, India.</p>\r\n	</li>\r\n	<li>\r\n	<p>2015-16 Appointed as Sales Representative by Universal Weather and Aviation for trip support and fuel services to General Aviation.</p>\r\n	</li>\r\n	<li>\r\n	<p>2016 Entered into partnerships for product offerings for aircraft interiors.</p>\r\n	</li>\r\n	<li>\r\n	<p>2017-2018 Separated Aviation and ATD (Aerospace/ Transport and Defence) Divisions.</p>\r\n	</li>\r\n	<li>\r\n	<p>2018 Aviation Division Enters Aircraft Brokerage Business.</p>\r\n	</li>\r\n	<li>\r\n	<p>2020Groups its Aviation Division offerings into AVTRIP, AVTRADE &amp; AVCARE for providing expert &amp; specialized services</p>\r\n	</li>\r\n	<li>\r\n	<p>2020Avdel Aviation adds new capabilities under his brand AVCARE and offers Air Charters &amp; VIP Concierge Services</p>\r\n	</li>\r\n</ul>\r\n\r\n<h2>AVDEL INDIA OPERATES UNDER THE FOLLOWING DIVISIONS:</h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>ATD (Aerospace, Transportation &amp; Defence) Division:</strong></p>\r\n\r\n<p>Avdel ATD Division was set-up to warehouse and supply fasteners and other hardware including mechanical control cables, and to also supply Advanced Composite Materials.</p>\r\n\r\n<p>With Distribution and representation for some of the world&rsquo;s leading brands for all the above products, Avdel India has emerged as a leading supplier to its customers for the same. Not just for the Aerospace segment but also for Defence, Rail, Marine &amp; Alternate Energy Industries.</p>\r\n\r\n<p><strong>Aviation Division:</strong></p>\r\n\r\n<p>Avdel Aviation caters to the needs of the growing Aviation and MRO segment with a special focus on Business (General) &amp; Commercial Aviation in India. Avdel along with their partners provide Trip Management Services, Business jet Acquisition &amp; Sales Consultancy, Aircraft Interior refurbishment services, Air Charters &amp; VIP Concierge Services, Supply Aviation grade carpets &amp; surface covering materials, Leather &amp; sheepskins, Aviation fabrics, Mirrors &amp; Glass panels, and Metal &amp; Plastic Coatings.</p>\r\n\r\n<p>Avdel India has warehousing facilities in Thane &amp; Panvel where all products are stocked enabling them to offer hassle-free clearing and logistics services for both Delivery Duty Paid (DDP) &amp; Delivery Duty Unpaid (DDU) consignments. Avdel India can also handle all necessary import documentation and delivery fulfilment directly to the stores of the customers.</p>\r\n\r\n<p>Avdel India&rsquo;s commitment to integrity, quality and social responsibility extends to its diverse and worldwide supply base. To ensure that all our partners conduct business with a high degree of integrity and in a responsible manner. All of our suppliers and service providers are expected to adhere to the Supplier Quality Manual, which lays down their responsibilities.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>AWARDS &amp; ACCOLADES</h2>\r\n\r\n<p><img src=\"http://192.168.1.4/avdel/public/images/awards.jpg\" /></p>', 1, '2024-02-01 02:00:37', '2024-02-01 02:00:37');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `management`
--

CREATE TABLE `management` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `management`
--

INSERT INTO `management` (`id`, `image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 'images/ZeQiPe0ZRkDLycafEBJwc265w3ohjYvBSvtvucRS.png', '<p>Avdel (India) Private Limited Management Team comprises of trusted, competent and motivated employees and advisors.. Each member of the management team shares the unwavering vision of making Avdel India one of the most admired companies in the world. Avdel&#39;s success in achieving consistent year-on-year organic growth is attributed to the deft leadership and dedication of its Management Team and employees.<br />\r\n<br />\r\nAvdel India Private Limited operations and management team reports to a Board of Directors comprising Bulchandani family members as well as outside independent Directors. The Board has appointed Sameer Bulchandani to lead Avdel India Private Limited.<br />\r\n&nbsp;</p>\r\n\r\n<h4>Sameer Bulchandani</h4>\r\n\r\n<h5>Director</h5>\r\n\r\n<p>Sameer Bulchandani is the Director and CEO of Avdel India Private Limited. Sameer reports to the Company Board and is responsible for leading the company&rsquo;s strategic planning, implementation, and business development initiatives. He has been involved in aerospace fasteners and hardware business since 1991. Sameer leads an innovation think-tank that guides the group through a changing economic landscape seizing opportunity to establish new businesses and as well as exit opportunities for existing ones. He remains actively involved in Avdel India&rsquo;s</p>\r\n\r\n<ul>\r\n	<li>&nbsp;Organic Growth as well as Mergers &amp; Acquisitions</li>\r\n	<li>&nbsp;Post-acquisition Integration</li>\r\n	<li>&nbsp;Capital Allocation and Financial Review</li>\r\n	<li>&nbsp;Business Development</li>\r\n	<li>&nbsp;Vendor Development</li>\r\n	<li>&nbsp;Team Building, and</li>\r\n	<li>&nbsp;Performance Measurement.</li>\r\n</ul>\r\n\r\n<p>Sameer led the Avdel India team to a successful demerger and subsequent sale of one of the manufacturing facilities to Stanley Black &amp; Decker in 2010. He is currently leading Avdel India&rsquo;s initiative into the Business Aviation Segment, a new initiative and focus area for the company.<br />\r\nSameer is an active member of the Society of Indian Aerospace Technologies and Industries (SIATI) as well as of the Indian Society for Advancement of Materials and Process Engineering (ISAMPE) and the Society for Aerospace Manufacturing Engineers (SAME) &amp; Business Aviation Operators Association (BAOA) in India.&nbsp;<br />\r\nSameer has a bachelor&rsquo;s degree in Economics and Engineering from Brown University, Rhode Island. He enjoys music, art, traveling, golf, and skiing.</p>', 1, '2024-02-01 04:25:15', '2024-02-01 04:25:15');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2024_01_12_092723_create_products_table', 2),
(10, '2024_01_16_053329_create_categories_table', 3),
(11, '2024_01_16_101006_create_subcategories_table', 4),
(13, '2024_01_16_102641_create_sub_subcategories_table', 5),
(14, '2024_01_31_124627_create_company_profiles_table', 6),
(15, '2024_02_01_081532_create_management_table', 7),
(16, '2024_02_01_112837_create_missions_table', 8),
(17, '2024_02_02_094556_create_pages_table', 9),
(18, '2024_02_06_115439_create_news_letters_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `missions`
--

CREATE TABLE `missions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `missions`
--

INSERT INTO `missions` (`id`, `image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'images/kx0PhhebDdEH7vSCJVvmCpaewih3loYGlFtCe9jq.png', '<h2><strong>OUR VISION</strong></h2>\r\n\r\n<p>AVDEL India aspires to be a leading partner to the Aviation, Space Missions, Defence Programmes&nbsp;and Transportation sectors by bringing a superlative experience to its customers in India through&nbsp;cost-effective solutions.<br />\r\n&nbsp;</p>\r\n\r\n<h2><strong>OUR MISSION</strong></h2>\r\n\r\n<p>AVDEL India strives to ensure its customers are able to meet their commitments as per their defined&nbsp;timelines by supplying them with impeccable products and services.<br />\r\n&nbsp;</p>\r\n\r\n<h2><strong>OUR VALUES</strong></h2>\r\n\r\n<p>AVDEL India believes in developing great relationships with all their customers and partners in order to make a positive difference to&nbsp;their operations by providing exceptional products and services that deliver value.</p>', 1, '2024-02-01 07:39:36', '2024-02-01 07:39:36');

-- --------------------------------------------------------

--
-- Table structure for table `news_letters`
--

CREATE TABLE `news_letters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `album_name` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_letters`
--

INSERT INTO `news_letters` (`id`, `album_name`, `date`, `location`, `image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'MEET THE AVDEL INDIA TEAM AT AERO INDIA SCHEDULED FROM 13-17 TH FEBRUARY -2023 IN BENGALURU.', '2023-01-01', 'Bengaluru, India', 'newsletters/H0y5BtICNtEUcUlozlFPwgFZIcIAWMTUMQMajxZR.png', '<p>Avdel India shall be participating at Aero India scheduled from 13-17th February 2023 at Yelahanka Airforce Station, Bengaluru.</p>\r\n\r\n<p>Avdel India team shall be present to welcome all its visitors, customers, suppliers, industry enthusiasts &amp; experts at <strong>Hall F Stand F 1.09 &amp; F 1.10</strong>. &nbsp;</p>\r\n\r\n<p>Please feel free to connect with us at <a href=\"mailto:support@avdel.com\">support@avdel.com</a> for scheduling an appointment or any other assistance that you may require,</p>\r\n\r\n<p><strong>Brief:</strong></p>\r\n\r\n<p>Avdel India has been regularly exhibiting at this show right from its inception and this year too it shall be exhibiting a full range of fasteners, hardware, installation tools &amp; advanced composites.&nbsp;Avdel India stands witness a huge footfall, amongst the people who visit the stand compromise of industry experts, customers (both regular and potential), and students who had an opportunity to see, touch and feel a huge range of products on display and learn from our technical team on its applications and end-uses.</p>\r\n\r\n<p><strong>About Aero India:</strong></p>\r\n\r\n<p>Aero India Exhibition which is organized every two years has already carved a niche for itself globally as a premier aerospace exhibition, with eleven successful editions organized since 1996. &nbsp;To complement the show&rsquo;s objective the organizers produced a special tagline for Aero India &ldquo;The Runway to a Billion Opportunities&rdquo; in 2019 and the same has been used since then for all their recent editions. The tagline was also created while keeping in mind that the Civil Aviation industry in India has emerged as one of the fastest-growing industries in the country.</p>\r\n\r\n<p>The tagline also underlined India&rsquo;s status as an emerging hub for the global aerospace industry and draws attention to the investment opportunities under Make in India.</p>\r\n\r\n<p>&nbsp;</p>', 1, '2024-02-06 06:41:51', '2024-02-06 06:41:51'),
(2, 'MEET THE AVDEL INDIA TEAM AT AERO INDIA SCHEDULED FROM 13-17 TH FEBRUARY -2023 IN BENGALURU.', '2024-02-15', 'yrstt', 'newsletters/GHxXeiQpF6zkDI7O9uH4DX0Dapcbdl20Ut1myOdi.png', '<p>trsgrsgsrgrsgr</p>', 1, '2024-02-06 06:52:12', '2024-02-06 06:52:12'),
(3, 'MEET THE AVDEL INDIA TEAM AT AERO INDIA SCHEDULED FROM 13-17 TH FEBRUARY -2023 IN BENGALURU.', '2024-02-15', 'yrstt', 'newsletters/K4XRxtDocN1xhYx6ldP9AaT6r5rZFWQn9IaEWOxl.png', '<p>trsgrsgsrgrsgr</p>', 1, '2024-02-06 06:53:32', '2024-02-06 06:53:32'),
(4, 'est', '2023-01-01', 'testt', 'upload/hFlZkIpGS8gjAdMmkDYZ1QJLJfYBy8GBbQUV8pES.jpg', 'frgergtehyrtsrghyrhsrfuvhjbdjv', 1, '2024-02-06 07:32:33', '2024-02-06 07:32:33'),
(5, 'est', '2023-01-01', 'testt', 'images/65c22f6269c65_1675155245290.jpg', 'frgergtehyrtsrghyrhsrfuvhjbdjv', 1, '2024-02-06 07:38:50', '2024-02-06 07:38:50'),
(6, 'est', '2023-01-01', 'testt', 'images/U2K7oOtEnjMu3eWlt8hzET57A3GZIcrTKW5vmeT3.jpg', 'frgergtehyrtsrghyrhsrfuvhjbdjv', 1, '2024-02-06 07:42:38', '2024-02-06 07:42:38');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page_name`, `image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Company Profile', 'images/AoftA7tK9nCDNQr4Rtrq7osjDtznKhau7B3Gf1dh.jpg', '<p>&nbsp;</p>\r\n\r\n<p>Avdel (India) Pvt. Ltd was established in 1961 and is apart of a well established group of companies with interests in various business sectors including Aerospace, Aviation, Automotive and Rail amongst others. Avdel India is an AS 9120 B &amp; ISO 9001 Certified company. With head quarters in Mumbai (India), Avdel India has regional offices in Bengaluru (the aerospace hub of the country) and sales office in Hyderabad and Trivandrum.&nbsp;</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>1961 Established in technical collaboration with Aviation Development Ltd, UK. Name of collaborators also changed the same year to Avdel Systems Ltd, UK.</p>\r\n	</li>\r\n	<li>\r\n	<p>1962 Began Production of Chobert Rivets for Transport Industry</p>\r\n	</li>\r\n	<li>\r\n	<p>1964 Began Production of Avex Rivets for the Transport Industry (For application in Buses)</p>\r\n	</li>\r\n	<li>\r\n	<p>1964/1965 Began Production of Installation Tools for Blind Rivets and Avdel Self Plugging Rivets for Aviation Industry (For Hindustan Aeronautics Ltd.)</p>\r\n	</li>\r\n	<li>\r\n	<p>1991 Import and stocking all types of speciality fasteners for the Automotive, Aerospace and Transport Industry in India.</p>\r\n	</li>\r\n	<li>\r\n	<p>1991-1999 Expanded Customer base to include some Electrical, Electronics, White Goods leading manufacturers in India.</p>\r\n	</li>\r\n	<li>\r\n	<p>1998 Separated Aviation and Commercial Products Division</p>\r\n	</li>\r\n	<li>\r\n	<p>1998-2008 Focus on Aerospace - Entered into strategic sales partnerships with various Aerospace Fastener and Speciality Hardware Manufacturers around the world to supply to the growing Aerospace market in India.</p>\r\n	</li>\r\n	<li>\r\n	<p>2008 Entered into strategic sales partnerships with leading Composite Material Manufacturers to sell Advanced Composite Materials to the Aerospace and Rail Industry in India.</p>\r\n	</li>\r\n	<li>\r\n	<p>2009 Demerged/spun off the Commercial and Manufacturing business into a separate entity (Avfast India Pvt. Ltd). In order to focus on the distribution of Hardware and Advanced Composite materials to Aerospace and Rail.</p>\r\n	</li>\r\n	<li>\r\n	<p>2010-13 Set-up Bonded Warehouse in a special economic zone in Maharashtra, India.</p>\r\n	</li>\r\n	<li>\r\n	<p>2015-16 Appointed as Sales Representative by Universal Weather and Aviation for trip support and fuel services to General Aviation.</p>\r\n	</li>\r\n	<li>\r\n	<p>2016 Entered into partnerships for product offerings for aircraft interiors.</p>\r\n	</li>\r\n	<li>\r\n	<p>2017-2018 Separated Aviation and ATD (Aerospace/ Transport and Defence) Divisions.</p>\r\n	</li>\r\n	<li>\r\n	<p>2018 Aviation Division Enters Aircraft Brokerage Business.</p>\r\n	</li>\r\n	<li>\r\n	<p>2020Groups its Aviation Division offerings into AVTRIP, AVTRADE &amp; AVCARE for providing expert &amp; specialized services</p>\r\n	</li>\r\n	<li>\r\n	<p>2020Avdel Aviation adds new capabilities under his brand AVCARE and offers Air Charters &amp; VIP Concierge Services</p>\r\n	</li>\r\n</ul>\r\n\r\n<h2>Avdel India operates under the following divisions:</h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>ATD (Aerospace, Transportation &amp; Defence) Division:</strong></p>\r\n\r\n<p>Avdel ATD Division was set-up to warehouse and supply fasteners and other hardware including mechanical control cables, and to also supply Advanced Composite Materials.</p>\r\n\r\n<p>With Distribution and representation for some of the world&rsquo;s leading brands for all the above products, Avdel India has emerged as a leading supplier to its customers for the same. Not just for the Aerospace segment but also for Defence, Rail, Marine &amp; Alternate Energy Industries.</p>\r\n\r\n<p><strong>Aviation Division:</strong></p>\r\n\r\n<p>Avdel Aviation caters to the needs of the growing Aviation and MRO segment with a special focus on Business (General) &amp; Commercial Aviation in India. Avdel along with their partners provide Trip Management Services, Business jet Acquisition &amp; Sales Consultancy, Aircraft Interior refurbishment services, Air Charters &amp; VIP Concierge Services, Supply Aviation grade carpets &amp; surface covering materials, Leather &amp; sheepskins, Aviation fabrics, Mirrors &amp; Glass panels, and Metal &amp; Plastic Coatings.</p>\r\n\r\n<p>Avdel India has warehousing facilities in Thane &amp; Panvel where all products are stocked enabling them to offer hassle-free clearing and logistics services for both Delivery Duty Paid (DDP) &amp; Delivery Duty Unpaid (DDU) consignments. Avdel India can also handle all necessary import documentation and delivery fulfilment directly to the stores of the customers.</p>\r\n\r\n<p>Avdel India&rsquo;s commitment to integrity, quality and social responsibility extends to its diverse and worldwide supply base. To ensure that all our partners conduct business with a high degree of integrity and in a responsible manner. All of our suppliers and service providers are expected to adhere to the Supplier Quality Manual, which lays down their responsibilities.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Awards &amp; Accolades</h2>\r\n\r\n<p><img src=\"../images/awards.jpg\" /></p>', 1, '2024-02-02 06:11:45', '2024-02-06 07:54:09'),
(2, 'Management Team', 'images/KEPUZwYDXAbyBlxxvCIpAZX89XGuS0cnf9ucW5PH.jpg', '<p>Avdel (India) Pvt. Ltd was established in 1961 and is apart of a well established group of companies with interests in various business sectors including Aerospace, Aviation, Automotive and Rail amongst others. Avdel India is an AS 9120 B &amp; ISO 9001 Certified company. With head quarters in Mumbai (India), Avdel India has regional offices in Bengaluru (the aerospace hub of the country) and sales office in Hyderabad and Trivandrum.&nbsp;</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>1961 Established in technical collaboration with Aviation Development Ltd, UK. Name of collaborators also changed the same year to Avdel Systems Ltd, UK.</p>\r\n	</li>\r\n	<li>\r\n	<p>1962 Began Production of Chobert Rivets for Transport Industry</p>\r\n	</li>\r\n	<li>\r\n	<p>1964 Began Production of Avex Rivets for the Transport Industry (For application in Buses)</p>\r\n	</li>\r\n	<li>\r\n	<p>1964/1965 Began Production of Installation Tools for Blind Rivets and Avdel Self Plugging Rivets for Aviation Industry (For Hindustan Aeronautics Ltd.)</p>\r\n	</li>\r\n	<li>\r\n	<p>1991 Import and stocking all types of speciality fasteners for the Automotive, Aerospace and Transport Industry in India.</p>\r\n	</li>\r\n	<li>\r\n	<p>1991-1999 Expanded Customer base to include some Electrical, Electronics, White Goods leading manufacturers in India.</p>\r\n	</li>\r\n	<li>\r\n	<p>1998 Separated Aviation and Commercial Products Division</p>\r\n	</li>\r\n	<li>\r\n	<p>1998-2008 Focus on Aerospace - Entered into strategic sales partnerships with various Aerospace Fastener and Speciality Hardware Manufacturers around the world to supply to the growing Aerospace market in India.</p>\r\n	</li>\r\n	<li>\r\n	<p>2008 Entered into strategic sales partnerships with leading Composite Material Manufacturers to sell Advanced Composite Materials to the Aerospace and Rail Industry in India.</p>\r\n	</li>\r\n	<li>\r\n	<p>2009 Demerged/spun off the Commercial and Manufacturing business into a separate entity (Avfast India Pvt. Ltd). In order to focus on the distribution of Hardware and Advanced Composite materials to Aerospace and Rail.</p>\r\n	</li>\r\n	<li>\r\n	<p>2010-13 Set-up Bonded Warehouse in a special economic zone in Maharashtra, India.</p>\r\n	</li>\r\n	<li>\r\n	<p>2015-16 Appointed as Sales Representative by Universal Weather and Aviation for trip support and fuel services to General Aviation.</p>\r\n	</li>\r\n	<li>\r\n	<p>2016 Entered into partnerships for product offerings for aircraft interiors.</p>\r\n	</li>\r\n	<li>\r\n	<p>2017-2018 Separated Aviation and ATD (Aerospace/ Transport and Defence) Divisions.</p>\r\n	</li>\r\n	<li>\r\n	<p>2018 Aviation Division Enters Aircraft Brokerage Business.</p>\r\n	</li>\r\n	<li>\r\n	<p>2020Groups its Aviation Division offerings into AVTRIP, AVTRADE &amp; AVCARE for providing expert &amp; specialized services</p>\r\n	</li>\r\n	<li>\r\n	<p>2020Avdel Aviation adds new capabilities under his brand AVCARE and offers Air Charters &amp; VIP Concierge Services</p>\r\n	</li>\r\n</ul>\r\n\r\n<h2>Avdel India operates under the following divisions:</h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>ATD (Aerospace, Transportation &amp; Defence) Division:</strong></p>\r\n\r\n<p>Avdel ATD Division was set-up to warehouse and supply fasteners and other hardware including mechanical control cables, and to also supply Advanced Composite Materials.</p>\r\n\r\n<p>With Distribution and representation for some of the world&rsquo;s leading brands for all the above products, Avdel India has emerged as a leading supplier to its customers for the same. Not just for the Aerospace segment but also for Defence, Rail, Marine &amp; Alternate Energy Industries.</p>\r\n\r\n<p><strong>Aviation Division:</strong></p>\r\n\r\n<p>Avdel Aviation caters to the needs of the growing Aviation and MRO segment with a special focus on Business (General) &amp; Commercial Aviation in India. Avdel along with their partners provide Trip Management Services, Business jet Acquisition &amp; Sales Consultancy, Aircraft Interior refurbishment services, Air Charters &amp; VIP Concierge Services, Supply Aviation grade carpets &amp; surface covering materials, Leather &amp; sheepskins, Aviation fabrics, Mirrors &amp; Glass panels, and Metal &amp; Plastic Coatings.</p>\r\n\r\n<p>Avdel India has warehousing facilities in Thane &amp; Panvel where all products are stocked enabling them to offer hassle-free clearing and logistics services for both Delivery Duty Paid (DDP) &amp; Delivery Duty Unpaid (DDU) consignments. Avdel India can also handle all necessary import documentation and delivery fulfilment directly to the stores of the customers.</p>\r\n\r\n<p>Avdel India&rsquo;s commitment to integrity, quality and social responsibility extends to its diverse and worldwide supply base. To ensure that all our partners conduct business with a high degree of integrity and in a responsible manner. All of our suppliers and service providers are expected to adhere to the Supplier Quality Manual, which lays down their responsibilities.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Awards &amp; Accolades</h2>\r\n\r\n<p><img src=\"../images/awards.jpg\" /></p>', 1, '2024-02-03 07:32:19', '2024-02-05 23:45:30'),
(3, 'TEST', 'images/1T6RRUGTu8vqG973AvRNhJNIV2RHwjAOHRZqv9q6.jpg', 'frgergtehyrtsrghyrhsrfuvhjbdjv', 1, '2024-02-05 02:27:06', '2024-02-05 02:27:06'),
(4, 'Mission, Vision & Values', 'images/b53gaBJHL22cmZY2BMe98SFIdqZBRC2XfHkjNL6Z.jpg', '<h2><strong>OUR VISION</strong></h2>\r\n\r\n<p>AVDEL India aspires to be a leading partner to the Aviation, Space Missions, Defence Programmes&nbsp;and Transportation sectors by bringing a superlative experience to its customers in India through&nbsp;cost-effective solutions.<br />\r\n&nbsp;</p>\r\n\r\n<h2><strong>OUR MISSION</strong></h2>\r\n\r\n<p>AVDEL India strives to ensure its customers are able to meet their commitments as per their defined&nbsp;timelines by supplying them with impeccable products and services.<br />\r\n&nbsp;</p>\r\n\r\n<h2><strong>OUR VALUES</strong></h2>\r\n\r\n<p>AVDEL India believes in developing great relationships with all their customers and partners in order to make a positive difference to&nbsp;their operations by providing exceptional products and services that deliver value.</p>', 1, '2024-02-06 00:18:42', '2024-02-06 01:11:48'),
(5, 'Our Partners', 'images/IBJtYIotvTOssyra13O6WPCaqXWGW52K2ceThAQN.jpg', '<p>Avdel India is a trusted partner to leading manufacturers and service providers who are experts in their respective fields. It&#39;s network of strategic alliances covers every major system &amp; platform and continues to grow every year.</p>\r\n\r\n<p>Avdel India along with these partnerships in place supports the Aviation, Aerospace, and Transport industry in India by establishing a strong network and support system designed to responsibly grow along with the industries we serve.<br />\r\n&nbsp; &nbsp;<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img alt=\"\" src=\"https://avdel.com/Images/CkImages/tny-637462080336805819.jpg\" style=\"height:481px; width:790px\" title=\"Capture.JPG\" /></p>\r\n\r\n<p>&nbsp;</p>', 1, '2024-02-06 00:19:09', '2024-02-06 01:25:38'),
(6, 'ATD (AEROSPACE/TRANSPORTATION/DEFENCE)', 'images/wsGm6cpVrLMd3MgrMkNR2lCYukmVTShE6rRSjgmH.jpg', '<p>Avdel ATD Division was set-up in 1998 to cater to the fastener requirements of an expanding aerospace, transportation and defence industry in India. Since then, the product range has expanded not just in the fastener and hardware, but to Mechanical Control Cables and to Advanced materials as well. Here, we offer the broadest range of industry specific hardwares, consumables and inventory management services for the Aerospace, Defence, Rail and Marine markets.</p>\r\n\r\n<p><img src=\"../Images/CkImages/tny-636735737942956250.jpg\" /></p>\r\n\r\n<h3>AEROSPACE</h3>\r\n\r\n<p>Avdel India has been actively working with various Government &amp; Private Aerospace manufacturing firms in India by successfully delivering solutions at every phase of the project right from its initialisation to execution. Avdel India continues supporting the growing Aerospace industry with quality products &amp; Services and pledges to be an integral part of the supply chain system to the Indian Aerospace industry.</p>\r\n\r\n<p>With years of experience of its operations, Avdel India acknowledges the needs of the industry and encourages companies to avail the benefits of our in house technical expertise, thereby assisting them to take proper decisions, right from the design to the production stages. Avdel India provides a broad range of solutions and tooling equipments, which further adds value to its customer by providing a one stop shop for all their hardware &amp; installation tooling requirements.</p>\r\n\r\n<p><img src=\"../Images/CkImages/tny-636735747587643750.jpg\" /></p>\r\n\r\n<h3>TRANSPORTATION</h3>\r\n\r\n<p>Avdel India is trying to make lives better, safer, and more complete; it takes great pride in being a trusted supplier by exceeding the expectations of the majority of our customers. Avdel India&rsquo;s team has the excellent product knowledge, coupled with on-time delivery and superior customer support ensures a great level of comfort amongst our customers. Avdel India&#39;s reputation gained over the years of its existence ensures repeat business and separates it from the competition.</p>\r\n\r\n<p>From large submarines &amp; ships to smallest of the Defence transportation and wide Rail transportation spread across the length and breadth of India. Avdel India plays a great role in efficiently supplying safer, comfortable, attractive, durable and environmentally responsible products &amp; services for various modes of transportation, due to which it has gained the honour of being a reliable supplier in the Indian territory.</p>\r\n\r\n<p><img src=\"../Images/CkImages/tny-636735747744831250.jpg\" /></p>\r\n\r\n<h3>DEFENCE</h3>\r\n\r\n<p>Avdel India takes great pride in servicing India&rsquo;s National Security Programmes and are committed to serving India&rsquo;s Defence Manufacturing companies and its subsidiaries by providing them timely delivery of products &amp; services.</p>\r\n\r\n<p>Avdel is committed to supplying innovative, secured, and advanced solutions thereby providing an unparalleled level of quality and safety throughout its supplied products range. Avdel supplies a wide range of products to the companies engaged in Defence Production &amp; its subsidiaries. Avdel India&rsquo;s value-added services to its customers by warehousing their ordered inventory for huge projects has greatly helped the organization to plan manufacturing in a phased manner.</p>', 1, '2024-02-06 00:19:47', '2024-02-06 02:49:25'),
(7, 'Aviation', 'images/f4C2G5mWtCYEJeOovPSDNJXgp8JrG6abdNHxyLxb.jpg', '<p>Avdel Aviation Division started in the year 2014 to provide top quality aviation services to the Business &amp; Commercial Aviation segment in India has further categorized its offerings with an intention to offer customized services and support through its experts in respective services delivery.</p>\r\n\r\n<p>The services are divided in to the following&nbsp; three groups:</p>', 1, '2024-02-06 00:20:25', '2024-02-06 03:08:32'),
(8, 'Clients', 'images/Jr9e4Dr0Nh7TYWUvVc4443UHEH8bGOVfp7O6C0xd.jpg', '<p>Avdel India has always focused on providing quality products and services to thier clients. With years of experience, support and knowledge, Avdel India has increased its customers base across Aerospace, Defense &amp; Transportation industry in India.</p>\r\n\r\n<p>Along with special focus on these industries, it has also been very instrumental in developing &amp; supporting Business &amp; Commercial Aviation Clients in India.</p>', 1, '2024-02-06 00:20:54', '2024-02-06 01:41:17'),
(9, 'Approvals & Manuals', 'images/vnPtDZpv22mt7TbfO0kNS7Hj7TdXXSVJtW0imhBk.jpg', '<div class=\"maintitlepage borderbtn\">\r\n            <p>Avdel India has been successfully carrying out ISO Audit every year and has been accredited with ISO: 9001-2015 &amp; AS9120 B. This ensures Avdel India and its suppliers adhere to the quality manuals and ensure full traceability for the products supplied.</p>\r\n<p>&nbsp;</p>\r\n<div class=\"row borderbtn\">\r\n<div class=\"col-md-4\" style=\"font-size: 12px; text-align: center;\"><a href=\"../images/brochures/AS9120B_Reissued_27_06_2019.pdf\" target=\"_blank\" rel=\"noopener\"><img src=\"../Images/CkImages/New-Certficate_19_20.jpg\" alt=\"\" width=\"100%\" /></a><br /><br />Click on Certificate image to download.</div>\r\n<div class=\"col-md-4\" style=\"font-size: 12px; text-align: center;\"><a href=\"mailto:support@avdel.com\"><img src=\"../Images/CkImages/tny-636734981793581250.jpg\" alt=\"\" width=\"97%\" /></a><br /><br />For Supplier Quality Manual, please send in a request to <a style=\"color: blue;\" href=\"mailto:support@avdel.com\">support@avdel.com</a></div>\r\n</div>\r\n        </div>', 1, '2024-02-06 00:22:54', '2024-02-06 02:30:21'),
(10, 'Associations', 'images/rC4jlgFnXbREgDkCSCGjL4XSNz8vpk0W4tUeIj3W.jpg', '<p>Avdel India is proud to be associated with various organizations, who focus their efforts on creating a common platform to raise industry-related concerns, bridging the gap between various stakeholders, and further provide an opportunity to engage and network with various professionals, industrialists, experts from their respective fields.<br />\r\n<br />\r\nCurrently, Avdel is engaged with the below-reputed organizations who are constantly working to provide industry knowledge &amp; latest updates on the developments in the Indian Aerospace &amp; Aviation Industry.<br />\r\n<br />\r\n<img alt=\"\" src=\"../Images/CkImages/tny-636734904805768750.jpg\" /></p>', 1, '2024-02-06 00:23:52', '2024-02-06 02:40:29'),
(11, 'Careers', 'images/UZW2Q4hubGWzM3BHIrSDEf8WyoLueSksXSmPnUQF.jpg', '<p>&nbsp;</p>\r\n\r\n<p>Avdel (India) Pvt Ltd (AIPL)- is one of the leading distributors for Aerospace hardware &amp; Aviation services in India. In addition to Aerospace hardware, Avdel also caters to the needs of both Commercial &amp; Business Aviation Companies in India by various services like Trip management, Aircraft Acquisitions &amp; Sales &amp; interior refurbishments. Avdel has maintained exponential growth in the past couple of years by adapting to the changing needs of the customers at large and by enhancing their overall capabilities. Our team of highly motivated &amp; experienced professionals has built -up an enviable position for the Company.<br />\r\n<br />\r\n<strong>Our Commitment:</strong><br />\r\nWe firmly believe in &quot;Quality Improvement&quot; &amp; strive to achieve higher levels of efficiencies, in order to offer complete Customer Satisfaction/delight. In keeping pace with the continuous growth, and technological advancement, we continuously seek qualified and deserving candidates for various positions in the company to develop, nurture &amp; grow our business to strengthen our position in the Industry.<br />\r\n<br />\r\nAvdel would like to develop and expand its business and technological competence. To keep pace with continuous growth, we seek top-notch professionals with in-depth knowledge of the industry, to develop, nurture, and retain its position in the industry.</p>', 1, '2024-02-06 00:25:05', '2024-02-06 03:10:35'),
(12, 'Download Brochure', 'images/RnslMMfpuVy2v43z5URATVZbCxZWae46pyeR2eGe.jpg', '<div class=\"col-md-4\" align=\"center\">\r\n                                <a class=\"\" href=\"../images/brochures/2020 ATD Brochure.pdf\" target=\"_blank\" rel=\"noopener\">\r\n                                    <img src=\"../Images/CkImages/tny-636740168682187500.jpg\" alt=\"\" width=\"100%\" />\r\n                                </a><br/><br/>\r\n                                Click Image for ATD Brochure\r\n                            </div>\r\n                            <div class=\"col-md-4\" align=\"center\">\r\n                                <a href=\"../images/brochures/2020 june BROCHURE.pdf\" target=\"_blank\" rel=\"noopener\">\r\n                                    <img src=\"../Images/CkImages/tny-636740168500625000.jpg\" alt=\"\" width=\"100%\" />\r\n                                </a><br/><br/>\r\n                                Click Image for Aviation Brochure\r\n                            </div>', 1, '2024-02-06 00:31:17', '2024-02-06 04:57:31'),
(13, 'News & Events', 'images/X24DElyfNAhC7eHe7XfZ9nBM1caHhuJHjdB144I1.jpg', 'frgergtehyrtsrghyrhsrfuvhjbdjv', 1, '2024-02-06 00:34:01', '2024-02-06 00:34:01'),
(14, 'Contact Us', 'images/ZL3TsXHja9ihEpwGV5czUxIPKbgCr9dEihNRUZsp.jpg', 'frgergtehyrtsrghyrhsrfuvhjbdjv', 1, '2024-02-06 00:36:17', '2024-02-06 00:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_description` varchar(255) NOT NULL,
  `cat_name` varchar(255) NOT NULL,
  `subcat` varchar(255) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `detail` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `product_description`, `cat_name`, `subcat`, `image_name`, `detail`, `created_at`, `updated_at`) VALUES
(44, 'Aerospace test', 'Very big', 'Main1133', 'Sub main', 'Aerospace test', 'Aerospace test', '2024-01-17 01:29:56', '2024-01-17 01:29:56'),
(45, 'Aerospace ff', 'Very big1', 'Main', 'Sub main', 'Aerospace ff', 'detail about2', '2024-01-17 01:29:56', '2024-01-17 04:31:01'),
(46, 'Aerospacerr', 'Very big2', 'Main', 'Sub main', 'Aerospacerr', 'detail about3', '2024-01-17 01:29:56', '2024-01-17 04:31:01'),
(47, 'Aerospace tt', 'Very big3rrrrr', 'Main', 'Sub main', 'Aerospace tt', 'detail about4', '2024-01-17 01:29:56', '2024-01-17 04:31:01'),
(48, 'Aerospace uedi', 'Very big4', 'Main', 'Sub main', 'Aerospace uedi', 'detail about5', '2024-01-17 01:29:56', '2024-01-17 04:31:01'),
(49, 'Aerospace ww', 'Very big5', 'Main', 'Sub main', 'Aerospace ww', 'detail about6', '2024-01-17 01:29:56', '2024-01-17 04:31:01'),
(50, 'Aerospace9', 'Very big6', 'Main', 'Sub main', 'Aerospace9', 'detail about7', '2024-01-17 01:29:56', '2024-01-17 04:31:01'),
(51, 'Aerospace10', 'Very big7', 'Main', 'Sub main', 'Aerospace10', 'detail about8', '2024-01-17 01:29:56', '2024-01-17 04:31:01'),
(52, 'Aerospace11', 'Very big8', 'Main', 'Sub main', 'Aerospace11', 'detail about9', '2024-01-17 01:29:56', '2024-01-17 04:31:01'),
(53, 'Aerospace12', 'Very big9', 'Main', 'Sub main', 'Aerospace12', 'detail about10', '2024-01-17 01:29:56', '2024-01-17 04:31:01'),
(54, 'Aerospace13', 'Very big20', 'Main', 'Sub main', 'Aerospace13', 'detail about11', '2024-01-17 01:29:56', '2024-01-17 04:31:01'),
(55, 'Aerospace15', 'Very big11', 'Main', 'Sub main', 'Aerospace15', 'detail about12', '2024-01-17 01:29:56', '2024-01-17 04:31:01'),
(56, 'Aerospace', 'Very big13', 'Main', 'Sub main', 'Aerospace', 'detail about14', '2024-01-17 01:29:56', '2024-01-24 02:52:37'),
(57, 'testhaiye', 'kuch nhi hai esmai', 'color', 'black', 'testhaiye', 'testhaiye', '2024-01-17 01:30:47', '2024-01-17 01:32:45'),
(58, 'testhaiyennn', 'kuch nhi hai esmai', 'color', 'black', 'testhaiyennn', 'kya detail', '2024-01-17 01:33:26', '2024-01-17 04:31:01'),
(59, 'Aerospace 345', 'Very big newwws', 'Main1133', 'Sub main', 'Aerospace 345', 'detail about1', '2024-01-17 01:35:03', '2024-01-17 04:31:01'),
(60, 'tes6t', 'testtttttt', 'Fruits', 'black', '1705478847.jpg', 'testtt', '2024-01-17 02:37:27', '2024-01-17 02:37:27'),
(61, 'p1', 'Lorem Ipsum is simply dummy text of the pr.', 'Color', 'watermelon', '1705483962.jpg', 'kya detail', '2024-01-17 04:02:42', '2024-01-17 04:02:42'),
(62, 'p2', 'p2fdkgbd', 'fastener', 'aaggsa', '1705485040.jpg', '6ytrdfdghj', '2024-01-17 04:20:40', '2024-01-17 04:20:40'),
(63, 'main', 'haan', 'blackkkk', 'orangeeee', 'main', 'tesrrrrrrr', '2024-01-17 04:26:17', '2024-01-19 07:34:36'),
(64, 'Post - 1', 'post - 1', 'Electronics', 'phone', '1705668272.jpg', 'post post', '2024-01-19 07:14:32', '2024-01-19 07:14:32'),
(65, 'root', 'root', 'root', 'root', 'root', 'root', '2024-01-24 03:56:00', '2024-01-24 03:56:00');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `subcat_name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `category_id`, `subcat_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'black', 1, '2024-01-16 04:50:35', '2024-01-16 04:50:35'),
(2, 1, 'watermelon', 1, '2024-01-16 04:54:10', '2024-01-16 04:54:10'),
(3, 3, 'phone', 1, '2024-01-16 05:40:24', '2024-01-16 05:40:24'),
(4, 4, 'Table', 1, '2024-01-17 00:15:58', '2024-01-17 00:15:58'),
(5, 5, 'anchor tool', 1, '2024-01-17 01:25:02', '2024-01-17 01:25:02'),
(6, 5, 'aaggsa', 1, '2024-01-17 02:06:11', '2024-01-17 02:06:11');

-- --------------------------------------------------------

--
-- Table structure for table `sub_subcategories`
--

CREATE TABLE `sub_subcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `subcategory_id` bigint(20) UNSIGNED NOT NULL,
  `subsubcat_name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_subcategories`
--

INSERT INTO `sub_subcategories` (`id`, `category_id`, `subcategory_id`, `subsubcat_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'light black', 1, '2024-01-16 05:38:21', '2024-01-16 05:38:21'),
(2, 3, 3, 'Iphone 15 pro max', 1, '2024-01-16 05:41:11', '2024-01-16 05:41:11'),
(3, 1, 2, 'small watermelon', 1, '2024-01-16 07:00:20', '2024-01-16 07:00:20'),
(4, 2, 1, 'dark black', 1, '2024-01-16 07:01:21', '2024-01-16 07:01:21'),
(5, 4, 4, 'Office Table', 1, '2024-01-17 00:16:38', '2024-01-17 00:16:38'),
(6, 5, 5, 'aleen', 1, '2024-01-17 01:25:45', '2024-01-17 01:25:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_profiles`
--
ALTER TABLE `company_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `management`
--
ALTER TABLE `management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `missions`
--
ALTER TABLE `missions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_letters`
--
ALTER TABLE `news_letters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subcategories_category_id_foreign` (`category_id`);

--
-- Indexes for table `sub_subcategories`
--
ALTER TABLE `sub_subcategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_subcategories_category_id_foreign` (`category_id`),
  ADD KEY `sub_subcategories_subcategory_id_foreign` (`subcategory_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `company_profiles`
--
ALTER TABLE `company_profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `management`
--
ALTER TABLE `management`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `missions`
--
ALTER TABLE `missions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `news_letters`
--
ALTER TABLE `news_letters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sub_subcategories`
--
ALTER TABLE `sub_subcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sub_subcategories`
--
ALTER TABLE `sub_subcategories`
  ADD CONSTRAINT `sub_subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sub_subcategories_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategories` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
